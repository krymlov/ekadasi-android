﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Srila Devananda Pandita - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
Sri Devananda Pandit used to live at Kuliya. He was a famous
reciter of the Srimad Bhagavatam and many people used to study the
Bhagavatam under his guidance.<br>
<br>
One afternoon, Srivasa Pandita came to hear Devananda Pandit's
recitation of the Bhagavatam. Hundreds of students were seated
around the Pandit and some were following his reading in their own
personal copies of the manuscript.<br>
<br>
Srivasa Pandita was a very advanced devotee and thus, when the
sweet nectar of the Bhagavatam entered his ears, his heart became
softened in love of God. He started to cry and roll on the ground,
his body having become agitated by the waves of ecstatic love.<br>
<br>
When the students of Devananda saw this, they thought, "This fellow
must be crazy. He is disturbing our hearing of the recitation. Get
him out of here." Thus they picked him up and carried him outside.
Though Devananda saw all this taking place, he didn't prevent those
foolish students of his from committing this offense. As the guru
was ignorant, so his students were sinful.<br>
<br>
Srivasa said nothing, but simply went to his house feeling very
sad. All of this occured before the appearance of Sri
Gaurasundara.<br>
<br>
During Mahaprabhu's exhibition of His divine position as the
Supreme Personality of Godhead for twenty-one hours, He asked
Srivasa if he remembered this incident.<br>
<br>
One day, when Mahaprabhu was taking a stroll around Nadiyanagara,
he came to Mahesvara Visarada Pandit's house. At that time
Devananda resided there. Mahaprabhu heard him reciting the Srimad
Bhagavatam from outside and became very angry.<br>
<br>
"What purport will that rascal explain? Not in any of his births
has he understood the meaning of even one verse of the Srimad
Bhagavatam. The Bhagavatam is the avatara of Sri Krsna in book
form. Devotion is the only subject it teaches. The four Vedas are
like yoghurt and the Bhagavatam is like butter. Srila Sukadeva
Gosvami did the churning and Maharaja Pariksit ate that butter.
Sukadeva Gosvami is very dear to Me. He knows very well that the
Srimad Bhagavatam is meant to describe the truth about Me according
to My own liking. Whoever sees any difference between Me, My own
devotees and the Srimad Bhagavatam simply brings destruction upon
himself." [C.B. Mad 21.13]<br>
<br>
Mahaprabhu made these statements in a voice loud enough for
Devananda to hear. Then He turned to go back to His home. The
devotees following Him begged for more mercy. He continued, "All
the scriptures state that the Srimad Bhagavatam enunciates the
highest realization. Without having understood any of this, simply
for the sake of name and fame as a religionist and a scholar, he
poses himself as a teacher of this great book. But he doesn't know
the purport.<br>
<br>
"Only one who has understood that the Srimad Bhagavatam is verily
the inconceivable intelligence of the Supreme Lord Himself knows
that the only meaning of the Bhagavatam is devotion. In order to
understand the book Bhagavata, one has to serve the
devotee-Bhagavata."<br>
<br>
Devananda could hear all of these remarks from the distance, yet he
thought nothing of it.<br>
<br>
After some time Gaurasundara accepted sannyasa and went to live at
Nilacala. It was then that Devananda at last began to feel some
remorse. "Such a great soul, totally imbued with love of God, but I
never went even once to have his association."<br>
<br>
One day Srila Vakresvara Pandita came to Kuliya to visit the house
of one devotee there. In the evening he held a festival of dancing
and chanting the Holy Name. Devananda was present on this occasion,
and was completely stunned by Sri Vakresvara's effulgence and
ecstatic chanting and dancing. As the night progressed more and
more, people came to listen to his kirtan until there was finally a
huge crowd. Devananda took a cane and began to control the crowd so
that Vakresvara's dancing wouldn't be disturbed.<br>
<br>
When Vakresvara fainted in ecstatic love, Devananda carefully put
his head on his lap and brushed the dust from his body with his own
upper cloth. Then he smeared that dust on his own body. That day
his service to the devotees had its auspicious beginning.<br>
<br>
After some days, Mahaprabhu returned to Bengal to see his mother
and the holy Ganges. He also came to Kuliya. At that time thousands
upon thousands of people came to have darsana of His lotus feet.
All of those who had previously committed offenses against Nimai
Pandita by thinking Him to be an ordinary human being now came to
seek His forgiveness; Mahaprabhu forgave each and everyone of them.
Among those present was Devananda, who fell down on the ground to
offer his obeisances to Mahaprabhu. From that moment he became one
of the Lord's foremost devotees.<br>
<br>
Still, he felt a little hesitant, and thus upon getting up, he
stood to one side. Mahaprabhu addressed him, "Because you have
served My dear devotee Vakresvara, I am now pleased with you. By
that service you have now been able to approach Me. Within
Vakresvara's person is Sri Krsna's complete potency. Whoever serves
him must receive Krsna's mercy."<br>
<br>
Devananda, in a faltering voice replied, "You are the Supreme
controller. Simply for the sake of reclaiming fallen souls You have
advented Yourself here at Nadiya. I am a sinful wretch and have
never served Your lotus feet and thus was cheated of Your causeless
mercy for so many years. Oh my Lord, Who resides with in the heart
of all living entities, You are Supremely merciful.<br>
<br>
Only because You have shown Yourself to me have I been able to see
You. O most compassionate One, please instruct me. Let me know the
actual purport of the Srimad Bhagavatam."<br>
<br>
Mahaprabhu replied, "Now hear Me, O brahmana, and know that the
only way to explain the verses of the Bhagavatam is in terms of
bhakti. In the beginning, middle and end of the Srimad Bhagavatam
there is only one teaching: devotion to Visnu, which is eternally
perfect and which is never destroyed or diminished."<br>
<br>
"As Krsna's various incarnations such as Matsya and Kurma appear
and disappear in this world by Their sweet will, in the same way,
the Srimad Bhagavatam is not made or composed by any person. It
makes its appearance and disappearance by its own sweet will. Due
to the appearance of devotion, the Bhagavatam blossomed forth from
Vyasadeva's mouth, by the mercy of Sri Krsna.<br>
<br>
"As the truths regarding the Supreme Authority are inconceivable,
so are the truths of Srimad Bhagavatam. Many may pretend to know
its meaning but they have no real grasp of the evidence the
Bhagavatam presents. But whoever who simply remembers the Srimad
Bhagavatam while admitting himself to be ignorant can understand
the real meaning.<br>
<br>
"The Bhagavata, which is saturated with loving devotion for Krsna,
is an expansion of Krsna Himself and contains descriptions of His
most confidential pastimes." [C.B. Ant. 3.505-516]<br>
<br>
"Now you should beg forgiveness by catching hold of Srivasa
Pandit's feet. The book Bhagavata and the devotee Bhagavata are not
different. If the devotee Bhagavata is merciful to us, then the
book Bhagavata manifests its true meaning."<br>
<br>
Then Devananda fell at Srivasa Pandita's feet and begged
forgiveness. Srivasa embraced him and his offense retreated far
away. All the devotees shouted in ecstasy, "Hari bol! Hari
bol!"<br>
<br>
His disappearance is on the 11th day of the dark fortnight in the
month of Pausa.<br>
<br>
<p style="text-align: center;"><a target="_blank" href="https://api.ning.com/files/lzg-kawjgFlrybZBT5r*VC*RVnRLeV0QJxXMyCIlG*FPUSmFouo3M8EjBgw*bheIbC7ypZOvOhP8yOCKwAkA***2IbMFUizozZTu89apEsg_/1.JPG" class="noborder"><img src="https://api.ning.com/files/lzg-kawjgFlrybZBT5r*VC*RVnRLeV0QJxXMyCIlG*FPUSmFouo3M8EjBgw*bheIbC7ypZOvOhP8yOCKwAkA***2IbMFUizozZTu89apEsg_/1.JPG?width=380"></a></p>
<div><span style="font-weight: bold;">Devananda Gaudiya
Math</span></div>
<div>In the area where this temple is located is where Sri Caitanya
forgave Devananda Pandita for his offenses. Whoever comes here
becomes free of all offenses. The Radha-Krishna Deities on the
altar are named Radha-Vinod Bihari. There is a Varaha Deity on the
left called Kola Varahadeva. In Satya-yuga, Lord Varaha appeared
before one of His devotees who lived in Navadvipa, named Vasudeva.
He told him how He would advent Himself in Kali-yuga as Sri
Caitanya Mahaprabhu. Lord Caitanya also showed His Varaha form in
this area.</div>
<div>This Math was established by Bhakti Prajnan Kesava Gosvami
Maharaja. His samadhi mandir is here. He appeared on January 1,
1898, within Gaura Mandala, in the district of Barisal in Bengal,
in the village of Banari Para. Before going to university he would
read Bhagavad-gita, Caitanya Caritamrita and Srimad Bhagavatam. In
1915 he took first initiation from Srila Bhaktisiddhanta Sarasvati
Thakur. In 1919 on Gaura Purnima he took Vaishnava initiation. He
preached in Puri, Allahabad, Assam, Mathura and many other places.
He took sannyasa from Srila Sridhar Maharaja in 1941 in Katwa. Soon
after this he established this temple. He gave sannyasa to A C
Bhaktivedanta Srila Prabhupada.</div>
<div>How To Get There?</div>
<div>To get here you first go to the town of Navadvipa. You then
take a rickshaw. From the boat ghata it takes about 15 minutes. The
sign over the gate says Sri Devananda Gaudiya Math. There are
statues of two elephants and lions fighting on the gate and
devotees with folded hands.</div>
</div>
</div>
</div>