﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Sri Srivasa Pandita - Appearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated">asyaartho vivrtas tair yah<br>
sa sankshipyaa vilikhyate<br>
bhakta-rupo gaurachandro<br>
yato 'sau nandanandanah<br>
<br>
bhakta-swarupo nityanando<br>
vraje yah sri-halayudhah<br>
bhaktaavatara acaryo<br>
'dvaito yah sri-sadaasivah<br>
<br>
bhaktaakhyaah srinivaasaadyaa<br>
yatas te bhakta-rupinah<br>
bhakta-shaktir dvi jaagranyah<br>
sri-gadaadhara-panditah<br>
<br>
"I shall now summarily explain the meaning of these words. In this
Pancha-tattwa, the bhakta-rupa (form of a devotee) is Lord
Chaitanya Mahaprabhu, who formerly appeared as Sri Krishna, the son
of Nanda Maharaj. The bhakta-swarupa (devotional incarnation) is
Lord Nityananda, who formerly appeared in Vrajabhumi as Lord
Balarama. The bhaktaavatara (devotional manifestation) is Lord
Advaita Acarya, who is not different from Sadaashiva. The
bhataakhya (pure devotee) is Srinivasa and other great devotees as
well. The bhakta-shakti (devotional energy) is Gadadhara Pandita,
the foremost of brahmanas." (Gaura Ganodesh Dipika 11.)<br>
<br>
sri-visvambharaadvaita-<br>
nityanandaavadhutahah<br>
atra trayah samunneyaa<br>
vighraha prabhavaash ca te<br>
<br>
eko mahaaprabhur jneyah<br>
sri-caitanya dayaambudhih<br>
prabhu dvau sri-yutau nityaa-<br>
nandaadvaitau mahaashayau<br>
<br>
goswaamino vighrash ca<br>
te dvi jaash ca gadaadharah<br>
pancha-tattwaatmaka ete<br>
srinivaasas ca panditah<br>
<br>
"Lord Chaitanya, Lord Nityananda Avadhuta, and Lord Advaita, are
all incarnations of the supremly exalted Personality of Godhead,
and They are all known by the title Prabhu (Master). Among Them,
Lord Chaitanya, Who is an ocean of mercy, is known as Mahaprabhu
(The Great Master), and the great personalities Lord Nityananda and
Lord Advaita are known only as Prabhu (Master). All three are also
known as Goswami (Master of the Senses). Gadaadhara is called by
the title Dvija (Brahmana, and Srinivaasa is called by the title
Pandita (Learned Scholar). These are the titles of the members of
the Pancha-tattwa." (Gaura Ganodesha Dipika 13.)<br>
<br>
"O my Lord Gaurahari, You are the abode of auspiciousness which is
as beautiful as the kirtana of Krsna. You are the ocean of
elegance, bestower of constant flow of devotion and mountain of
love which is as bright as gold. Your beautiful features give
soothing relief to the eyes of every living being and you are
mankind's salvation from all kinds of misfortune. You are the
center of the Lila Vilasa, and the life of the devotees. Be kind to
me." (Bhakti-ratnakara. KJÄ1.)<br>
<br>
"O my Prabhu, Sri Gopala Bhatta, the bee at the lotus like feet of
Sri Gaura. You are the sun which destroys the darkness of illusion,
the ocean of kindness and the greatest of all the brahmanas. You
are the son of Sri Venkata Bhatta and a valuable ornament of divine
love and devotion. You are the destroyer of worldly miseries and a
resort of happiness to the misery stricken people. O Lord save me."
(Bhakti-ratnakara. KJÄ.2.)<br>
<br>
"O my Prabhu, Srila Gopala Bhatta, a bee at the lotus like feet of
Mahaprabhu. You are the most skillful devotee of the Lord. O my
Prabhu Srinivasa, whose complexion is as golden as Sri Sacinandana,
you are the king amongst all the brahmanas. Please bless me."
(Bhakti-ratnakara. KJÄ3.)<br>
<br>
"I constantly worship the companions of Srinivasa Prabhu who is
like the wishing tree of devotional love of Sri Krsna Caitanya
Candra." (Bhakti-ratnakara. KJÄ4.)<br>
<br>
"My dear listeners, please repeatedly and joyfully hear the
Bhaktiratnakara which is the life of all Vaisnavas and the
destroyer of all miseries and misfortunes." (Bhakti-ratnakara.
KJÄ5.)<br>
<br>
"Sri Gadadhara is expansion Radharani and Srinivasa is the
expansion of Narada Muni, or in other words they are the internal
and the devotional energy respectively." (A.C. Bhaktivedanta Swami
Prabhupad. 27th May 1970. Letter to Tamal Krishna.)<br>
<br>
srivas-pandito dhimaan<br>
yah pura narado munih<br>
purvataakhyo muni-varo<br>
ya aasin narada-priyah<br>
sa raama-panditah srimams<br>
tat-kanistha-sahaodarah<br>
<br>
"Intelligent Srivaasa Pandita had previously been Narada Muni, the
best of the <span class="IL_AD" id="IL_AD7">sages</span>.
Srivaasa's younger brother, Sriman Rama Pandita, had previously
been Narada's close friend Parvata Muni" (Gaura Ganodesh Dipika
90.)<br>
<br>
Srila Vrindavana dasa Thakura, the Vyasadeva of Sri Gaurasundara's
pastimes, has sung the praises of Srivasa Pandita in this way:<br>
<br>
"It was in Srivasa Pandita's house that Sri Caitanya enacted His
pastimes of sankirtana. Those four <span class="IL_AD" id="IL_AD11">brothers</span> (Srivasa, Sri Rama, Sripati and Srinidhi)
were continuously engaged in singing the names of Sri Krsna. They
worshipped Sri Krsna and bathed in the Ganges thrice daily."<br>
<br>
These four brothers previously lived in the district of Sri Hatta.
Later on they came to reside on the banks of the Ganges. There they
regularly used to attend the assembly of devotees who gathered at
Sri Advaita Acarya's house to hear the Srimad Bhagavatam and engage
in congregational chanting of the Holy Names. Gradually
<span class="IL_AD" id="IL_AD6">the brothers</span> became very
close friends of Sri Jagannatha Misra, with whom they would chant
and listen to the Bhagavatam.<br>
<br>
In all matters Srivasa was the leader of the four brothers. By the
strength of his devotion, he could understand that Sri Krsna was
going to appear in the house of Jagannatha Misra.<br>
<br>
Srivasa Pandits's wife's name was Sri Malinidevi. She was a very
close friend of Sri Sacidevi and was always very helpful to
her.<br>
<br>
Seeing the wretched state of the fallen living entities in Kali
yuga, the devotees began to plaintively pray to the Lord for their
deliverance. Krsna hears the call of His devotees. On the full moon
<span class="IL_AD" id="IL_AD4">day in the</span> month of
Phalguna, in the year 1407 (<span class="IL_AD" id="IL_AD12">Christian</span> era 1486), the Lord incarnated Himself.
With His appearance there was the simultaneous dawning of
all-auspiciousness within the world as it became filled with
Harinama. Just as Sri Advaita Acarya, from Santipura, could
understand that the Lord had appeared, similarly, Srivasa and some
of the other devotees of Navadwipa also understood.<br>
<br>
Since previously Malinidevi was engaged in attending upon Sacidevi,
Srivasa and Jagannatha Misra came to her at this time and hinted
that she was especially needed now.<br>
<br>
As long as the Lord chooses not to reveal Himself, no one can
possibly recognize Him. Therefore, even though from His childhood
the Lord exhibited many supernatural abilities, still, due to His
deluding potency, the devotees could not fully understand His true
nature.<br>
<br>
Their hearts were simply filled with parental affection for Him,
without a trace of awe or reverence. Srivasa and Malini gave
Sacidevi and Jagannatha much advice how to raise their new son.
Srivasa and Malini were just like a mother and father to Sri
Gaurasundara. Because young Nimai Pandita seemed to have grown
arrogant by His <span class="IL_AD" id="IL_AD2">scholarship</span>,
one day Srivasa Pandita decided to give Him good counsel.<br>
<br>
"Why do people study? So that they might understand what is
devotion to Sri Krsna," he told Nimai. "If by scholarship one
doesn't gain devotion to Sri Krsna, then how will that learning
help him? It becomes simply a tedious endeavour which in the end is
nothing but a waste of time. If you have actually learned something
then begin your worship of Sri Krsna now. Make haste. This is the
purpose of your life."<br>
<br>
Nimai laughed as He replied, "By your mercy certainly that will
come to be. If you are all kind enough for me, then definitely I'll
attain devotion to Sri Krsna's lotus feet."<br>
<br>
Shortly thereafter Mahaprabhu <span class="IL_AD" id="IL_AD1">journeyed</span> to Gaya where He performed the pastimes
of accepting initiation from Sri Isvara Puri. Thus gradually, He
began to take up His real work of distributing loving devotion to
Sri Krsna.<br>
<br>
One day, in an ecstatic mood, Lord Gauranga entered Srivasa's house
asking, "Srivas, whom do you worship? Whom do you meditate upon?
Now with your own two eyes, see that person standing before you."
[C.B. Madhya 2/258]<br>
<br>
Saying this, Mahaprabhu entered the Deity room within Srivasa's
home <span class="IL_AD" id="IL_AD9">temple</span> and sat down on
the simhasana of Lord Visnu, revealing His own four-armed form,
holding the conch, disc club and <span class="IL_AD" id="IL_AD5">lotus flower</span>. Seeing this form, Srivasa was totally
stupefied.<br>
<br>
Sri Gaurasundara then said, "Due to being called by your sankirtana
and the loud roaring of Sri Advaita Acarya, I have left Vaikuntha
and have descended upon this mortal world, accompanied by My
eternal associates, will destroy the miscreants and deliver the
pious. Now, without fear, you can chant my glories."<br>
<br>
Hearing these words of His Lord, which dispelled all fear, Srivasa
fell on the ground, offering his obeisances. Then he began to
recite hymns in praise of the Lord.<br>
<br>
"My obeisances at the lotus feet of the support of the entire
universe Visvambhara, Whose bodily colour is like that of a newly
arrived rain cloud and wore garments which are the color of a
thunderbolt. My obeisances at the lotus feet of the son of Saci,
who is bedecked with ornaments of peacock feathers and a garland of
read beads (gunja-mala). My obeisances at the lotus feet of the
pupil of Ganga dasa, the beauty of whose lotus face conquers that
of ten million moons. My obeisances at your lotus feet, You who
carry a buffalo horn, stick and flute. The four Vedas have
proclaimed You to be the son of Nanda. Unto You my dear Lord, my
obeisances I offer again and again." [C.B.Madhya 2/272]<br>
<br>
"Today my birth, my activities, my everything has become
successful. Today my very existence has been crowned with the
greatest auspiciousness. Today the race of my forefathers has
finally born fruit, and my house, which was also their house, has
become blessed. Today the great fortune of my eyes is completely
beyond calculation, because I have been able to see that person
whose lotus feet are served by the goddess of fortune,
Laksmi­devi".<br>
<br>
Having described the glories of Sri Gaurasundara in various ways,
Sri Gaurasundara showed even more compassion to Srivasa by
revealing Himself to all of his family members. Seeing the niece of
Srivasa Pandita present before Him, Prabhu called to her,
"Narayani! Call upon Sri Krsna with tears in your eyes."<br>
<br>
And immediately this little girl, only four years of age, as if in
a delirious frenzy began to cry out, "Hari! Krsna!" while shedding
tears incessantly. In fact the flood of tears coursed down her body
to such an extent that the ground around her feet became soaked."
[C.B. Madhya. 2].<br>
<br>
Seeing little Narayani totally agitated in ecstatic love, Srivasa's
wife and even the household servants also began to shed tears of
love. The courtyard of Srivasa took on a very beautiful appearance,
being decorated with ecstatic love for Krsna.<br>
<br>
There was one maidservant in Srivasa Pandita's house by the name
Duhkhi. Every day she used to bring water from the Ganga for
Mahaprabhu's bath. One day Gaurasundara asked Srivasa, "Who brings
this water?"<br>
<br>
"Duhkhi brings it," Srivasa replied.<br>
<br>
"From today her name is Sukhi." Thus the Lord indicated that those
who serve the Lord and the Lord's devotees are not duhkhi (sad);
rather, they are sukhi (happy).<br>
<br>
At this time Sri Gaursundor began His lila as the yuga-avatara at
the house of Srivasa Pandit. Having been reunited with Sri
Nityananda Prabhu He started congregational chanting of the Lord's
Holy Names in the courtyard of Srivasa's house. Nityananda Prabhu
took up His residence there. Sri Malinidevi served Nitya­nanda as
though He were her own son.<br>
<br>
Sri Nityananda was actually Sri Baladeva Himself, and in His
pastimes as Lord Nityananda He behaved somewhat like a madman. He
was always overwhelmed by love of Krsna, and was not aware of the
state of His outward dress or ornaments.<br>
<br>
One evening, Sri Gaurasundara, accompanied by His associates, was
engaged in chanting and dancing at the house of Srivasa Thakura
when one of Srivas Thakur's sons passed away after having suffered
the effects of some disease. Within the inner apartment of his
house, the women began to wail in lamentation at the boy's untimely
death. Srivasa Pandita, who was outside in the courtyard,
understood some tragedy must have taken place.<br>
He quickly entered the house only to find that his son had passed
on to the next world. As he was a very grave devotee and completely
conversant in the science of the Absolute Truth, he was able to
console the women in their grief.<br>
<br>
"You are all aware of Krsna's glories, so please restrain
yourselves and don't cry. Whoever during his last moment hears the
Lord's Holy Name, even though he might be the greatest sinner,
attains Krsna's abode. And that incomparably wonderful Lord, He
whose glories are sung by all His servants up to and including Lord
Brahma, is now personally dancing in the courtyard of your
house.<br>
<br>
"This moment is so auspicious that it is sure this boy has
successfully completed his journey from this world. Is there
anything to be lamented at in this? His good fortune is completely
assured. I can understand that whatever part I have played in this
is also crowned with success." [C.B.M 25.30]<br>
<br>
He concluded by telling them, "If you can't control your emotions
because of your family affection to the boy, then at least don't
cry now, you can cry later. The Supreme Lord of Gokula Himself, Sri
Gaurasundara, is performing sankirtana accompanied by His devotees
in this house. If, due to your crying, the happiness that He is
experiencing from dancing in ecstasy during sankirtana is
disturbed, then I will just this minute jump into the Ganges and
thus give up my own life."<br>
<br>
"Not a blade of grass moves unless by Krsna's will. To see in this
happiness or distress, or knowledge or ignorance is simply
imagination. Know that whatever Krsna wills is good, and thus,
giving up your own selfish desires, become free from confusion and
unnecessary botheration. Krsna is giving and Krsna is taking away,
and it is Krsna alone Who is maintaining everyone. Someone He
protects and someone He destroys, all according to His will. If
someone contemplates something contrary to Krsna's will, then as a
result of His desire He only get torment. Giving up all
lamentation, simply hear Krsna's name, and thus pass through all
difficulties feeling great happiness. Then your desires will
actually be fulfilled.'<br>
<br>
Having given all of these instructions to those present, Srivasa
again went outside to join Mahaprabhu in ecstatic chanting and
dancing. The women, leaving the dead body, came to hear the kirtan
of Mahaprabhu. And so Mahaprabhu continued His chanting until the
middle of the night.<br>
<br>
When everyone was at last leaving to take rest, Mahaprabhu spoke.
"Today My mind is feeling some tribulation. I think some sad event
has occurred in Srivasa's house."<br>
<br>
The pandit replied, "What possible unhappiness could there be in
that person's house where Your divinely blissful countenance is
seen?"<br>
"Srivasa! Why wasn't I feeling bliss in kirtana today? What
inauspicious thing transpired in your house?"<br>
<br>
Srivasa answered, "My Lord, You are Yourself all auspicious. Where
You are present, no sorrow can be found anywhere!" But the other
devotees informed the Lord that Srivasa's son had passed away.<br>
<br>
Hearing this news, Gaura Raya cried out, "Alas, what a tragic
event. Why didn't you tell me of this unfortunate news before?"<br>
<br>
"I will explain," Srivasa Pandita replied. "I couldn't tolerate
disturbing You while You were enjoying sankirtana. If one of my
sons dies, what sorrow is there in that for me? If we all die while
seeing You, that would actually be a matter of great happiness. On
the other hand, if You would have to stop dancing, then perhaps I
would have died. My Lord, this was the danger that I feared, and
thus I didn't tell you at the time."<br>
<br>
Seeing Srivasa Pandita's profound devotion, Sri Gaurasundara said,
"How can I give up such company as this?" With tears in His eyes He
continued. "Due to love for Me, he didn't even feel lamentation at
the death of his son. How will I abandon their companionship?"<br>
<br>
The Lord continued to cry, and the devotees began to worry within,
having heard Him speak of leaving them.<br>
<br>
Thereafter Mahaprabhu came to where the dead body of the infant was
lying. Touching it, He called, "Boy! Why are you going away and
leaving Srivasa Pandita?"<br>
<br>
The life of the dead child returned at the touch of Mahaprabhu's
hand. After offering obeisances to the Lord, he replied, "O Prabhu!
Whatever You ordain is absolute. No one can do anything but what is
sanctioned by You. As many days as I was destined to remain here,
that many days I have stayed. Now that my time has elapsed, I have
proceeded to leave.<br>
<br>
"My Lord, I have taken birth and died repeatedly. But this time, at
the time of death, I passed away quite happily, having taken
darsana of Your beautiful face."<br>
<br>
After saying this, the child became silent. Sri Gauranga Raya thus
enjoyed a kind of transcendental sport. Having heard the uncommon
words of the dead child, the devotees floated in the ocean of
bliss. Srivasa Pandita, along with his family members, then fell at
the lotus feet of Sri Gaursundara and cried in ecstatic love.<br>
<br>
Mahaprabhu then told him, "Since Myself and Nityananda are your two
sons, please don't feel any more distress in your mind over what
has happened." Hearing these compassionate words of their Lord, the
devotees cheers were resounded throughout the heavens.<br>
Proving the statements of sastra, Their Lordships Gaura-Nitya­nanda
became deeply indebted to Srivasa due to his great love for and
service to Them.<br>
<br>
After Mahaprabhu took sannyasa, Srivasa Pandit came to live at
Kumarhatta. Every year he would go with His brothers to see
Mahaprabhu at Puri. He also regularly came to see Sri Sacimata in
Navadwip and would spend a few days there during those times.<br>
<br>
When Mahaprabhu came from Nilacala to see His mother and the river
Ganges, He also stopped at Kumarhatta to see Srivasa.<br>
<br>
"After staying some days at Advaita's house, Mahaprabhu came to
Srivasa's temple at Kumarhatta." [C.B]<br>
<br>
It was at this time that Mahaprabhu gave this benediction to
Srivasa: "There will never be poverty in your house. If you simply
remain indoors, never even venturing out of your house, whatever
you require for your worship will come to your door."<br>
<br>
Srivasa Pandita, along with his three brothers, eternally serve Sri
Gaurasundara. He is the incarnation of Narada and accompanied
Mahaprabhu in all His Navadwipa lilas.<br>
<br>
Across the Ganges from Halisahar (the present name of the village
of Kumarhatta), there is a town of the name Chuchura. Located here
are some very beautiful Deities of Sri Sri Nitai-Gauranga. At
present the seva-puja is becoming carried on by Devananda Gaudiya
Matha. The resident Swami Maharaja recalls that when the Gaudiya
Math took over the sevaitship of this temple, its name was Srivasa-
Mahaprabhu Mandir. Thus there is a strong possibility that these
are the worshippable Deities of Srivasa Pandita. The temple is at
present named 'Uddhavar Gaudiya Math.' It is near 'Chaumatha',
close to the 'Chori Mor' intersection where the town clock
stands.<br>
<br>
There is a rail station at Chuchura (Chinsura) on the
Bandel-Navadwip line. Chuchura may also be reached by ferry from
Halisahar on the other side of the Ganga, where Caitanya Boda, the
birthplace of Sri Isvara Puri, is located.</div></div>
                            </div>
</div>

                </div>