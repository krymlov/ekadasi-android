﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Sri Svarupa Damodara Gosvami - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated"><p style="font-family: Arial; font-size: 14px;"></p>
<p style="text-align: justify;"><span style="font-family: Verdana;"><span style="font-family: Arial; font-size: 14px;">Shri Svarupa Damodara is an
eternal associate and friend of Shri Chaitanya Mahaprabhu.<br>
Previously his name was Shri Purushottama Acharya.<br>
He lived in Nabadwipa.<br>
He always stayed with Mahaprabhu.<br>
<br>
When Shriman Mahaprabhu took sannyasa, he became half-mad.<br>
He went to Varanasi and took sannyasa from a sannyasi named
Chaitanyananda, who ordered him to study Vedanta and teach it to
the people in general.<br>
Shri Purushottama Acharya did not accept the dress of a sannyasi,
but only gave up the shikha and sacred thread.<br>
His name became Svarupa.<br>
After this, taking up the order of his sannyasa-guru, Shri
Purushottama Acharya went to Jagannatha Puri.<br>
At that time, he again met with Shri Chaitanya.<br>
The Chaitanya Charitamrita (ML 10:102-104) records this meeting as
follows<span style="font-weight: bold;">:</span><br>
"One day, Svarupa Damodara arrived.<br>
His mystic awareness of the Lord's inner purpose was deep and
unfathomable.<br>
He was an ocean of rasa.<br>
In his previous ashrama, his name had been Purushottama
Acharya.<br>
When he was in Nabadwipa, he had attained the Lord's lotus
feet.<br>
But when he saw the sannyasa of Shri Chaitanya, he went half
mad.<br>
And so, he went straight to Varanasi to take sannyasa.<br>
<br>
In connection with Svarupa Damodara, Shrimad Krishnadasa Kaviraja
Goswami has written further, as follows<span style="font-weight: bold;">:</span><br>
"Shri Svarupa Damodara was the limit of scholarship; still, he did
not generally converse with anyone.<br>
He kept to himself. He liked to remain alone, and did not see
others.<br>
He was the very embodiment of Krishna-prem.<br>
He was like a second Mahaprabhu, in the sense that he fully
understood the Lord's conclusions on Krishna-tattva and
Krishna-bhakti, and could fully represent them.<br>
As such, whoever wanted to bring before the Lord a book, poem, or
song would first bring it to Svarupa Damodara, who would examine it
before the Lord would hear it.<br>
The Lord did not delight in hearing literature and song which was
opposed to the conclusions of bhakti (bhaktisiddhanta-viruddha), or
which contained rasabhasa, conflicting devotional mellows.<br>
Therefore, Svarupa Damodara would examine these things, and if he
decided that they were pure, then he would arrange for the Lord to
hear them.<br>
<br>
Svarupa Damodara would sing for Shri Chaitanya the songs of
Vidyapati, Chandidasa, and Gita-Govinda, and this gave the Lord
much pleasure.<br>
Svarupa Damodara could sing like a Gandharva, and he knew the
scriptures like Brihaspati, the guru of the gods.<br>
No one was a greater soul than he.<br>
He was most dear to Advaita and Nityananda and the life and soul of
the devotees headed by Shrivasa Thakura."<br>
<br>
In this way, it is said that Svarupa Damodara was like a second
form of Mahaprabhu, in song he was like a Gandharva, and in
scripture, he was like Brihaspati.<br>
Svarupa Damodara was very expert in music as well as Vedic
scriptures.<br>
Shri Chaintaya used to call him Damodara, becasue of his expert
singing and musical skills.<br>
The name Damodara was given by Shri Chaitanya and added to the name
given by his sannyasa guru.<br>
He was therefore known as Svarupa Damodara.<br>
He compiled a book of music called Sangita-Damodara.<br>
<br>
When anyone wanted to submit a song, a verse, or a book to Shriman
Mahaprabhu, they first had to submit it to the examination of
Svarupa Damodara, before it would be heard by the Lord.<br>
When Svarupa Damodara came from Kashi, he submitted a verse before
the Lord, glorifying Him.<br>
This verse has been recorded by Kavi Karnapura in his Chaitanya
Chandrodaya Nataka as follows<span style="font-weight: bold;">:</span><br>
<br>
heloddhunita khedaya vishadaya pronmila-damodarau<br>
shamyacchastra vivadaya rasadaya cittarpitonmadaya<br>
shashvad-bhaktivinodaya sa-madaya madhurya-maryadaya<br>
shri chaitanya dayanidhe tava daya bhuyadamandodaya<br>
<br>
"O Ocean of Mercy, Shri Chaitanya! Let that which easily drives
away whatever pain we have been experiencing in this material
world; that which is all purifying, that which manifests the
greatest transcendental bliss; that which by its sunrise casts away
all the doubtful conclusions of the shastra; that which rains rasa
on our hearts and minds, rules our consciousness and thus causes
jubilation; that all-liberating, all-auspiciousness-giving, the
limit of madhurya-rasa mercy of yours-let it arise within our
hearts. "<br>
<br>
Shri Svarupa Damodara made his dandavats before the Lord, and the
Lord embraced him, saying, "Today I saw in a dream that you were
arriving here.<br>
Everything was just right. Just as one who is blind becomes happy
getting eyes, so I have become happy upon getting you."<br>
Shri Svarupa Goswami said, "O Lord! Please forgive me.<br>
Forgetting you, I deserted your lotus feet. Not having a trace of
prema within me, I left your service, and being sinful, I went to a
foreign land.<br>
But, although I abandoned you, you didn't abandon me.<br>
By the ropes of your mercy, you have bound me by the neck and
dragged me back to your lotus feet."<br>
Hearing Svarupa Damodara saying these words in great humility, the
Lord again embraced him and said, "Shri Krishna is very kind. By
his mercy we have met once again."<br>
<br>
Svarupa Damodara always stayed near the Lord.<br>
Whatever mood the Lord was in, Svarupa Damodara would perform
kirtan to augment the Lord's internal sentiments.<br>
Around the same time that Svarupa Damodara came to Puri, Shri
Ramananda Raya arrived from Vidyanagara.<br>
Shri Ramananda Raya was a great poet and could explain everything
in a very elegant style.<br>
Shri Chaitanya Mahaprabhu heard rasatattva from Ramananda's
mouth.<br>
<br>
In the daytime, Shri Chaitanya Mahaprabhu used to perform kirtan
with his devotees.<br>
At night, in the company of Ramananda Raya and Svarupa Damodara, he
would relish the truths about the pastimes and mellows of Radha and
Krishna.<br>
In the same way that Lalita and Vishakha are the internal
confidantes of Shri Radha, Svarupa Damodara and Ramananda Raya are
the internal confidantes of Shri Chaitanya Mahaprabhu.<br>
<br>
At the time of Shri Gaurasundara's final pastimes, Shri Svarupa
Damodara was constantly with the Lord.<br>
Shri Svarupa Damodara's right hand was Raghunatha Dasa Goswami.<br>
<br>
On the second day of the day of full moon in the month of Asharh,
Shri Svarupa Damodara Goswami passed away from this earth and
entered into the Lord's unmanifest pastimes.<br></span></span></p></div></div>
                            </div>
</div>

                </div>