﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Srila Nimbarkacarya - Appearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated"><p style="text-align: center;"><a target="_blank" href="https://api.ning.com/files/iaXzSJaCxulNhzJ8lQa0c4aJT6lEOpH-PfiVW7Mx3pDD*AB-YHimbcBcVC-Gv02zE-qFwm3BiV1r5312OcpA6foafAuskieJylQ2F2LeihQ_/srinimbarkacarya2.jpg" class="noborder"><img src="https://api.ning.com/files/iaXzSJaCxulNhzJ8lQa0c4aJT6lEOpH-PfiVW7Mx3pDD*AB-YHimbcBcVC-Gv02zE-qFwm3BiV1r5312OcpA6foafAuskieJylQ2F2LeihQ_/srinimbarkacarya2.jpg?width=224"></a></p>
<div align="justify">Sri Nimbarka Acarya comes in the line of the Kumara Sampradaya and is believed to have lived around the 11th and 12th <span id="IL_AD12" class="IL_AD">centuries</span>. Born in the modern Murgarapattam in the southern Dravidian province, he was the son of Aruni and Jayanti, a very pious brahman a couple of the Tailanga order.<br>
<br>
He was a great ascetic of the Tridandi order. He wrote many books according to the Dvaitadvaitavada and he lived most of his devotional life at a place called Nimbagrama near Govardhana at Vrindavana.<br>
<br>
While at Nimbagrama, he defeated a great Jain pandit in a discussion. The Jain had refused to dine with him saying that the Sun had already set and so he would not take any food. At this, Nimbarka by his own potency made the sun appear from a Nimba tree and the astonished Jain accepted his <span id="IL_AD6" class="IL_AD">hospitality</span>. It is said that his name Nimbarka or Nimbaditya has been derived from this miracle of Aditya or Arka meaning sun from a nimba tree.<br>
<br>
Once in a village near the forest of Bilva Paksha, a group of brahmanas came to engage in the worship of Lord Siva. Just as Lord Visnu is very much pleased when He is offered the leaves of His most beloved Tulasi, so Lord Siva is pleased by offerings of leaves from the bilva or bael tree. Thus the brahmanas worshipped Lord Siva by offering bilva leaves, for a period of one fortnight and satisfied Lord Siva greatly.<br>
<br>
Amongst those brahmanas was one whose name was Nimbarka. He had been particularly attentive in his worship of Lord Siva. Being especially pleased with Nimbarka's devotion, Lord Siva personally appeared to him.<br>
<br>
"Nimbarka," said Lord Siva, "at the edge of this village is a sacred bael forest. There, in that forest, the four Kumaras are absorbed in <span id="IL_AD7" class="IL_AD">meditation</span>. By their mercy you will receive transcendental knowledge, for they are your spiritual masters, and by rendering service unto them you will receive all that is of value." After saying this Lord Siva disappeared.<br>
<br>
Nimbarka immediately went to that place and with great determination he searched for the four Kumaras in every direction, until he finally found them, shining as brilliantly as the sun. They were seated on a very <span id="IL_AD8" class="IL_AD">beautiful natural</span> platform beneath a tree and they appeared like fire blazing on an altar. They were all very young, seeming to be no more than five years in age, but they appeared most noble in character as they sat there naked, rapt in meditation. Nimbarka was so excited that he cried out, "Hare Krsna!"<br>
<br>
This sudden sound startled the four brothers, breaking their meditation. Opening their eyes, they saw before them the blissful form of an ideal devotee, and with great pleasure they embraced Nimbarka, one after another.<br>
<br>
"Who are you," they asked, "and why have you come here? We are certainly ready to answer all your prayers."<br>
<br>
Nimbarka fell at the feet of the four brothers like a rod, and with great humility he introduced himself.<br>
<br>
With a sweet smile on his face Sanat Kumara then said, "The all-merciful Supreme Personality of Godhead, knowing that Kali-yuga will be extremely troublesome for the living entities, resolved to proagate devotional service to Himself. With this goal in m ind, He has empowered four personalities with devotion and sent them into this world to preach. Ramanuja, Madhva, and Visnuswami are three and you are the fourth of these great souls. Laksmi accepted Ramanuja as a disciple, <span id="IL_AD3" class="IL_AD">Brahma</span> accepted Madhva, Rudra accepted Visnuswami and meeting you today we have the good fortune of being able to instruct you. This is our intention. Previously we were engaged in meditating on the impersonal Brahman, but by the causeless mercy of Lord Visnu, we have been relieved of this <span id="IL_AD1" class="IL_AD">sinful</span> activity. Since I have realised that it is essential to preach pure devotional service, I have composed a literary work entitled Sanat Kumara Samhita. After taking initiation from me, you should follow the instructions I have set forth therein."<br>
<br>
Nimbarka was greatly enlivened at the prospect of taking initiation and he immediately ran to take bath in the Ganges. After this he quickly returned and prostrated himself once more before the effulgent brothers.<br>
<br>
"O deliverers of the fallen," said Nimbarka, in all humility, "please deliver this low-born rascal."<br>
<br>
The four Kumaras gave him the Radha-Krsna <span id="IL_AD4" class="IL_AD">mantra</span> and instructed him in the method of worshipping Radha and Krsna with the sentiments of great love called Bhava Marga.<br>
<br>
In that sacred bael forest Nimbarka began to worship Sri Sri Radha Krsna according to the Sanat Kumara Samhita and he chanted the mantra they had given him. Very soon Radha and Krsna revealed Themselves to Nimbarka. They stood before him, spreading an effulgence which illuminated all directions. In sweet voices They addressed him thus: "Nimbarka, you are very fortunate, for you have performed Sadhana in Sri Navadwipa. We both combine, and assume one form, as the son of Sacidevi." At that moment Radha and Krsna combined and displayed Their form of Gauranga.<br>
<br>
Beholding this splendid vision, Nimbarka began to tremble. "Never," he said, "Never have I ever seen or heard of such a form at any time."<br>
<br>
Lord Gauranga then said, "Keep this form, which I now show you, a secret for the time being. Just preach about devotional service and the pastimes of Radha and Krsna, for I gain great satisfaction from this. When I make My appearance and perform My <span id="IL_AD5" class="IL_AD">education</span> pastimes, you will also appear. Taking birth in <span id="IL_AD10" class="IL_AD">Kashmir</span> as a great pandit, you will tour all over India defeating all opposition. Your reputation and learning will be celebrated everywhere and you will be known as Kesava Kashmiri. Whilst wandering in Sri Navadwipa you will come to Sri Mayapur. Simply by hearing your name all the great <span id="IL_AD11" class="IL_AD">pandits</span> of Navadwipa will flee. Intoxicated with scholastic pride, I will take great pleasure in defeating you. However, by the mercy of Mother Sarasvati, the goddess o f learning, you will realize My true identity. Giving up your false pride, you will take shelter of Me and I will reward you with the supreme gift of loving devotional service and will again despatch you to preach. Thus you can satisfy Me by preaching the philosophy of dvaita-advaita. Keep My identity a secret. In the future when I begin My sankirtan movement, I will personally preach, taking the essence of your philosophy and the <span id="IL_AD9" class="IL_AD">philosophies</span> of Madhva, Ramanuja and Visnuswami."<br>
<br>
After instructing Nimbarka in this way, Lord Gauranga disappeared, and filled with intense ecstacy, Nimbarka began to shed tears of love. After worshipping the lotus feet of his gurus and taking their permission, he left Sri Navadwipa to begin his preaching mission.<br>
<br>
(Source: Sri Navawipa-dham mahatmya)<br>
<br>
<p style="text-align: center;"><img src="https://api.ning.com/files/lunXdl9By1lKsPHpxN2n0aTHEDtqaxEuyZXhQBRCivX0UsnkmiWxv555C4*ICJ0L0bF3FnEGFv1MD*98kp*1qjGT-8AEQa3eSZ6pbKRhY4o_/lordkrishnaandmeerabaioneithersideofdambaruBE20_200_120.jpg?width=200"></p>
</div></div></div>
</div>
</div>
</div>