﻿<div class="tribe-events-single-event-description tribe-events-content">
<table style="width: 100%; ">
<tbody>
<tr>
<td>
<p style="text-align: justify;">
	Sri Viracandra or Virabhadra Prabhu appeared on the ninth day of the dark fortnight of the month of Kartik. Krsna dasa Kaviraja Gosvami has given this description of Him in the C.c. Adi 11.8-12.
</p>
<p>
	&nbsp;
</p>
<p style="text-align: justify;">
	<span id="more-6529"></span>"After Nityananda Prabhu, the greatest branch is Virabhadra Gosani, who also has innumerable branches and sub-branches. It is not possible to describe them all. Although Virabhadra Gosani was the Supreme Personality of Godhead, He presented Himself as a great devotee. And although the Supreme Godhead is transcendental to all Vedic injunctions, He strictly followed the Vedic rituals. He is the main pillar in the hall of devotional service erected by Sri Caitanya Mahaprabhu. He knew within himself that He acted as the Supreme Lord Visnu, but externally He was prideless. It is by the mercy of Sri Virabhadra Gosani that people all over the world now have the chance to chant the names of Caitanya and Nityananda. I there take shelter of the lotus feet of Virabhadra Gosani so that by His mercy my great desire to write Sri Caitanya-Charitamrta will be properly guided."
</p>
<p style="text-align: justify;">
	Sri Birbhadra Gosvami is the son of Sri Nityananda Prabhu and the disciple of Sri Jahnava Mata. His mother is Srimati Vasudha Devi. In the Gaura-ganoddesa-dipika it is described that Sri Viracandra Prabhu is an incarnation of Ksirodakasayi Visnu – one of the expansions of Sri Sankarsana. He is thus not different from Lord Caitanya Himself.
</p>
<p style="text-align: justify;">
	Once upon a time, Isvari (Sri Jahnava Mata) went to the house of one Jadunandana Acarya of Jhamatpur near Rajbol Hat(ta?)œ and by her mercy he became a devotee. Jadunanda's wife was extremely devoted to her husband and his two daughters, Srimati and Narayani, possessed bodily forms which excelled the limits of mortal beauty. By the desire of Isvari that very perfect brahmana gave these two daughters in marriage to Prabhu Viracandra. [B.R.13]
</p>
<p style="text-align: justify;">
	Sri Yadunandana Acarya became the disciple of Vircandra Prabhu and Srimati and Sri Narayani were given initiation in mantra by Sri Jahnava Mata. Viracandra prabhu had a sister, Srimati Gangadevi, who was none other than Mother Ganga herself. Her husband Madhavacarya was an incarnation of Raja Santanu.
</p>
<p style="text-align: justify;">
	Taking permission from his mother, Viracandra Prabhu set out on a pilgrimage to Vrindavana. First he came to Saptagram and proceeded to the house of Sri Uddharana Datta Thakura where He was met by Uddharana Datta's son, Srinivasa Datta. Srinivasa greeted Him with respect and showed Him all hospitality for the two days that he stayed there. From there He went to Santipur where He was led to Advaita Bhavan in a kirtan procession by Advaita Acarya's son, Sri Krsna Misra. Next he crossed the Ganges River and came to Gauri dasa Pandita's house in Ambika Kalna. Sri Hrdaya Caitanya Prabhu respectfully greeted Him and honored Him as His guest. From here He came to Navadvipa, to the house of Jagannath Misra. When the family members and associates of Mahaprabhu learned that He was the son of Sri Nityananda Prabhu, they were highly pleased and showed Him all courtesies.
</p>
<p style="text-align: justify;">
	After spending two days there He came to Sri Khanda where He was greeted very respectfully and affectionately by Sri Raghunandana and to Sri Kanai Thakura. He spent a few days there, and then went to Srinivasa Acarya Prabhu's house in Jajigram, where Acarya Prabhu received his exalted guest by offering Him the appropriate worship. A few days passed In the ecstasy of sankirtan and then Viracandra Prabhu came to Kantak Nagan (Katwa). He remained there for one day and then proceeded to Bhudarigram. There He was received by Sri Govinda Kaviraj who worshipped His lotus feet and offered Him all hospitality. Sri Viracandra Prabhu was very pleased at his devotion and remained there for a couple of days. Thereafter He made His auspicious arrival in Kheturi.
</p>
<p style="text-align: justify;">
	As soon as he received news of His arrival, Sri Narottama Thakura came out in great ecstasy to meet Sri Viracandra Prabhu. He led Him into the courtyard of the temple of Sri Gauranga where sankirtan accompanied with ecstatic dancing commenced. A steady crowd of people thronged to get a glimpse of Prabhu Vircandra.<br>
	[B.R. 13]
</p>
<p style="text-align: justify;">
	After spending a few days in Kheturi in ecstatic sankirtana, Viracandra Prabhu set off for Sri Vrindavana Dhama. Along the way many sinful and atheistic people were delivered by His influence. As news of His arrival reached Vrindavana, the Gosvamis, namely Srila Jiva Goswami, Srila Krsna dasa Kaviraj Gosvami, Sri Ananta Acarya, Sri Hari dasa Pandita, Sri Krsna dasa Brahmacari (Sri Madhu Gopala's pujari, a disciple of Sri Gadadhara Pandita), Sri Madhu Pandita (pujari of Sri Gopinatha), Sri Bhavananda, Sri Kasisvara, as well as his disciple Sri Govinda Gosvami and Sri Yadavacarya were all informed.
</p>
<p style="text-align: justify;">
	The Gosvamis, accompanied by many residents of Sri Vrindavana Dhama, all came out to meet Prabhu Viracandra. Everyone was overwhelmed by His appearance in ecstatic love, and they all were humming like bumble bees in discussing His good qualities. Prabhu Viracandra, in the company of these exalted souls, took darshana of Sri Madana Mohana, Sri Govindaji, Sri Gopinatha, the principal Deities in Vrindavana.<br>
	[B.R. 13]
</p>
<p style="text-align: justify;">
	After receiving the consent of Sri Jiva Gosvami and Sri Bhugarbha Gosvami, Prabhu Viracandra set out to wander in the twelve forests of Vrajamandala, becoming completely overwhelmed by seeing the various places of Krsna's pastimes, like Sri Radha Kunda, Sri Shyama Kunda and Giri Govardhana. His display of ecstatic symptoms captivated the Brijbasis. After seeing the sights of Sri Vrajadhama, Bircandra Prabhu returned to Gaudadesa.
</p>
<p style="text-align: justify;">
	All of the divine qualities of His father, Sri Nityananda Prabhu, were fully present in Him. Those who were fortunate enough to witness His ecstatic love praised His glories far and wide.
</p>
<p style="text-align: justify;">
	Gopijana Vallabha, Rama Krsna and Ramacandra, who are thought by some to be the sons of Virabhadra Gosai, were actually His disciples. The youngest, Rama Krsna, who resided at Khardaha, belonged to the Sandilya Gotra and was a member of the Batabyal brahmana community, known for their great accomplishment in studying the Vedas. The oldest, Gopijana Vallabha, lived at Latagram near Manakar, in the district of Barddhaman. The middle one, Rama­candra, lived at Ganeshpur near Maldah. Because their gotras and surnames (titles) were different, and because they lived at different places, it is concluded they were not actually Vira­candra Prabhu's sons.
</p>
<p style="text-align: justify;">
	His birthplace is at Khardaha, which now is a rail station on the Sealdah-Krsnanagar line. The village where Nityananda Prabhu made His advent in this world – Ekacakra – is also named Viracandra­pur. Here the Deity of Sri Bankim Raya was worshipped by Vira­candra Prabhu.
</p>
</td>
</tr>
</tbody>
</table>
			</div>