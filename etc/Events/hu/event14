﻿<h3 class="post-title entry-title" itemprop="name headline">Anantha Chaturdashi Vrata</h3>
<div class="post-content">
<div class="entry-content" itemprop="articleBody">
<p style="text-align: justify;">
	<img alt="Anantha-Chaturdashi-Vrata" class="alignnone size-full wp-image-21524" data-id="21524" height="450" src="http://www.iskcontimes.com/wp-content/uploads/2018/09/Anantha-Chaturdashi-Vrata.png" width="716" srcset="http://www.iskcontimes.com/wp-content/uploads/2018/09/Anantha-Chaturdashi-Vrata.png 716w, http://www.iskcontimes.com/wp-content/uploads/2018/09/Anantha-Chaturdashi-Vrata-300x189.png 300w" sizes="(max-width: 716px) 100vw, 716px">
</p>
<p style="text-align: justify;">
	The Anantha chaturdashi-vrata (Bhadrapada shukla Chaturdashi) is widely observed in India. It is also called ‘Ananthapadmanabha-vrata’. Lord Vishnu appeared on this day as Padmanabha lying on the couch of Anantha Sesha (the thousand-hooded serpent).
</p>
<p style="text-align: justify;">
	Anantha vrata is observed with the intention of fulfillment of worldly desires (‘kamya vrata’). This vrata is mainly observed for regaining lost prosperity. On the day of Anantha Chaturdashi, the frequencies of Sri Vishnu are active in the Universe. It is easy for average people to attract and absorb these frequencies on this day. Performing Ananta puja means to absorb the action-energy of Sri Vishnu.
</p>
<p style="text-align: justify;">
	The presiding Deity of Anantha vrata is Anantha, that is, Sri Vishnu. The Deities of Yamuna and Shesha also accompany Sri Vishnu.
</p>
<p style="text-align: justify;">
	Anantha Vrata, is dedicated to Lord Anantha Padmanabha Swamy. It is observed on the fourteenth day in Shukla Paksha during Bhadrapada month. Anantha means the infinite and endless. Anantha Padmanabha is Lord Vishnu reclining on the snake Anantha in Anantha Shayana posture. This posture of Lord Vishnu resembles His concern and thoughts about the Universe and its prosperity.
</p>
<p style="text-align: justify;">
	Anantha Padmanabha puja makes devotees free from all their sorrows and keeps them in readiness to challenge any attack from their enemies. Throughout India, Anantha Chaturdashi vrata is observed with utmost devotion. In South India, Anantha Vrata is observed in a festive atmosphere. Special sweets and other dishes are prepared on the day and a portion of it is offered to Brahmins. Anantha Vrata is observed for 14 consecutive years for marital bliss and prosperity of the family.
</p>
<p style="text-align: justify;">
	<strong>History of Anantha Chaturdashi Vrata:</strong>
</p>
<p style="text-align: justify;">
	In some scriptures such as Mahabharata, Anantha Vrata is mentioned as a great observance to get rid of one’s sorrows in life. Lord Krishna advised Yudhisthira Maharaja to observe Anantha Vrata for 14 years to regain his kingdom and his entire wealth.
</p>
<p style="text-align: justify;">
	<strong>Ritual of Anantha Vrata – Tying of Anantha Daara (thread):</strong>
</p>
<p style="text-align: justify;">
	The important ritual during the Vrata is tying of the sacred thread on the hand. First, the performers of the Vrata place the threads along with the Deity of Lord Anantha Padmanabha Swamy to sanctify them. The thread is worshipped by placing kum kum on it. This sacred thread is referred as Anantha Daara. It is made up of 14 strands. Some make it of 14 knots.
</p>
<p style="text-align: justify;">
	Women tie this thread on their left hand and men on their right hand.
</p>
<p style="text-align: justify;">
	<strong>Anantha Chaturdashi Katha:&nbsp;</strong>
</p>
<p style="text-align: justify;">
	There once lived a Brahmin called Sumanta born in Vasishtha Gotra and had married Brighu's daughter Dheeksha. They had a charming daughter called Susheela. When Susheela was a young girl, she lost her mother. Her father married a woman called Karkashe who was hard hearted and quarrelsome. She made the life of Susheela miserable. In spite of these difficulties at home, Susheela grew to be a fine lady ready to be married. When Sumanta was worried over her marriage, there came to his house looking for a bride, a rishi called Kaundinya. Sumanta gives his daughter to Kaundinya rishi in marriage. With his wife being uncooperative, he could give to his son-in-law as a marriage gift, a little of fried wheat flour.
</p>
<p style="text-align: justify;">
	Kaundinya accompanied by his wife Susheela left the place and reached the bank of a river during their travel. That day being Bhadrapada shukla Chaturdashi many Brahmins and their wives who were wearing red dress, were worshiping Ananta Padmanabha. Curious Susheela approaches these ladies and asks them for details of the vrata. They say that the vrata is called Anantha vrata and the fruits are infinite (anantha); when Susheela showed interest in performing the vrata, they tell her in detail as follows:
</p>
<p style="text-align: justify;">
	The vrata is to be performed on Bhadrapada shukla Chaturdashi. Having taken bath and wearing a clean dress, decorate the altar with devotion. Keep a kalasha on the south of the altar in which you invite the Lord. Keep seven darbhas tied to each other to represent Ananta. Keep a red thread with 14 knots on the altar. Worship the Lord with 14 varieties of flowers and 14 varieties of leaves. The prasadam for this vrata is Atiras. Make 28 of them and serve them to brahmanas. Those attending should be given food and respect. Do this vrata for 14 years each year replacing the thread that was worn in the earlier year. On the 15th year do the udyapana (conclusion with donation of gifts to brahmanas).&nbsp;&nbsp;
</p>
<p style="text-align: justify;">
	Having listened to the narration with attention, faith and devotion, Susheela performed the puja with those gathered there; distributed half of the fried wheat flour to brahmanas, wore the red thread and with the thoughts of Ananta Padmanabha accompanied her husband to his ashram. Because of the power of Anantha vrata, Kaundinya's ashram acquired beauty and wealth. All his relatives were eagerly waiting to do the Anantha vrata. Susheela had acquired an aura of brilliance.
</p>
<p style="text-align: justify;">
	One day Kaundinya sees the red thread on Susheela's hand and asks for an explanation. In spite of her telling him the truth, in a fit of anger and jealousy, Kaundinya forcibly removes the thread and throws it in the fire. Susheela picks up the half burnt thread and immerses it in milk. This behavior proved very dear to Kaundinya; pretty soon all his wealth was lost; his relatives deserted him; his cattle died and he now knew that this was on account of his rudeness to Anantha Padmanabha swami in the form of the red thread.
</p>
<p style="text-align: justify;">
	Kaundinya leaves his house in search of Anantha Padmanabha. Like a demented person he enters the jungle. After some days he falls down. The Lord in His infinite compassion comes there in the form of an old Brahmin, revives him and takes him to a palace where he shows him His four armed form along with Mahalakshmi. Kaundinya praises the Lord in many stotras. The Lord pleased gives him three boons, viz., removal of poverty, ability to follow dharma, mukti saubhagya.
</p>
<p style="text-align: justify;">
	Kaundinya returned home and did the puja with shraddha and bhakti. He lived a life of wealth and happiness. In the bygone days, sages like Agastya, kings like Janaka, Sagara, Dilipa, and Harishchandra have performed this vrata.</p>
</div>
</div>
