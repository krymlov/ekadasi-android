﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Sri Isvara Puri - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
<div style="text-align: justify;">"Sri Krsnadasa Kaviraja Gosvami
has described in Sri Caitanya Caritamrta that the first sprout of
the desire tree of devotion was manifested in the person of Sri
Madhavendra Puri, and that that sprout developed into a sapling in
the person of Sri Isvar Puri. Then, in the person of Sri Caitanya
Mahaprabhu, Who was also the gardener Himself, that the sapling
become the trunk of an enormous tree- the desire tree of devotion."
- <span style="font-weight: bold;">[C. C. Adi 9.10-11]</span><br>
<br>
"Sri Isvara Puri appeared in this world on the full moon day of the
month of Jyestha. He served his guru, Sri Madhavendra Puri, very
faithfully, especially during the end of Sri Puripada's life." -
<span style="font-weight: bold;">[C. C. Antya
8.26]</span><br style="font-weight: bold;">
<br style="font-weight: bold;">
"Sri Isvara Puri, in his <span id="IL_AD9" class="IL_AD">travels</span> to various holy places, once came to
Sridhama Navadwipa, where he stayed in the house of Sri Gopinatha
Acarya. At that time Sri Nimai Pandita was absorbed in His pastimes
of learning. Iswara Puri entered Nadiya nagara in disguise and thus
no one could understand who he really was."<br>
<br>
"That noble minded gentleman, was always absorbed in the mellows of
devotion to Sri Krsna. He was thus extremely dear to Sri Krsna and
he was an ocean of mercy. No one, however, could recognize him by
his dress. By the will of fate, he came to Sri <span id="IL_AD5" class="IL_AD">Advaita</span> Acarya's house." <span style="font-weight: bold;">[C. B. Adi 11]</span><br>
<br>
He came to where Sri Advaita Acarya was engaged in worshipping Sri
Krsna and quietly sat down there. By his divine luster one
Vaishnava cannot remain hidden from another. Advaita Acarya began
to look in his direction again and again. Finally he asked him:
"Bap! Who are you? I have a feeling you are a Vaishnava
sannyasi."<br>
<br>
Sri Isvara Puri very humbly replied: "I am a low class shudra, come
to view your lotus feet." Mukunda Datta, who was also present could
also understand that this is a Vaishnava sannyasi and thus he
contrived to expose him. In his sweet voice, he began a kirtana
describing Sri Krsna's pastimes. When Sri Isvara Puri heard that
kirtan he at once fell to the ground, and the earth beneath him
became wet with his tears. The devotees present were dumbstruck.
"We haven't seen a Vaishnava like this before."<br>
Advaita Acarya very firmly embraced him. Now everyone could
understand that this is Madhavendra Puri's dearmost disciple, Sri
Isvara Puri. Loud shouts of "Hari! Hari!" rose up into the air.<br>
<br>
Sri Isvara Puri remained at Navadwipa for some days. One day when
Nimai Pandita was returning home from the <span id="IL_AD10" class="IL_AD">school</span>, he happened by chance to meet Sri Isvara
Puri on the way. Sri Isvara Puri was as though mesmerized while
looking at the son of Saci, and thought to himself, "He has all the
indications of being a divine personage, and appears very grave
indeed."<br>
Srila Isvara Puri inquired, "Oh Viprabori ! May I know your name?
Where do you live? What is that manuscript in Your hand?"
Mahaprabhu very humbly offered His namaskar. Some of his disciples
replied, "His name is Nimai Pandit." Exclaiming, "You are the
famous Nimai Pandita!" Isvara Puri felt very happy to meet Him.
Mahaprabhu, coming forward and bowing his head, humbly requested,
"Sripada, please come with Me to My house and be My guest for
lunch."<br>
<br>
Isvara Puri thought to himself, "What a pleasant demeanor He has!"
As though charmed by some <span id="IL_AD4" class="IL_AD">mantra</span> he quietly came along with the Pandit to His
house. Arriving there, Mahaprabhu personally washed his feet, while
in the Deity room, Saci Mata offered the various preparations that
she had prepared to the Lord. Then Mahaprabhu served that prasadam
to Sri Isvara Puri and afterwards accepted his remnants. Thereafter
they sat in the temple (Visnu grha) and discussed Krsna-katha, by
which they both became filled with ecstatic love.<br>
<br>
Sri Iswara Puri thus remained at Navadwipa for a few months,
<span id="IL_AD11" class="IL_AD">staying</span> at the house of Sri
Gopinatha Acarya. Everyday Mahaprabhu would come to have darshan of
his lotus feet and occasionally He would invite him for lunch. At
that time Srila Gadadhara Pandita was just a young boy and Isvara
Puri was very affectionate to him. He began to teach him from the
book which he had composed "Sri Krsna Lilamrta".<br>
<br>
One evening when Mahaprabhu had come to offer His obeisances to
Isvara Puri, Srila Puripada spoke to him, "You are a great pandita.
I have composed a manuscript about Sri Krsna's pastimes. If you
would please hear it, then I can recite it before You, and You can
correct any mistakes that there might be. I will be very pleased if
You do this."<br>
<br>
Mahaprabhu replied smilingly, "Whatever a devotee speaks is
dictated by Sri Krsna Himself. If anyone sees any fault in this
then he is simply a <span id="IL_AD2" class="IL_AD">sinful</span>
wretch. Whatever poems he composes, certainly Krsna is very pleased
by that. Krsna accepts the mood in which things are offered as the
most substantial part of the offering." To Isvara Puri these words
were like drops of nectar and he could understand that Sri Nimai
Pandita was an extraordinary person. After passing some days in
Nadia, Isvara Puri continued his <span id="IL_AD8" class="IL_AD">tour of</span> the holy places.<br>
<br>
Meanwhile Mahaprabhu was bringing His pastimes of learning to a
close and now desired to reveal His true Self and establish the
religion of the age by distributing love of Godhead through the
chanting of His Holy Names. Thus He came to Gaya, ostensibly to
offer pinda for His departed father and fore­fathers. Sri Isvara
Puri was present at that time at Gaya. After having offered pinda
at the various holy shrines, he finally came to the place where the
lotus footprints of Sri Visnu are. While taking darshan and hearing
of the glories of the shrine Mahaprabhu fainted and fell to the
ground. By the arrangement of Providence, Sri Isvara Puri happened
to come to that spot just then and learned from Sri Candrashekar
Acarya what had transpired. After a short time Mahaprabhu regained
His consciousness, and seeing Isvara Puri He got up to offer His
obeisances. Sri Isvara Puri embraced Him and the two of them were
drenched by each others tears of love.<br>
<br>
Mahaprabhu addressed Isvara Puri, "My journey to Gaya is successful
just by My seeing your lotus feet. If one offers Pinda at this holy
place, then his forefathers become delivered. But simply by seeing
you, tens of millions of forefathers get liberation. Therefore your
presence is even more auspicious than that of this holy tirtha. All
of the holy tirthas pray for the dust of your lotus feet.
Therefore, O Puripada, I am praying at your lotus feet to ferry me
across the ocean of material existence and to cause me to drink the
nectar from Krsna's lotus feet."<br>
<br>
Srila Isvara Puri replied, "Please <span id="IL_AD7" class="IL_AD">hear me</span> Panditji, I have understood that You are an
incarnation of the Supreme Lord. This morning I saw a very
auspicious dream and now that has actually materialized. From the
first day I saw you at Navadwipa, I have always thought of You. I
get such pleasure by seeing You, as much pleasure as I get by
seeing Krsna."<br>
<br>
Hearing this, Mahaprabhu bowed his head and smilingly replied,
"This is my great <span id="IL_AD6" class="IL_AD">fortune</span>."<br>
<br>
On another day Mahaprabhu approached Sri Isvar Puri and requested
that he initiate Him with the divine mantra. "My mind is becoming
very restless in anticipation of this initiation. "Srila Puripada
very blissfully replied, "What to speak of mantras, I am prepared
to offer You my very life." <span style="font-weight: bold;">[C. B.
Adi 17.10]</span><br>
<br>
Thereafter Srila Isvara Puri initiated Mahaprabhu with the divine
mantra.<br>
<br>
One morning Srila Isvara Puri came to where Mahaprabhu was staying.
Mahaprabhu was extremely pleased to see him and after offering His
obeisances He invited him to stay for lunch. Isvara Puri replied
that, "Being able to accept foodstuffs from Your hand is a matter
of great fortune for me." Mahaprabhu Himself cooked and then very
carefully served His guru the prasadam. Afterwards he smeared
sandalwood paste on his body and put a garland of flowers around
his <span id="IL_AD3" class="IL_AD">neck</span>. Thus the Supreme
Lord Himself taught how one should serve his guru. Without serving
the great devotees, it is not possible to receive love of Godhead.
Service to the guru is the door to devotion.<br>
<br>
On his return from Gaya, Mahaprabhu came by way of Kumarhatta, the
birthplace of His guru, and began to roll on the ground in ecstasy
there, as the ground became wet with His tears. Finally He
collected some dust from that holy place and bound it in the corner
of his upper garment, saying, "This dust is as dear to me as My
life." then he set out for Navadwipa.<br>
<br>
The birthplace of Srila Isvara Puri is located within the present
town of Halyahor, which is near the Kanchra-para rail station,
which is on the Sealdah -Krsnanagar line. One should get down at
Kanchra-para station and then proceed by rickshaw to Caitanya Doha.
Doha means pond, which is what was created when many followers of
Caitanya Mahaprabhu, following in His footsteps, came here and
collected earth from the birthplace of Srila Isvara Puri.<br>
<br>
Thereafter Mahaprabhu accepted sannyasa and by the order of His
mother came to live at Jagannath Puri. By this time Isvara Puri had
already left this world. He sent two of his disciples Sri Govinda
and Kasisvara brahmacari to serve the Lord at Nilacala.</div>
<div><img class="align-center" src="https://api.ning.com/files/JlLdkIDjEXR4qF2PDR5AMct4IXWj46Nx-iEq55OHKa0euX4iJsJyWs*fpt-8am3TdOvI-hQY7As6hppySJXWi4pc*MRwz4nM/35.png?width=125" width="125"></div></div></div>
</div>
</div>
</div>