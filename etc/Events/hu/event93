﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Sri Gadadhara Pandita - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
<div>Sri Gadadhar was the consant companion of Mahaprabhu from the
time of their childhood. His father's name was Sri Madhva Misra and
his mother's name Sri Ratnavati-devi. They lived very near the
house of Sri Jagannatha Misra in Mayapura. Ratnavati-devi thought
of Saci-devi as her own sister, and always used to visit her.</div>
<div><br></div>
<div>During their childhood, Sri Gaura Hari and Gadadhara would
play together, sometimes at Mahaprabhu's house and sometimes at
Gadadhara's house. They both studied together at the same school.
Gadadhara was a few years younger then Nimai. Nimai couldn't remain
without Gadadhara even for a moment and Gadadhara likewise couldn't
stand to be separated from Nimai.</div>
<div><br></div>
<div>In the Gaur-ganoddesa-dipika, it is described that that person
who in Vraja was the daughter of Sri Vrsabhanu Raja, namely Srimati
Radharani, is now celebrated as Sri Gadadhara Pandita.</div>
<div><br></div>
<div>Srila Svarupa Damodara has written in his diary: avani sura
bavah Sri Panditakhyo jatindrah / sa khalo bhavati Radha Srila
Gauravataro.</div>
<div><br></div>
<div>Sri Vasudeva Ghosa Thakura has also written:</div>
<div><br></div>
<div>Agam agocar gora akhil brahma-par, veda upar, najane pasandi
mati bhora Nitya-nityananda CaitanyaGovinda Pandit Gadadhar Radhe
Caitanya yugala-rupa kebol raser kup avatar sadsiva sadhe Antare
nave-ghana bahire gaura tanu yugalrupa parkase Kahe Vasudeva Ghose
yugal bhajan base janame janame rahu asa</div>
<div><br></div>
<div>"Lord Gaurasundara, who is beyond the perview of the
scriptures, beyond the entire Brahman, and above even the Vedas,
can never be known by the atheists whose intelligence is dull. Lord
Nityananda is His eternal self. Lord Caitanya is Lord Govinda
Himself and Pandita Gadadhara is none other than Sri Radha. The
divine couple, who are present in Sri Caitanya, are a well of rasa.
Advaita Acarya (Sadasiva) has prayed for His descent.</div>
<div><br></div>
<div>Within He is blackish but of a golden hue without, the
manifestation of the divine couple. Thus Vasudeva Ghosa sings of
the beauty of this divine couple - Sri Gaura-Gadadhara - in whose
worship he has been completely subjugated. He prays that he will
desire to serve them birth after birth."</div>
<div><br></div>
<div>In Sri Caitanya-caritamrta we likewise find:</div>
<div><br></div>
<div>Panditer bhav mudre kahan na jay gadadhar pran nath nam hoila
jay panditer krpa prasad lehan na jay gadai gauranga kari sarvaloke
gay</div>
<div><br></div>
<div>"The emotions and expressions of Pandita Gadadhara are not
possible to describe. Another name of Lord Gauranga is the 'Lord of
the life of Gadadhara.' Who can understand what mercy has been
bestowed upon him? Their glories are sung by everyone as Gadai-
Gauranga."</div>
<div><br></div>
<div>During the time when Sri Isvara Puri was present for a few
months in Navadwipa at the house of Sri Gopinath Acarya, he taught
Gadadhara from the book he had composed, Sri Krsna Lilamrta.</div>
<div><br></div>
<div>Gadadhara was from his very childhood very serene, patient,
calm, quiet, fond of solitude and very renounced. Nimai Pandit
during the time of His precocious youth would ask his fellow
studentsmeaningless, fallacious questions in logic. Gadadhara
however, was not especially fond of this pastime, and therefore he
sometimes used to remain at some distance from Nimai. But Nimai
wouldn't allow him to get away. He would tell him - "Gadadhara! In
a very short time I'll become such a Vaisnava that Lord Brahmaand
Lord Siva themselves will come to My door."</div>
<div><br></div>
<div>Sri Gadadhara Pandita was very affectionate towards Mukunda
datta. Whenever any Vaisnava came to Navadwipa, Mukunda would
inform Gadadhara and they would both go to have darsana. One time
Pundarika Vidyanidhi came from Cattagram to Navadwipa and Mukunda
invited Gadadhara to come along and meet him. Gadadhara was very
excited to meet such a Vaisnava and thus the two of them happily
set out together to take advantage of sadhu-sanga with the famous
Pundarika Vidyanidhi.</div>
<div><br></div>
<div>However, when Gadadhara saw that Pundarika Vidyanidhi dressed
and acted like a wealthy materialist, he lost whatever reverence he
had previously felt even before speaking with him. Gadahara thought
to himself, "How can a Vaisnava look and act as if he was so
addicted to sense enjoyment?"</div>
<div><br></div>
<div>However, Mukunda knew the real character of Pundarika
Vidyanidhi, and he could also sense the doubts in the mind of
Gadadhara Pandita. Thus he recited some slokas from the Srimad
Bhagavatam in a very sweet voice. When Pundarika Vidyanidhi heard
his beautiful recitation of these slokas, in a fit of ecstasy he
began to cry while calling, "Krsna, Krsna", and finally fainted
dead away on the floor. [Chaitanya Bhagavat. Madhylila
7.78-79].</div>
<div><br></div>
<div>Gadadhara now felt very remorseful in his mind. He thought to
himself, "Because I have ignorantly considered this highly advanced
soul to be an ordinary materialist, what an offense I have
committed! In order that I might be saved from the reaction of this
offense, I think the only solution is to accept initiation from
him."</div>
<div><br></div>
<div>Gadadhara Pandita submitted his proposal to Mukunda, who
presented it to Pundarika Vidyanidhi with a full account of the
pandita's high qualifications. "Hearing this proposal, Pundarik
became very happy. 'Providence has bestowed upon me a great jewel;
certainly I will accept him. You shouldn't have any doubt about
that. It is the result of many lifetimes of good fortune that one
gets a disciple like this.'" [Chaitanya Bhagavat. Madhylila
7.117-118]. On an auspicious day, Sri Gadadhara Pandita received
the divinemantra from Pundarika Vidyanidhi.</div>
<div><br></div>
<div>Sri Caitanya Mahaprabhu, upon going to Gaya Dham, first began
to manifest Krsna-prema. There he enacted the pastime of accepting
the shelter of Sri Isvara Puri. After returning to His house He
began to exhibit a new life.</div>
<div><br></div>
<div>Day and night He floated in the ocean of love of Krsna.
Gadadhara, upon seeing the Lord showering of tears in love for
Krsna, also began to cry in ecstatic love. From this time,
Gadadhara Pandita was always at Mahaprabhu's side.</div>
<div><br></div>
<div>One day Sri Gadadhara brought some pan to Sri Gauranga, Who,
in an exuberance of emotion asked him, "Gadadhara! Where is that
beautiful blackish boy Who is dressed in yellow garments?" After
asking this, He began to shed tears.</div>
<div><br></div>
<div>Gadadhara couldn't immediately think of what to say. He
finally respectfully suggested, "He is in your heart." Hearing
this, Mahaprabhu began to tear at his chest with his nails, but
Gadadhara quickly grabbed his hands. Prabhu said to him,
"Gadadhara! Let go of My hands. I cannot remain a moment more not
seeing Krsna."</div>
<div><br></div>
<div>Gadadhara replied, "Just try to be a little patient and calm
Yourself. Krsna will come any minute now."Hearing this, Mahaprabhu
became somewhat composed.</div>
<div><br></div>
<div>Sacimata heard all this from a distance and then came running
there. Being very pleased with Gadadhara, she declared, "Though He
is just a boy, He is very intelligent. I am frightened to go before
my son when He is in these moods. But Gadadhara has very cleverly
pacified him."</div>
<div><br></div>
<div>"Sacimata told Gadadhar, 'Gadadhara! You must please remain
with Nimai always. If you stay with Him then I won't have to
worry."</div>
<div><br></div>
<div>One day, Sri Gadadhara, having heard that Mahaprabhu will
speak Krsna-katha at Suklambara Brahmacari's house, went there and
sat down inside. Meanwhile, Mahaprabhu arrived outside on the
verandah and began to speak about Krsna to an audience of devotees
there; Gadahara Prabhu listened from within the house. Soon
Mahaprabhu became totally absorbed in the ecstacy Krsna-prema,
which quickly spread to the assembly of devotees. For some time
they continued to taste the mellows of love of Krsna together and
then becalmed themselves.</div>
<div><br></div>
<div>Gadadhara, however, couldn't restrain his ecstacy. With his
head bowed down, he continued to cry very loudly. Hearing his
pitiful sobbing, Mahaprabhu inquired, "Who is crying within the
room?" Suklambara Brahmacari replied, "Your Gadadhara."</div>
<div><br></div>
<div>Mahaprabhu exclaimed, "Gadadhara! You are so pious and
virtuous. Since your very childhood you were so devoted to Krsna.
My life has been wasted. Due to my previous evil activities I
cannot get the association of the Lord of my life, Sri
Krsna."</div>
<div><br></div>
<div>Saying this Prabhu embraced Gadadhara in great love.</div>
<div><br></div>
<div>When Mahaprabhu began His pastimes in Navadvipa, Gadadhara was
his chief companion. Radha-Krsna of Vraja are now sporting on the
banks of the Ganga as Gadai-Gauranga, and the cowherd boy-friends
of Vraja are assisting in His pastimes of kirtana.</div>
<div><br></div>
<div>One day after roaming throughout the town of Navadwipa,
Mahaprabhu came to a forest and seated Himself there. Then He began
to remember His pastimes in Vraja. Mukunda began to sing the
purva-vag (courtship) songs in a very sweet voice while Gadadhara
collected some flowers from that forest and after stringing a
garland placed it on Prabhu's neck. Previously, in the way that Sri
Radha used to decorate Sri Krsna, Gadadhara began to dress Prabhu
in that exact same manner. Some were singing beautiful songs, while
others began to dance in a very graceful style. Then Sri
Gaurasundara taking Gadadhara by His side seated Himself on a dias
at the base of one tree, while Adwaita Acarya began to offer arati.
Nityananda seated Himself on Prabhu's right side and Srivasa
Pandita began to decorate everyone with flower garlands. Narahari
was fanning that Divine couple with a whisk made from the tail
hairs of forest cows. Suklambar decorated them all with sandalwood
paste while Murari Gupta recited the premdhani. Madhava, Vasudeva,
Purusottama, Vijaya, and Mukunda began to sing songs in the various
ragas.</div>
<div><br></div>
<div>Then Prabhu closed his pastimes in Nadiya and as per his
mother's request he took up his residence at Nilacala. Gadadhara
also went there to live at this time. He engaged himself in the
service of Sri Tota Gopinatha. Mahaprabhu would very often visit
His dear friend Gadadhara and lose Himself in discussions on the
topics of Sri Krsna. The temple of Sri Tota Gopinathji is located
near the ocean at Yameshvar.</div>
<div><br></div>
<div>When Lord Caitanya traveled to Sri Vrindavana, Gadadhara, not
being able to bear separation from Him, wanted to go also but
Mahaprabhu reminded him of his vow of Ksetra-sannyasa (by which he
was to remain constantly in Sri Ksetra in the service of Sri
Gopinatha) and sent him back.</div>
<div><br></div>
<div>Sri Gadadhara Pandita would regularly recite Srimad
Bhagavatam. Sri Gaurasundara, along with His associates, would
listen.</div>
<div><br></div>
<div>Then one might ask a question, if Lord Chaitanya is Radha and
Krsna combined, where is the necessity for the appearance of
Gadadhara Pandit?</div>
<div><br></div>
<div>The answer to this is that it has been explained that
Mahaprabhu is Krsna in search of the loving sentiment of Radha for
Krsna. Sri Gadadhara Pandita represents that bhava personified, and
thus he has appeared to be at the side of Mahaprabhu to assist him
in his search. In this explanation, Gadadhara gives his unalloyed
love to Lord Gauranga willingly.</div>
<div><br></div>
<div>It has also been explained that Krsna has stolen the bhava of
Radha and appears as Mahaprabhu, Radha Krsna combined. Gadadhara
represents that which is left of Radha after Krsna steals her
bhava. The mood is that seeing that neither Sri Radha nor Sri Krsna
appear on the purnima tithi (full moon), but when The Divine couple
combine as Sri Caitanya Mahaprabhu, this appearance in the world
occurs on the Purima in Navadwipa.</div>
<div><br></div>
<div>Sri Gadadhara pandit appears on the new moon and leaves the
world on the same day. New moon means no moon - dark moon or
Amavasya tithi. He also appeared in a land that is desert-like, dry
and vacant in contrast to the lush Ganga basin of Navadwipa.</div>
<div><br></div>
<div>All of this indicates the condition of the heart or nature of
Sri Gadadhara. He is thus feeling empty, and lets everyone know
about it too. It is said that what remains in him is the bhava of
Rukmini, who is Radha's expansion in Dvaraka. Thus his surface mood
in Gaura lila is like that of a dakshine (right wing) gopi rather
than a vama (left wing) gopi (Vama type is little aggressive in
nature, and fights with the lover). However, to prove Gadadhar's
dedication as right-wing He is submissive to the extreme (daksina
type tolerates everything whatever comes, only with a defensive
attitude he stands. That is daksina path, that is as in Srimati
Rukmini Devi. So when the spirit of Radharani of that opposing
nature was drawn out by Mahaprabhu, then what remains is compared
like that of Rukmini, as a passive seer, without any power to
assert, only onlooker. Onlooker and bearing everything, a very
pitiable condition. The idea is to awaken kindness and sympathy
from everyone).</div>
<div><br></div>
<div>"What is he and what is she and what is her position now. And
how her lover has taken up everything from her, looted everything
from her, leaving her as a beggar wandering in the street. So much
looted. Radharani when looted to such a degree by Krsna, that
becomes Gadadhara, the pitiable figure. The wealth of course cannot
go forever. She is the proprietor, owner cannot be far off. After a
long time, that must come to her again, one day. And those that are
helping her in her day of distress, they will get how much
remuneration at that time. When she will get back all
property."</div>
<div><br></div>
<div>Just as in Vraj Srimati Radharani is the Queen and proprioter
of everything, so Gadadhara is master of everything, but still here
He has given everything to His master, and thus in one sense He is
empty. This mood of His being empty is unique, he is playing in
such a way, as His part or play, as Gadadhara Pandita. His is the
highest position of self-sacrifice.</div>
<div><br></div>
<div>"Nimai Pandita showed His character as an aggressor,
impertinent, and an extraordinary genius. Gadadhara Pandita was
just the opposite. He had some natural inclination, that was in His
submission and total dedication towards Nimai Pandita. And Nimai
Pandita also had some special attraction for Gadadhara Pandita. But
Gadadhara Pandita could not face Nimai Pandita directly. Some sort
of shyness He felt about Nimai Pandita. So, this is what we know
about Gadadhara Pandita. Gadadhara Pandita had very intimate
relationship with Mahaprabhu, in which the acaryas, Swarupa
Damodara, Rupa, Sanatana, Kaviraja Goswami, Raghunatha Dasa, all of
them, could see Radharani and Rukmini both in his personality, and
according to that we can try to understand him."</div>
<div><br></div>
<div>It is true that Gauranga Mahaprabhu is Vrajendranandana Krsna,
within Him, as is the case with svayam bhagavan Krsna, all of His
expansions are present. Thus as Gaura-Narayana He consorts with
Rukmini in Her appearance as Laksmipriya devi. Furthermore, just as
Vrajendranandana Krsna never leaves Vrndavana, similarly
Sacinandana Gaura Krsna never leaves Navadvipa Dhama.</div>
<div><br></div>
<div>Thus the lila of Mahaprabhu in Puri, which is representative
of Dvaraka, is imbued with aisvarya (opulence) not found in Nadiya
(Bengal). Indeed this aisvarya is said to have weighed heavily on
Gadadhara Pandita and influenced His loving sentiment of bhava.
Srila Krsna dasa Kaviraja Goswami says of Gadadhara in Puri Dhama
that because He was influenced by aisvarya-jnana of Gauranga, He
was, as Srila Prabhupada comments further, always submissive
(daksina-nayiki). He could not be angered by Mahaprabhu even when
Mahaprabhu tried to anger Him and see the mood of Radha
(vama-nayiki) in Him.</div>
<div><br></div>
<div>Krsna dasa Kaviraja pointed out that even as Krsna in the form
of Mahaprabhu tried to anger Gadadhara (Radha) He could not draw
out this sentiment from Him because he maintained a temperament
like that of Rukmini's (daksina-nayiki) due to the fact that
Mahaprabhu had already stolen the vama-nayiki aspect of Radha from
him. Indeed, Svarupa Damodara Goswami asked Gadadhara why He did
not reproach Mahaprabhu and instead tolerated Mahaprabhu's
criticisms of Him. To this Gadadhara replied, prabhu svatantra
sarvajna-siromani tanra sane 'hatha' kari,--bhala nahi mani, "The
master is independent. He is the topmost omniscient person. If I
speak on an equal level with Him, this will not be good for
Me."</div>
<div><br></div>
<div>The above opinion is one that is shared by the late Om
Visnupada Bhakti Pramode Puri Goswami. In his book, 'Heart of
Krsna' that contains a series of articles originally written in
Bengali during the manifest presence of Srila Bhaktisiddhanta
Saraswati Thakura, Puri Goswami Maharaja states, "That which is
left over after the bhava and luster of Radha has been plundered by
Krsna is Gadadhara Pandita."</div>
<div><br></div>
<div>It is stated that no one loves Lord Gauranga more than Sri
Gadadhara. Yet even though He takes much abuse and neglect from
Gauranga, still He cannot leave Him. His necessity and dependency
is very great. The understanding is that without Mahaprabhu She/He
is transcendentally incomplete, thus He/She wants to get His/Her
bhava back from Krsna to again become complete, the complete united
whole Radha-Krsna. To unite Gaura with Gadadhara (Radha Krishna) is
the highest ideal of Gaudiya Vaisnavism.</div>
<div><br></div>
<div>The worship of Gaura-Gadadhara is in madhurya-rasa so is not
for the kanistha adhikari on the beginning stages of devotional
service where one strives to follow the principles and avoid
offences, rather this is for advanced devotees who are already
steady (nistha) in Krsna consciousness and getting a genuine taste
(ruchi). Then in transcendental view They are worshipped like Radha
and Krishna. Thus Sri Gadadhara pandit is like Sri Radha standing
next to the Gaura (Krsna) who has stolen Her/His bhava (love), and
She wants it back. When They are united, then the divine couple
Radha-Krsna/Gaura-Gadadhara are present. Sometimes the devotees
will see them as Radha-Krsna, and sometimes as Gaura-Gadadhara
according to that pure devotee's mood. Bhaktivinoda Thakura has
written an arati song for Gaura-Gadadhara describing Radha-Krsna.
This is the internal reality. It is all very esoteric and not to be
imitated. Who can understand such wonderful and intimate pastimes,
thus we use ordinary terms to try to explain them but these mellows
are not ordinary.</div>
<div><br></div>
<div>When Vakresvara pandita came to live at Puri, he would
accompany Sri Chaitanya Mahaprabhu, Advaita Acharya and other
devotees to Tota Gopinath to hear Gadadhara Pandit give discourses
on the Bhagavatam.</div>
<div><br></div>
<div>One should hear the book Bhagavatam from the devotee
Bhagavatam.</div>
<div><br></div>
<div>One who thinks, "I understand the Bhagavatam" does not really
know the Bhagavatam's central meaning. On the other hand, an
ignorant person who takes shelter of the Bhagavatam will directly
perceive its meaning. The Bhagavatam is pure love, it is the
incarnation of Krishna himself. It recounts Krishna's most intimate
activities. After recounting the Vedas and the Puranas, Vedavyasa
felt unsatisfied, but as soon as the Bhagavatam manifested on his
tongue, his mind was immediately satisfied. (Chaitanya Bhagavat
3.3.514-8)</div>
<div><br></div>
<div>smara gaura-gadadhara keli kalam</div>
<div>bhava gaura-gadadhara paksa caram</div>
<div>srnu gaura-gadadhara caru katham</div>
<div>bhaja godruma-kanon kunja-vidhum</div>
<div><br></div>
<div>[Srila Bhaktivinoda Thakura]</div>
<div><br></div>
<div>"Remember Gaura-Gadadhara during the time of Their pastimes,
Meditate on Gaura-Gadadhar, the Divine couple, the par-excellence
of beauty. Hear beautiful discussions on the topics of
Gaura-Gadadhara, and worship them in the forest grove of Godruma,
bathed in the light of the moon."</div>
<div><br></div>
<div>gadadhar pandit prabhu age bosi</div>
<div>pare bhagavat - sudha dhale rasi rasi [B.R. 3/107]</div>
<div><br></div>
<div>After enacting His pastimes on this planet for forty-eight
years, it was seen by witnesses that Sri Gaurasundara entered into
the body of Sri Tota-Gopinathji, who was the Deity in Puri on the
Nilachala beach served by Sri Gadadhara Pandita.</div>
<div><br></div>
<div>nyasi siromani cesta bujhe sadya kar</div>
<div>akasmat prithini karila andhakar</div>
<div>pravesila ei gopinatha mandire</div>
<div>hoila adarsan, punah na aila bahire</div>
<div>[B.R. 8.356-357]</div>
<div><br></div>
<div>In the diary of one Orissan devotee, Mahaprabhu is said to
have first went missing inside the Gundica Mandira. Then the
devotees began to comb the four directions in search of their
Beloved Lord. Finally, outside of Tota Gopinatha Mandira, at
Yameswar, the outer garment of Mahaprabhu was found lying on the
ground.</div>
<div><br></div>
<div>There's a story in connection with Sri Gadadhar Pandit that
when He was very old and trying to render service to Tota-Gopinath
that He couldn't any more reach to put the garland around the neck
of the Deity. To reciprocate with His loving service the Deity
knelt down to accept the flower garlands of His beloved Gadadhar,
Who is none other than His beloved Srimati Radharani incarnate as
mentioned previously.</div>
<div><br></div>
<div>To this day the Deity of Tota-Gopinath in the temple remains
in a kneeling position to accept the eternal service of His beloved
Gadadhar. A slight crack which is still to be found near the right
knee of Sri Gopinathji is the spot where Mahaprabhu is said to have
entered the Deity.</div>
<div><br></div>
<div>His appearance is on the amavasya (new moon) of the month of
Vaisakha.</div>
<div><br></div>
<div>Sri Gadadharastakam by Srila Svarupa Damodara Gosvami</div>
<div><br></div>
<div>Text 1</div>
<div>sva-bhakti-yoga-lasinam sada vraje viharinam</div>
<div>hari-priya-ganagragam cacisuta-priyecvaram</div>
<div>saradha-krsna-sevana-prakacakam mahacayam bhajamy aham</div>
<div>gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned and exalted spiritual master,
Srila Gadadhara Prabhu, who appears very splendid, engaged in the
Lord's devotional service. He always performs pastimes in Vraja,
where he is very prominent among the gopis, who are very dear to
Lord Hari. Lord Caitanya, the Son of Saci is the dear Lord of
Gadadhara Prabhu, who is preaching the service of Radha and
Krsna.</div>
<div><br></div>
<div>Text 2</div>
<div>navojjvaladi- bhavana-vidhana-karma-paragam</div>
<div>vicitra-gaura-bhakti-sindhu-ragga-bhagga-lasinam</div>
<div>suraga-marga-darsakam vrajadi-vasa-dayakam bhajamy</div>
<div>aham gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned spiritual master, Srila
Gadadhara Prabhu. He is expertly absorbed in meditation on the nine
rasas of devotional service, beginning with ujjvala-rasa (conjugal
love), and he dances in the waves of the amazing ocean of
devotional service to Lord Caitanya. He preaches the path of
raganuga-bhakti (spontaneous devotional service), and He is a fit
person to attain residence in the transcendental land of
Vraja.</div>
<div><br></div>
<div>Text 3</div>
<div>sacisutagghri-sara-bhakta-vrnda-vandya-gauravam</div>
<div>gaura-bhava-citta-padma-madhya-krsna-vallabham</div>
<div>mukunda-gaura-rupinam svabhava-dharma-dayakam bhajamy</div>
<div>aham gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned spiritual master, Srila
Gadadhara Prabhu. The best of those who are devoted to the lotus
feet of Lord Caitanya, the Son of Saci-devi, offer respecful
obeisances to him and treat him with great importance, and he is
very dear to Lord Krsna, Who is seated in the middle of the lotus
flower which is his heart, assuming His golden form of Caitanya
Mahaprabhu. He preaches that Lord Mukunda has assumed the golden
form of Lord Caitanya, and he returns the living entities to their
constitutional position as servants of the Lord.</div>
<div><br></div>
<div>Text 4</div>
<div>nikunja-sevanadika-prakasanaika-karanam sada</div>
<div>sakhi-rati-pradam maha-rasa-svarupakam</div>
<div>sadacritagghri-pankajam sariri-sad-gurum varam bhajamy</div>
<div>aham gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned and exalted spiritual master,
Srila Gadadhara Prabhu. It is only because of him that the truth of
the exalted devotional service performed by the intimate associates
of Krsna in the forests and groves of Vrndavana has been revealed.
He is preaching the ecstatic love attained by the gopis, and he is
indeed a personification of the mellows of devotional service. The
saintly devotees take shelter of his lotus feet, and he teaches the
truth of spiritual life to the living entities.</div>
<div><br></div>
<div>Text 5</div>
<div>mahaprabhor maha-rasa-prakasanagkuram priyam sada</div>
<div>maha-rasagkura-prakacanadi-vasanam mahaprabhor</div>
<div>vrajagganadi-bhava-moda-karakam bhajamy aham</div>
<div>gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned spiritual master, Srila
Gadadhara Prabhu. He is very dear to Lord Caitanya Mahaprabhu, and
it is as if he had sprouted from the manifestation of the Lord`s
ecstatic love. He is always adorned with the garments of ecstatic
love of Godhead, and He delights Lord Caitanya Mahaprabhu by
arousing in the Lord the ecstatic emotional love experienced by the
gopis of Vraja.</div>
<div><br></div>
<div>Text 6</div>
<div>dvijendra-vrnda-vandya-pada-yugma-bhakti-vardhakam</div>
<div>nijesu radhikatmata-vapuh-prakacanagraham</div>
<div>asesa-bhakti-sastra-siksayojjvalamrta-prabam bhajamy</div>
<div>aham gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned spiritual master, Srila
Gadadhara Prabhu, who expands the mellows of devotional service and
whose lotus feet are worshipped by the kings of the brahmanas.
Among his confidential associates, he reveals his actual form as
Srimati Radharani. He distributes the nectar of the ecstatic mellow
of the gopis conjugal love, strictly following the instructions of
all the devotional scriptures.</div>
<div><br></div>
<div>Text 7</div>
<div>muda nija-priyadika-svapada-padma-sindhubhir</div>
<div>maha-rasarnavamrta-pradesta-gaura-bhaktidam</div>
<div>sadast-sattvikanvitam nijesta-bhakti-dayakam bhajamy</div>
<div>aham gadadharam supanditam gurum prabhum</div>
<div><br></div>
<div>I worship the greatly learned spiritual master, Srila
Gadadhara Prabhu. With delight he is giving the devotional service
of Lord Caitanya which reveals the nectarean ocean of the most
exalted mellows of devotional service, along with the nectar
flowing from the lotus feet of the gopis, headed by Srimati
Radharani, who is most dear to Krsna. Gadadhara Prabhu is decorated
with the eight transcendental ecstatic symptoms of
sattvika-bhava,and he is distributing devotional service to his
worshippable Lord Krsna.</div>
<div><br></div>
<div>Text 8</div>
<div>yadiya-riti-raga-ragga-bhagga-digdhamanaso naro `api</div>
<div>yati turnam eva narya-bhava-bhajanam tam</div>
<div>ujjvalakta-cittam etu citta-matta-catpado bhajamy aham</div>
<div>gadadharam supanditam guram prabhum</div>
<div><br></div>
<div>I worship the greatly learned spiritual master, Srila
Gadadhara Prabhu. Even a conditioned soul, whose mind becomes
anointed by the colorful waves of spontaneous devotional service as
delineated by Srila Gadadhara Prabhu, quickly attains the highest
level of devotional service. He becomes like a maddened bumble bee
whose mind is anointed with the mellow of ujjvala-rasa (conjugal
love).</div>
<div><br></div>
<div>Text 9</div>
<div>maha-rasamrta-pradam sada gadadhara-astakam pathet tu</div>
<div>yah subhaktito vrajaggana-ganotsavam</div>
<div>saci-tanuja-pada-padma-bhakti-ratna-yogyatam labheta</div>
<div>radhika-gadadharagghri-padma-sevaya</div>
<div><br></div>
<div>"These eight verses glorifying Srila Gadadhara Prabhu
distribute the nectat of the most exalted mellow of devotional
service, and they are like a festival for the gopis of Vraja. If
one regularly reads these verses and serves the lotus feet of Srila
Gadadhara Prabhu, who is an incarnation of Srimati Radharani, then
he will become qualified to attain the jewel of pure devotional
service for the lotus feet of Lord Caitanya,the son of
Sacidevi."</div></div></div>
</div>
</div>
</div>