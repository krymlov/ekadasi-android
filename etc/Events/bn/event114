﻿<div class="xg_column xg_span-20 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Srila Madhvacarya - Appearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated"><div style="text-align: center;"><object height="394" width="500" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" _origwidth="500"><img src="https://api.ning.com/files/T6yG7eB6nKkwKKQi4U52HABBax5O1t3vUAZA52Ij7A4BXOTR1PD8JBNW7rYd9S9SVwNoleVlW40CC6cXA3Ye-*ccVpXpyqf2/Madhvacharya_App.jpg?width=500" width="500"><br>
<param name="allowscriptaccess" value="never"></object></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;">For twelve years Madhyageha Bhatta would regularly travel the eight miles north from his village of Belle to Udupi.&nbsp;</span><span style="font-family: Arial; font-size: 14px;">There at the Anantesvara temple he would pray for a son.</span></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br></span></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
One day a devotee in a trance like state climbed the temple flagpole and announced that to reestablish the purest principles of religion, a male child, an incarnation of Vayu, the demigod in charge of air, would soon be born.&nbsp;Madhyageha understood within his heart that this would be his own child.&nbsp;Soon his wife, Vedavati, gave birth to a son.&nbsp;The happy couple named him Vasudeva.</span></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
<br>
From infancy Vasudeva showed extraordinary intellect, so much so that he was given brahminical initiation at age five, three years early.&nbsp;Whatever he heard or read, even just once, he could remember.&nbsp;His body was unusually strong, lustrous, and beautiful.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
At age eleven, Vasudeva left home for Udupi, to live with Acyutapreksa, an ascetic widely respected for his scholarship and saintly character.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
After one year, despite strong protests from his father, Vasudeva renounced the world.<br>
Acyutapreksa named him Purnaprajna.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
Less than forty days after taking sannyasa, Purnaprajna defeated Vasudeva Pandita, a famous wandering scholar, in public debate.&nbsp;The pandita was known for his hair-splitting dialectical ability, but he was no match for the young Purnaprajna.&nbsp;The pandita spoke for three days and then dared anyone to refute his conclusions.&nbsp;Purnaprajna shocked the crowd when he accepted the challenge.&nbsp;First, to show he had a full grasp of the issues, he repeated almost verbatim the pandita's arguments.&nbsp;Then, one by one, he smashed them all.&nbsp;His victory was the talk of Udupi.&nbsp;Acyutpreksa gave him the title Anandatirtha, in recognition of his mastery of Vedanta.</span></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
<br>
Word spread far and wide about the debating skill of the young ascetic in Udupi.&nbsp;Challengers and admirers converged on the town.&nbsp;Buddhisagara and Vadisimha, two Buddhist monks who had converted many to their fold, challenged Anandatirtha.&nbsp;After a day-long skirmish, they promised to return the next day.&nbsp;That night, however, they secretly fled from Udupi.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
Anandatirtha went on a tour of South India.&nbsp;The most notable events on this tour were two encounters with Vidyasankara Swami, the lineal successor to Sripad Sankaracarya, who was the original propounder of the monistic theory of the Absolute Truth.&nbsp;</span></div>
<div align="justify"></div>
<div align="justify"><span class="font-size-2"><strong><span style="font-family: Arial; font-size: 14px;">Some basic tenets of Sankaracarya's philosophy are as follows :</span></strong></span></div>
<div align="justify"><span class="font-size-2"><strong><span style="font-family: Arial; font-size: 14px;"><br></span></strong></span></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;">God and the soul are identical; the formless, senseless, impersonal Absolute is the only reality; all else is illusion; and the incarnations of God are all products of illusion.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
Anandatirtha was toughly familiar with this philosophy, so he knew all its weak points.&nbsp;With firmness and courage he challenged the venerated Vidyasankara, and a fierce debate ensued.&nbsp;Vidyasankara could not defeat his opponent, yet he refused to accept defeat.&nbsp;They met again, in Ramesvaram, during the monsoon season, at which time Vidyasankara taunted and harassed Anandatirtha.&nbsp;But the young saint tolerated the abuse.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
On his return journey, while addressing an assembly of learned men, Anandatirtha stated that every Vedic utterance conveyed a triple meaning, that each verse of the Mahabharata had ten meanings, and that each of the thousand prominent names of Lord Visnu had a hundred meanings.</span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
When the astonished assembly demanded he prove his statement, Anandatirtha explained a hundred meanings of Visva, the first name of Visnu.&nbsp;Before he could proceed further, however, they begged him to stop, admitting they did not have the intelligence to comprehend his elaborate explanations.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
Back in Udupi, Anandatirtha, who was now known as Madhva, wrote a commentary on Bhagavad-gita and gave a copy to Acyutapreksa for his approval.&nbsp;Madhva's next tour was to Badarinatha, high in the Himalayas.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
In Badarinatha he met Srila Vyasadeva, the author of the four Vedas and their voluminous supplementary literature.&nbsp;In preparation for this meeting, Madhva had observed complete silence and complete fasting for forty-eight days.&nbsp;He learned the full meaning of the Vedanta-sutra, the distilled essence of the Vedic wisdom, from the transcendental author himself and promised to write a commentary on the sutras, one that would be faithful to Srila Vyasadeva's original intent and purport.</span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
By the time he came down from the Himalayas, his commentary, Sutra-bhasya, was completed.&nbsp;He sent a copy ahead to Udupi for Acyutapreksa's approval.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
On his return trip, Srila Madhvacarya converted Sobhana Bhatta and Sami Sastri to Vaisnavism.&nbsp;They later became successors to Madhva, as Padmanabha Tirtha and Narahari Tirtha.&nbsp;Madhva refused to let Narahari take sannyasa, ordering him to remain in his high government position, in return for which he was to obtain the Deities of Mula Rama and Sita, lying in the King of Kalinga's treasury.</span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
For many years Narahari remained in that service, until finally, just three months before Madhva's departure from this world, Narahari brought the ancient images of Sita-Rama to his guru.&nbsp;These were the original Deities of Rama and Sita, worshiped by Maharaja Iksvaku and then by Maharaja Dasaratha, the father of Lord Rama.&nbsp;Then during the time of Lord Krsna's advent, the Pandavas gave them to the Gajapati kings of Orissa.&nbsp;Eventually the Deities were kept in the kings' treasury.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
While still in his twenties, Srila Madhvacarya undertook a second tour to Badarinatha, this one after he had founded Sri Krsna Matha in Udupi.&nbsp;On the way, a tyrannical king pressed Madhva's party into digging a reservoir for the city of Devagiri.&nbsp;Madhva, however, persuaded the king himself to take part in the digging and then left with his party.&nbsp;The pilgrims had many other hardships and adventures, but Madhva always saved them with his quick thinking and mystic powers.&nbsp;In Badarinatha, Madhva again heard from Vyasa, who gave him eight sacred Salagrama stones.</span></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
<br>
On the return trip Madhva stopped in Goa, where he enacted an amazing gastronomical feat.<br>
Previously he had eaten a thousand bananas in one sitting.&nbsp;But in Goa, he out did his earlier record.&nbsp;He ate four thousand bananas and then drank thirty pots of milk.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
When asked to prove that plants indeed respond to music, Madhva took a few seeds in his hand and began singing in his melodious voice.&nbsp;The seeds sprouted, Madhva continued singing, and the plants grew, swaying to the melody.&nbsp;Madhva continued singing.&nbsp;The plants grew into full maturity and yielded fruits and flowers.&nbsp;News of this feat spread everywhere.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
From Udupi Madhva travelled south again.&nbsp;In Visnumangalam he debated with Trivikramacarya, a logician and grammarian of remarkable skill, who was able to make the Sanskrit language convey any meaning that suited his purpose.&nbsp;The debate lasted fifteen days, and in the end Trivikrama surrendered at Madhva's feet.&nbsp;A full account of that debate is given the Madhva-vijaya, written by the son of Trivikramacarya.&nbsp;News of Trivikrama's conversion brought hundreds more men and women into Madhva's fold.&nbsp;His life's mission thus became firmly rooted in India.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
Srila Madhvacarya wrote thirty-nine books clarifying the tenets of Vaisnavism and showing Vaisnavism to be the true Vedic religion.&nbsp;In many of his works he attacked the monistic creed of Sankaracarya's followers, exposing their doctrine as subversive to genuine spiritual understanding.&nbsp;Unable to defeat Madhva by argument, certain groups of monists conspired to impede Madhva's mission by less honourable means.&nbsp;They tried to defame him, declaring him to be a heretic and all his followers outcastes.&nbsp;They even stole his writings and his valuable collection of ancient books, thinking that without literature his mission would be finished.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
Somehow, King Jaya Simha of Visnumangalam acquired the books and returned them to Madhvacarya.&nbsp;Madhva had appeared in two other incarnations.&nbsp;During the time of Lord Krsna's appearance on the earth he appeared as the warrior Bhima, one of the five Pandava brothers.</span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
During the time of Lord Rama, he incarnated as the beloved Hanuman, the ideal servant of the Supreme Lord.&nbsp;And, as in those incarnations, Madhva performed many feats of strength and displayed mystical perfections.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
As a child he would appear suddenly in one mighty leap from anywhere in response to his mother's call.&nbsp;In school he cured a friend's headache by blowing into his ear.&nbsp;To help his father out of debt he turned tamarind seeds into money.&nbsp;On two occasions he made seeds sprout into plants by singing.&nbsp;An enormous rock in Ambu Tirtha, requiring at least fifty men to move it, bears an inscription stating that Madhvacarya placed it there with one hand.&nbsp;Many times Madhva made small quantities of food increase for distribution to hundreds of people.&nbsp;At the age of seventy-nine, his mission well established, Srila Madhvacarya passed away.&nbsp;His devotees say he went to Badarinatha to join Srila Vyasadeva.<br></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
The essential principles of Sri Madhvacarya's teachings-where they run parallel to the teachings of Sri Caitanya Mahaprabhu-have been summarized in ten points by Baladeva Vidyabhusana in his Prameya-Ratnavali.&nbsp;These ten points are as follows<span style="font-weight: bold;">:</span><br></span></div>
<div align="justify"></div>
<div style="text-align: center;" align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
<strong>shri madvhah praha vishnum paratamam akhilamnaya vedyam ca cisvam</strong><br>
<strong>satyam bhedam ca jivam hari carana jusas tartamyam ca tesham</strong><br>
<strong>moksham vishnv-anghri-labham tad-amala-bhajanam tasya hetum pramanam</strong><br>
<strong>pratyaksadi trayam cety upadisati hari krsna-caitanya chandra</strong></span></div>
<div align="justify"></div>
<div align="justify"><span style="font-family: Arial; font-size: 14px;"><br>
<br>
<strong>"Shri Madhvacaharya taught that:</strong><br>
<strong>1) Krishna, who is known as Hari is the Supreme Lord, the Absolute.</strong><br>
<strong>2) That Supreme Lord may be known through the Vedas.</strong><br>
<strong>3) The material world is real.</strong><br>
<strong>4) The jivas, or souls, are different from the Supreme Lord.</strong><br>
<strong>5) The jivas are by nature servants of the Supreme Lord.</strong><br>
<strong>6) There are two categories of jivas: liberated and illusioned.</strong><br>
<strong>7) Liberation means attaining the lotus feet of Krishna, that is, entering into an eternal relationship of service to the Supreme Lord.</strong><br>
<strong>8) Pure devotional service is the cause of this relationship.</strong><br>
<strong>9) The truth may be known through direct perception, inference, and Vedic authority. These very principles were taught by Shri Chaitanya Mahaprabhu."</strong><br>
<br>
In his Caitanya Caritamrta commentary (CC Madhya 9.245), Sripad Bhakdivedanta Swami comments:&nbsp;"For further information about Madhvacarya, one should read Madhva-vijaya by Narayana Acarya."</span></div>
<div align="justify"></div>
<div align="justify"><a href="http://www.iskcondesiretree.net/photo/albums/sri-madhvacharya-appearance">Click here for images</a></div>
<div align="justify">
<p>&nbsp;</p>
</div></div></div>
                            </div>
</div>

                </div>