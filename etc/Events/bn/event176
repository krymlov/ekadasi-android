﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Srila Sanatana Gosvami - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated">In his Laghu-Vaisnava-Tosani, Sri Jiva Goswami has mentioned their
family lineage beginning with Sri Sarva, who was a yajur vedi
brahmana, Bharadvaja gotriya, from Karnataka. His son was Sri
Aniruddha deva who had two sons Sri Rupesvara and Sri Harihar-deva.
There was apparently some altercation between the two brothers, who
had different mothers (Aniruddha deva had two wives) as a result of
which Sri Rupesvara along with his wife and eight horses, came to
Paulastya desa, where he was befriended by the ruler of that land,
Sri Sekaresvar. Sri Padmanabha deva, the son of Sri Rupesvar was
extremely learned in all the Vedas. He came with his family to live
at Naihati, on the banks of the Ganga. He had eight daughters and
five sons. His five sons, following in the footsteps of their
predecessors, were very expert in the study of Vedas. Their names
were Purusottama, Jagannatha, Narayana, Murari and Mukundadeva.
Mukundadeva's son, Sri Kumara deva, being oppressed by some of the
other family members, came to live at Bakla Candradvip. Sri
Kumaradeva had many sons, amongst whom Sri Amar (Sanatana), Sri
Santosh (Sri Rupa) and Sri Vallabha (Anupama) were great
devotees.<br>
<br>
Sri Sanatana was born in the Christian year 1488 (Sakabda 1410).
He, along with his brothers, began his studies from their maternal
uncle's house, in a small village, Sakurma, near the capital of
Gaura-desa.<br>
<br>
The Badsa Hassain Shah, having heard of their profound scholarship
and intelligence, decided to engage the two brothers as his
ministers. Though they were unwilling, the order of the Badsa could
not be entirely neglected. Thus they came to live at Ramakeli, the
capital of Gaura-desa at that time, and were presented much wealth
by the Badsa. Many brahmanas and pandits from distant lands would
stay with Rupa and Sanatana whenever they would come to Ramkeli,
specially, those from Karnataka and Navadwipa. There is still a
house near the Ganga, called Bhattabati, which is said to have been
their residence.<br>
<br>
They had many instructors and professors. In rhetoric their teacher
was Sri Vidyabhusanapada. They were trained in philosophy by the
brother of Sarvabhyuma Bhattacarya, Sri Vidyavacaspati, as well as
Sri Paramananda Bhattacarya, Sri Rampada Bhadrapada. Their names
have been mentioned in the commentary of the tenth canto of Srimad
Bhagavatam.<br>
<br>
These three brothers were from childhood endowed with great
devotion to the Lord. In memory of Sri Vrindavana, near their
residence they planted many auspicious trees such as Tamal,
Kadamba, Juthika and Tulasi. In the midst of these gardens
constructed Shyama-kunda and Radha-kunda. In these auspicious
surroundings they always remained absorbed in service to Sri Madana
Mohana. Having heard something about the famous Nimai Pandita they
became very anxious to have His darshana, and always meditated on
when they might get such a chance.<br>
<br>
One morning, Sanatana Goswami, saw a dream in which one brahmana
was presenting the Srimad Bhagavatam to him. Suddenly he woke up,
but seeing that no one was there, he felt sad. Later on, in the
morning after he had finished his bath and performance of puja, one
brahmana came to his house and presented him with the Bhagavata,
instructing him to study it very thoroughly. Having received it in
this way, he was beside himself with bliss, and from that day,
considering the Bhagavata to be the essence of all scriptures, he
began its thorough study.<br>
<br>
"My only constant companion, my only friend, guru, wealth, that
which has delivered me, my great fortune and the source of my good
luck, the form through which I taste pleasure, my obeisances to
you, Srimad Bhagavatam." [Sri Krsna-lila stava].<br>
<br>
When the brothers learned that Nimai Pandita, the life and soul of
Nadia, had accepted sannyasa and gone to stay at Puri, they fainted
dead away, having lost hope of getting His darshana. They were a
little pacified only after a celestial voice informed them that
they would be able to see the Lord, here in Ramakeli itself.<br>
<br>
After five years had passed, Mahaprabhu decided to come to Bengal
to have darshana of his mother and mother Ganga. All of the
devotees were in ecstasy and Saci mata was so overjoyed that she
was not even aware of her own body. After spending a few days with
Advaita Acarya in Santipur, He came to Ramakeli. [C. C. Mad
1.166]<br>
<br>
Sakara Mallik (Sanatan) and Dabir Khas (Rupa), along with their
brother Sri Ballabha (Anupama), who had a son who was just a small
child (Sri Jiva), they offered salutations at the lotus feet of
Mahaprabhu.<br>
<br>
After Mahaprabhu had left Ramakeli to return to Puri, the brothers
began to observe some vows and rituals in order to obtain shelter
at His lotus feet. After sending the family members to their
residences at Candradwip and Fateyabad, Sri Rupa and Anupama loaded
a boat with their accumulated wealth and left Ramakeli. Sanatan
remained there alone. Thereafter, Rupa and Anupama, having received
news of Mahaprabhu's journey to Vrindavana, set out to meet Him.
Arriving at Prayaga, their desire was fulfilled. At that time they
informed Mahaprabhu that their brother had been incarcerated at
Ramakeli. Mahaprabhu simply smiled and replied that he would get
his freedom very soon.<br>
<br>
Meanwhile, after the successful departure of Rupa and Anupama,
Sanatan was planning how he also could make his getaway. The Badsa
had entrusted Dabir Khas and Sakar Mallik with the main
responsibility of managing the affairs of his kingdom. When
Sanatana stopped attending his darbar on the plea of being ill, he
sent his personal doctor to examine him. The doctor informed him
that there was nothing wrong with Sakara Mallik, and so the Badsa
personally came there to find out what was the matter. The Badsa
addressed Sanatana Gosvami, "My doctor says that you are perfectly
healthy. All my affairs depend on you, but you are simply sitting
in your house, in the company of these pandits. Your brother has
also left. In this way my kingdom will topple. I don't know what
you are trying to do to me."<br>
<br>
Sanatana Gosvami said, "We will no longer be able to assist you in
the affairs of your government. You had better find someone else to
do it."<br>
<br>
The Badsa got up in great anger and declared, "You brothers have
ruined all my plans."<br>
<br>
Sanatan replied, "You are the independent ruler of Gauda. If you
feel anyone has committed any misdeed, then you can punish him as
you see fit."<br>
<br>
The Badsa had Sanatan imprisoned. During this time the Badsa was
preparing to go to Orissa to engage in warfare with the king of
that country, so he requested Sanatana to accompany him. Sanatana
refused, telling him, "As you will naturally try to give pain to
the Deities in the temples and the sadhus, I will<br>
not be able to accompany you."<br>
<br>
Therefore the Badsa set out for Orissa leaving Sanatana imprisoned.
At this time Sanatana received a letter from Sri Rupa, stating that
he had depositied eight hundred gold coins with one grocer. With
the help of this money, Sanatana should immediately arrange his
release. The account of his escape and journey to meet Sri Caitanya
Mahaprabhu in Varanasi is found in C.c., Madhya-lila ch. 20. After
that he went to Vrndavana.<br>
<br>
In a kutir cottage made from leaves, Sanatana Gosvami lived for
some time at Mahabon, the birth place of Sri Krsna. One day, he was
walking along the banks of the Yamuna, going to beg some foodstuffs
in a nearby village. Madana Gopaladeva was playing with some
cowherd boys there, and when he saw Sanatana Gosvami he came
running towards him, "Baba! Baba!". Catching hold of Sanatana's
hand he told him, "I want to go with you!"<br>
<br>
"Lala!" replied Sanatana, "Why do you want to go with me?"<br>
<br>
"I want to stay where you live."<br>
<br>
"If you stay with me, what will you eat?"<br>
<br>
"Baba! Whatever you eat."<br>
<br>
"But I only eat some dry capatis and chick peas."<br>
<br>
"Then that is what I will eat."<br>
<br>
"No that won't be enough for you. You should stay with your mother
and father."<br>
<br>
"Na. baba. na. I want to stay with you."<br>
<br>
Sanatan Goswami patiently explained that the boy might feel
difficulty if he stayed with him, and sent him home. Then he went
to beg some capatis in the village.<br>
<br>
That night, in a dream, he saw that boy again come to him. Smiling
very sweetly, he caught hold of Sanatana's hand and said, "Baba! I
am coming tomorrow to stay with you. My name is Madana Gopal". His
dream ended and he woke up. Losing himself in great ecstasy, he
said to himself, "What did I see? Such a beautiful boy!" Thinking
of Lord Krsna he opened the door to his hut and saw standing
outside a beautiful Deity of Gopal. His effulgence shone in all
directions. For a few seconds Sanatana was completely stunned as he
gazed upon Gopal's radiant smile. He expected that the Deity might
say something or come towards him. Finally, tears of love gliding
down his cheeks, Sanatana fell to the ground, offering his
dandavats.<br>
<br>
Gradually, he performed Gopala's abhiseka (bathed the Deity) and
offered worship to Him. Sanatana's brother Rupa came there and,
seeing the Deity, was deeply moved in ecstatic love. Sanatana kept
the Deity with him in his leaf hut and began to worship Him in
great happiness. Srila Rupa Gosvami immediately sent word of this
auspicious event to Mahaprabhu, at Puri.<br>
<br>
According to the different perspectives of vision of various
devotees, Krsna's pastimes might sometimes be described in
different ways, putting more or less emphasis on the external
events that surround the internal moods and sentiments felt by
Krsna and His devotees. In light of this, it has been described in
the Prema-vilas that the Madana Mohana Deity resided at the home of
one Mathura brahmana, Damodar Caube by name. Subsequent to the
period of time during which He was worshipped by Sri Advaita Acarya
Damodar Caube, his wife Ballabha and their son, Madan Mohan, used
to worship the Deity in the mood of parental affection and
friendship. Damodar Caube's son used to play together with Lord
Madana Gopala. Sometimes, like naughty brothers, they would slight
one another and then complain to the parents. Their parents would
feed them together at the same time and lay them down to rest
together.<br>
<br>
Sanatana Gosvami used to sometimes beg chapatis from Caube's house.
When he saw how the Deity was being worshipped he would instruct
Damodara's wife Ballabhadevi in the rules and regulations of proper
Deity worship. However, she found all of these rules very difficult
to follow. One day when Sanatana saw the Deity Madana Gopala and
the boy Madan Mohan eating their lunch together, he became moved by
the transcendental mood there and the symptoms of ecstatic love
appeared in his body. Then he told Ballabhadevi that she should
worship Madana Gopala according to the dictates of her heart.<br>
<br>
One night Sanatana Gosvami and Damodar Caube's wife both had a
dream simultaneously in which Madana Gopala requested to be able to
come and live with Sanatana Gosvami. In great happiness Sanatana
received Madana Gopala from the family and Him to a small hillock
near Surja ghat, where he constructed a small hut made of branches
and leaves. Then he began to serve Madana Gopala, preparing
offerings for Him from whatever he obtained by begging.<br>
<br>
One day Madana Gopala refused to eat, complaining that there wasn't
even any salt in the chapatis. Sanatana replied, "Today it's salt
and tomorrow it will be ghee. But I am sorry. I don't have the time
or the inclination to chase around after rich men requesting
special items from them". Having silently listened to this reply
Madana Mohana didn't say anything further, but rather arranged that
Krsna dasa Kapoor would come that way, as will subsequently be
described.<br>
<br>
Sanatana Gosvami would beg some flour from the village and then
with that prepare capatis for Madana Gopala. Sometimes he would
collect some forest vegetables, roots or spinach and also prepare
some vegetables. If sometimes there was no ghee or oil, or salt,
then he would just cook dry capatis. But he felt very bad about
this. On the other hand, he could not see an alternative.
Mahaprabhu had ordered him to compile Bhakti-shastras (devotional
scriptures) and the major portion of his time was devoted to that.
Sometimes it simply wasn't possible to find time to beg some money
with which to purchase salt and oil.<br>
<br>
"Madana Mohana is the son of a Maharaj. Seeing that He is simply
eating dry capatis Sanatana felt very sad; Madana Mohana, who is
within the heart of everyone could understand, 'Sanatana wants to
render greater service to Me.' Then Madana Mohana Himself desired
that His service might be increased."<br>
<br>
Within a few days a wealthy Kshatriya named named Sri Krsna dasa
Kapoor came to Mathura to engage in trade and business. By chance,
however, his boat became stuck on a sand bar in the Yamuna and by
no means could he manage to free it. By the by, he came to learn
that a sadhu by the name of Sri Sanatan Gosvami was living nearby.
In order to seek the blessings of the sadhu, Krsna dasa Kapoor came
to his hermitage and found Sanatana Gosvami engaged in writing.<br>
<br>
Sanatana Gosvami's body was very lean and thin from the practice of
great austerities and he was wearing only a kaupin. Krsna das
offered his dandavats and Sanatana Gosvami in turn offered him a
grass mat to sit on. Krsna dasa touched the mat with his hand and
sat on the ground. He appealed to the Gosvami, "Baba! Please bestow
your mercy on me."<br>
<br>
Sanatana replied, "I am a beggar. What mercy can I bestow upon
you?"<br>
<br>
"I simply want your blessings. My boat is stuck on a sand bar in
the Yamuna, and we can't free it by any means."<br>
<br>
"I am completely ignorant about all these matters You can speak to
Madana Gopala about it."<br>
<br>
Krsna das offered his dandavats to Madana Mohanji and spoke to Him,
"O Madana Gopala Deva! If, by Your mercy my boat is freed, then
whatever profit is realized from the sale of its cargo, I will give
to this Gosvami to be engaged in Your service."<br>
<br>
Praying this way, Kapoor Seth took leave from Sanatan Gosvami. That
afternoon there was such a downpour of rain that the boat very
easily floated off the sand bar and on to Mathura. Krsna dasa could
understand that this was the mercy of Lord Madana Gopala Deva. His
goods were sold at a very handsome profit and with this money he
constructed a temple and kitchen and made all the necessary
arrangements for the royal execution of Sri Madana Gopala's
worship. Seeing this arrangement, Sanatana Gosvami was very happy
and after some period initiated Krsna dasa Kapoor as his
disciple.<br>
<br>
Sri Madana Mohana Deva is presently worshiped at Karauli,
Rajasthan. When the daugher of the king of Jaipur was offered in
marriage to the king of Karauli, she very insistently requested
that her father send Lord Madana Mohana with her as a dowry, as she
was very attached to Him. Her father was very reluctant and agreed
only after stipulating one condition: "Madana Mohana would be
placed in a room with many other Deities. Whoever she chose while
blindfolded could go with her to Karauli."<br>
<br>
Madana Mohana reassured her by telling her that she would be able
to recognize Him by the soft touch of His arm. By this stratagem,
she easily recognized Madana Mohana who still resides in Kaurali
till this day. There is a direct bus to Karauli from Jaipur.
Otherwise, one can go by train from Mathura to Hindaun and then
from there to Karauli by bus.<br>
<br>
One day Sanatana Gosvami came to Radha-kunda to meet Sri Rupa and
Sri Raghunatha dasa Gosvami. Upon his arrival they both got up to
greet him and after respectfully seating him, they immersed
themselves in discussion of the nectarean pastimes of Sri Sri
Radha-Krsna. At that time Srila Rupa Gosvami was composing some
hymns in praise of Srimati Radharani, collectively known as "Catu
Puspanjali". Sanatan Gosvami, while reading these, came across one
verse:<br>
<br>
anava gorocana gauri praba rendi barambaram<br>
amani stavak vidyoti veni byalangana fanam<br>
<br>
Here "byalangana fanam" means that the braids of Radharani's hair
appeared very beautiful like the hoods of a snake. Sanatana Gosvami
reflected, "Is that a proper comparison 'like the hood of a
poisonous snake'?"<br>
<br>
At noon Sanatan came to the banks of Radha-kunda, and after
offering prayers there, he began to take his bath. Then, on the
opposite bank of the kunda, he noticed some cowherd girls playing
under the shade of a large tree. As he watched them from the
distance, it appeared that a black snake, hanging from the tree,
was about to wrap itself around the neck and shoulders of one of
those cowherd girls. Sensing some danger he called out to her, "Ohe
Lali! Look out! There is a snake just behind you!" But the girls
were absorbed in their play and didn't take notice of him. So he
immediately took off running to save them from the impending
danger. Seeing him approaching them, Srimati Radharani and Her
friends began to laugh. Then they disappeared. Sanatana was
completely stunned but then gradually the understanding came to him
that Sri Rupa's comparison was appropriate.<br>
<br>
Coming to the banks of Pavan Sarovar, Sanatana Gosvami entered into
some woods there, and giving up food and water, he became absorbed
in intense meditation on the lotus feet of Sri Sri Radha-Govinda.
Sri Krsna, who is within the heart of everyone, could understand
that His devotee was going without food, so He came there in the
dress of a cowherd boy, with a pot of milk in His hand, and stood
smiling before Sanatana Gosvami. [B.R. 5/1303]<br>
<br>
"Baba! I brought some milk for you."<br>
<br>
"Oh Lala! Why have you gone to such trouble for me?"<br>
<br>
"I saw you are sitting here for so long without any food."<br>
<br>
"How do you know that I am not eating anything?"<br>
<br>
"I come here to pasture my cows and I watch you to see what you are
doing. But you never take any food."<br>
<br>
"You should have sent someone else, you are just a small boy. You
have suffered some difficulty in bringing this milk here for
me."<br>
<br>
"Na, na, Baba. It was no trouble. At home everyone else was busy,
so I was happy to be able to come myself."<br>
<br>
Sanatan Gosvami requested the boy to sit down while he transferred
the milk into another container.<br>
<br>
"Na Baba! I can't sit down now. It is almost sunset. I have to milk
my cows now. I will come to get the pot tomorrow."<br>
<br>
When when Sanatana looked up there was no one there. He could
understand Sri Krsna Himself had brought him this milk. With tears
of love streaming down his cheek, he drank the milk. From that day
he gave up fasting and would go to beg some food-stuffs from the
Brijabasis. The Brijabasis also built him a small hut.<br>
<br>
One day Rupa Gosvami had a desire to cook some sweet rice for his
elder brother, Sanatana, but he had none of the necessary
ingredients, Sri Radha Thakurani, who fulfills the desires of her
devotees, could understand everything. Dressing Herself as a
cowherd girl, she came there carrying a basket containing rice and
sugar with a pot of milk in her other hand. "Svamin! Svamin! Please
accept this offering which I have brought".<br>
<br>
Hearing someone calling in such a sweet voice, he opened the door
of the kutir and saw an extremely beautiful cowherd girl standing
there with a present of rice, sugar and milk in her hands.<br>
<br>
"Lali! What brings you here so early this morning?"<br>
<br>
"Svamin, I came to bring you this present."<br>
<br>
"Oh! But you have gone to so much trouble."<br>
<br>
"What trouble? I have come to serve the sadhus."<br>
<br>
Sri Rupa requested her to sit down, but she replied that there was
much work at home, so she couldn't sit down just now. And then she
was gone. Sri Rupa looked up and saw that there was no one there
and was a little startled. "Now where did she run off to so
quickly?"<br>
<br>
He prepared the sweet rice and after offering to Sri Giri-dhari, he
gave the prasadam to Sri Sanatana. Sanatana was in total ecstasy
while accepting this prasadam and inquired, "From where did you get
the rice and milk?"<br>
<br>
Sri Rupa replied, "One cowherd girl brought everything."<br>
<br>
Sanatana asked, "Just like that? Suddenly she brought every
thing?"<br>
<br>
"Yes. This morning I was thinking to make some sweet rice for you.
Just after that I saw one cowherd girl standing before our kutir
with all the ingredients in her hands."<br>
<br>
As Sanatana heard this, tears began to glide down his cheeks. "The
taste of this sweet rice is from another world. Who else could have
brought such ingredients but Srimati Radha Thakurani herself. Don't
desire like this again." [B.R. 5.1322]<br>
<br>
Everyday Sri Sanatana Gosvami would circumambulate the fourteen
mile circumference of Govardhan hill. As he became advanced in
years this became somewhat difficult, but he was not inclined to
give up his vow. Krsna, however could understand that it was
difficult for him, so He came to him one day dressed as a cowherd
boy.<br>
<br>
"Baba! You have become old now, so I think it isn't necessary for
you to circumambulate Govardhan hill anymore."<br>
<br>
"No. Lala! This is my regular vow, my worship."<br>
<br>
"You can renounce this vow in your old age."<br>
<br>
"No Lala. One should never renounce his vows."<br>
<br>
"Baba. I have a very good idea, if you will accept it."<br>
<br>
"If it is acceptable surely I will accept it."<br>
<br>
Then Sri Krsna presented him a stone from Govardhan hill with the
imprint of His foot, a calf's foot print and the impressions of a
stick and flute in it.<br>
<br>
"Baba! This is a Govardhan sila."<br>
<br>
"What will I do with this?"<br>
<br>
"You can circumambulate this sila, and that will be the same as
circumambulating Giri Govardhan." Saying this much the cow herd boy
disappeared. Then Sanatana could understand that Giriraja Himself
had presented His worshippable form to him and from that day he
would circumambulate this sila.<br>
<br>
Sometimes Sri Sanatana used to stay at Mahaban. One day he saw some
cowherd boys playing on the banks of the Yamuna, and amongst them
was one boy whom he thought was Madana Gopala.<br>
<br>
"Is that my Madana Gopala playing there? No it must be one of the
local village boys."<br>
<br>
Then on another day as he was passing by the Yamuna, there again he
saw that same boy and thought, "This time let me wait and see where
he goes." Finally as the evening approached the boys finished
playing and set out for their respective homes.<br>
<br>
Following behind that particular boy, sure enough, Sanatana saw him
enter the temple. Then he could understand that Madana Gopala goes
every day to the banks of the Jamuna to play with the other
boys.<br>
<br>
Wherever Sri Sanatana and Sri Rupa would go throughout Vraja, in
all the various villages the two brothers were much adored by the
Brajabasis, who would feed them milk and yogurt. They in turn would
see the Vrajavasis as Krsna's own family members and respect them
in that way. Though it was not their business to engage in ordinary
gossip, with the Brajavasis they would inquire about their
well-being, e.g. how many sons and daughters they had and who had
been married where, what everyone's various names were, how their
cows were giving milk, how the bulls wewre working in the fields,
how the harvest was going on, who had fallen ill and if they were
getting better or not.<br>
<br>
In this way Rupa and Sanatana became the life of the villagers and
the Brijabasis also became the life of Rupa and Sanatana.<br>
<br>
Sri Sanatana would sometimes stay at Cakleswar, near Govardhan. At
that place there were many mosquitos, which was a great
disturbance. When he was one day being harassed by these insects,
Sanatan remarked, "I won't stay here anymore. It is impossible to
concentrate on anything. Neither can I write, nor chant."<br>
<br>
That night, Lord Siva came to Sanatan and told him, "Sanatan!
Please continue your service here in a happy frame of mind. From
tomorrow there will be no more disturbance from mosquitos."<br>
<br>
After that there were no more mosquitos and Sanatana continued his
bhajan free from disturbance.<br>
<br>
Sri Sanatana Gosvami compiled many scriptures. These include: Sri
Brhat-bhagavatmrta, Sri Hari-bhakti-vilas and its Dig-darsani-tika,
Sri Krsna-lilastava (dasam carit), Sri Bhagavata- tipani,
(commentary on Srimad Bhagavatam) and Brihat-vaisnava-tosani.<br>
<br>
Sri Sanatan Gosvami was born - 1488 (Christian) 1410 (Sakabda). At
the age of 27 he came to live at Braja where he remained for 43
years. He thus lived to be 70 years old. His disappearance was on
the full moon day of Asar in the year 1558 (Christian calendar).
His name in Braja-lila is Rati-manjari.</div></div>
                            </div>
</div>

                </div>