﻿<div class="xg_column xg_span-20 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Srila Murari Gupta - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated"><div style="text-align: center;"><img src="https://api.ning.com/files/LL9-QwpTRjUz2VI-1iksvxEwLB4aCIAl191ow3S4LbpqIyreuvTs6tMk10vDy8fZcE1lXENxB5HOrxTuDfhPUtsbYQk9g4sV/SrilaMurariGupta.jpg" width="400"></div>
<div align="justify">
<p style="text-align: center;"></p>
<p style="text-align: center;"><strong>sri-murari gupta sakha - premera bhandara</strong><br>
<strong>prabhura hridaya drave suni’ dainya yanra</strong></p>
<p><br>
<strong><span id="IL_AD8" class="IL_AD">TRANSLATION</span></strong><br>
Murari Gupta, the twenty-first branch of <span id="IL_AD3" class="IL_AD">the tree</span> of Sri Caitanya Mahaprabhu, was a storehouse of love of Godhead. His great humility and meekness melted the heart of Lord Caitanya.</p>
<p><br>
<strong>PURPORT</strong><br>
Sri Murari Gupta wrote a book called Sri Caitanya-carita. He belonged to a vaidya physician family of Srihatta, the paternal <span id="IL_AD11" class="IL_AD">home of</span> Lord Caitanya, and later became a resident of Navadvipa. He was among the elders of Sri Caitanya Mahaprabhu. Lord Caitanya exhibited His Varaha form in the house of Murari Gupta, as described in the Caitanya-bhagavata, Madhya-lila, Third Chapter. When Sri Caitanya Mahaprabhu exhibited His maha-prakasa form, He appeared before Murari Gupta as Lord Ramacandra.</p>
<p>&nbsp;</p>
<p>When Sri Caitanya Mahaprabhu and Nityananda Prabhu were sitting together in the house of Srivasa Thakura, Murari Gupta first offered his respects to Lord Caitanya and then to Sri Nityananda Prabhu. Nityananda Prabhu, however, was older than Caitanya Mahaprabhu, and therefore Lord Caitanya remarked that Murari Gupta had violated social etiquette, for he should have first shown respect to Nityananda Prabhu and then to Him. In this way, by the grace of Sri Caitanya Mahaprabhu, Murari Gupta was informed about the position of Sri Nityananda Prabhu, and the next day he offered obeisances first to Lord Nityananda and then to Lord Caitanya. Sri Caitanya Mahaprabhu gave chewed pan, or betel nut, to Murari Gupta. Once Sivananda Sena offered food to Lord Caitanya that had been cooked with excessive ghee, and the next day the Lord became sick and went to Murari Gupta for treatment. Lord Caitanya accepted some water from the waterpot of Murari Gupta, and thus He was cured. The natural remedy for indigestion is to drink a little water, and since Murari Gupta was a physician, he gave the Lord some drinking water and cured Him.</p>
<p>&nbsp;</p>
<p><br>
When Caitanya Mahaprabhu appeared in the house of Srivasa Thakura in His Caturbhuja murti, Murari Gupta became His carrier in the form of Garuda, and in these pastimes of ecstasy the Lord then got up on his back. It was the desire of Murari Gupta to leave his body before the disappearance of Caitanya Mahaprabhu, but the Lord forbade him to do so. This is described in Caitanya-bhagavata, Madhya-lila, Chapter Twenty. When Sri Caitanya Mahaprabhu one day appeared in ecstasy as the Varaha murti, Murari Gupta offered Him prayers. He was a great devotee of Lord Ramacandra, and his staunch devotion is vividly described in the Caitanya-caritamrita, Madhya-lila, Fifteenth Chapter, verses 137 through 157. (A.C.Bhaktivedanta Swami Prabhupada. Sri Chaitanya Charitamrita. Adi-lila 10.49. purport.)</p>
<p style="text-align: center;"><br>
<strong>Adi 10:50</strong><br>
<strong>pratigraha nahi kare, na laya kara dhana</strong><br>
<strong>atma-vritti kari’ kare kutumba bharana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
Srila Murari Gupta never accepted charity from friends, nor did he accept money from anyone. He practiced as a physician and maintained his family with his earnings.</p>
<p><br>
<strong>PURPORT</strong><br>
It should be noted that a grihastha (householder) must not make his livelihood by begging from anyone. Every householder of the higher castes should engage himself in his own occupational duty as a brahmana, kshatriya or vaisya, but he should not engage in the service of others, for this is the duty of a sudra. One should simply accept whatever he earns by his own profession. The <span id="IL_AD9" class="IL_AD">engagements</span> of a brahmana are yajana, yajana, pathana, pathana, dana and pratigraha. A brahmana should be a worshiper of Vishnu, and he should also instruct others how to worship Him.</p>
<p>&nbsp;</p>
<p>A kshatriya can become a landholder and earn his livelihood by levying taxes or collecting rent from <span id="IL_AD6" class="IL_AD">tenants</span>. A vaisya can accept <span id="IL_AD5" class="IL_AD">agriculture</span> or <span id="IL_AD7" class="IL_AD">general</span> trade as an occupational duty. Since Murari Gupta was born in a physician’s family (vaidya-vamsa), he practiced as a physician, and with whatever income he earned he maintained his family. As stated in Srimad-Bhagavatam, everyone should try to satisfy the Supreme Personality of Godhead through the execution of his occupational duty. That is the perfection of life. This system is called daivi-varnasrama. Murari Gupta was an ideal grihastha, for he was a great devotee of Lord Ramacandra and Caitanya Mahaprabhu. By practicing as a physician he maintained his family and at the same time satisfied Lord Caitanya to the best of his ability. This is the ideal of householder life. (A.C.Bhaktivedanta Swami Prabhupada. Sri Chaitanya Charitamrita. Adi-lila 10.50. purport.)</p>
<p style="text-align: center;"><br>
<strong>Adi 10.51</strong><br>
<strong>cikitsa karena yare ha-iya sadaya</strong><br>
<strong>deha-roga bhava-roga, - dui tara kshaya</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
As Murari Gupta treated his patients, by his mercy both their bodily and spiritual diseases subsided.</p>
<p><br>
<strong>PURPORT</strong><br>
Murari Gupta could treat both bodily and spiritual disease because he was a physician by profession and a great devotee of the Lord in terms of spiritual advancement. This is an example of service to humanity. Everyone should know that there are two kinds of diseases in human society. One disease, which is called adhyatmika, or material disease, pertains to the body, but the main disease is spiritual. The living entity is eternal, but somehow or other, when in contact with the material energy, he is subjected to the repetition of birth, death, <span id="IL_AD10" class="IL_AD">old age</span> and disease.</p>
<p>&nbsp;</p>
<p>The physicians of the modern day should learn from Murari Gupta. Although modern philanthropic physicians open gigantic hospitals, there are no hospitals to cure the material disease of the spirit soul. The Krishna consciousness movement has taken up the mission of curing this disease, but people are not very appreciative because they do not know what this disease is. A diseased person needs both proper medicine and a proper diet, and therefore the Krishna consciousness movement supplies materially stricken people with the medicine of the chanting of the holy name, or the Hare Krishna maha-<span id="IL_AD2" class="IL_AD">mantra</span>, and the diet of prasada.</p>
<p>&nbsp;</p>
<p>There are many hospitals and <span id="IL_AD4" class="IL_AD">medical clinics</span> to cure bodily diseases, but there are no such hospitals to cure the material disease of the spirit soul. The centers of the Krishna consciousness movement are the only established hospitals that can cure man of birth, death, old age and disease. (A.C.Bhaktivedanta Swami Prabhupada. Sri Chaitanya Charitamrita. Adi-lila 10.51. purport.)</p>
<p><br>
"All the devotees worshiped Lord Caitanya Mahaprabhu in this way, and the Lord remained in ecstasy for seven praharas, or twenty-one hours. He took this opportunity to show the devotees that He is the original Supreme Personality of Godhead, Krishna, who is the source of all other incarnations, as confirmed in the Bhagavad-gita (10.8): aham sarvasya prabhavo mattah sarvam pravartate. All the different forms of the Supreme Personality of Godhead, or vishnu-tattva, emanate from the body of Lord Krishna. Lord Caitanya Mahaprabhu exposed all the private desires of the devotees, and thus all of them became fully confident that Lord Caitanya is the Supreme Personality of Godhead.</p>
<p>&nbsp;</p>
<p>"Some devotees call this exhibition of ecstasy by the Lord sata-prahariya bhava, or “the ecstasy of twenty-one hours,” and others call it mahabhava-prakasa or maha-prakasa. There are other descriptions of this sata-prahariya bhava in the Caitanya-bhavagata, Chapter Nine, which mentions that Sri Caitanya Mahaprabhu blessed a maidservant named Duhkhi with the name Sukhi. He called for Kholaveca Sridhara, and showed him His maha-prakasa. Then He called for Murari Gupta and showed him His feature as Lord Ramacandra. He offered His blessings to Haridasa Thakura, and at this time He also asked Advaita Prabhu to explain the Bhagavad-gita as it is (gitara satya-patha) and showed special favor to Mukunda. (A.C. Bhaktivedanta Swami Prabhupada. Sri Chaitanya Charitamrita. Adi-lila 17.18. purport.)</p>
<p style="text-align: center;"><br>
<strong>varaha-avesa haila murari-bhavane</strong><br>
<strong>tanra skandhe cadi’ prabhu nacila angane</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
One day Sri Caitanya Mahaprabhu felt the ecstasy of the boar incarnation and got up on the shoulders of Murari Gupta. Thus they both danced in Murari Gupta’s courtyard.</p>
<p><br>
<strong>PURPORT</strong><br>
One day Caitanya Mahaprabhu began to cry out, “Sukara! Sukara!” Thus crying out for the boar incarnation of the Lord, He assumed His form as the boar incarnation and got up on the shoulders of Murari Gupta. He carried a small gadu, a small waterpot with a nozzle, and thus He symbolically picked up the earth from the depths of the ocean, for this is the pastime of Lord Varaha. (A.C. Bhaktivedanta Swami Prabhupada. Sri Chaitanya Charitamrita. Adi-lila 17.20. purport.)</p>
<p><br>
Sri Murari Gupta in all humility in his Chaitanya Mangala presentation of Lord Chaitanya's pastimes first begs for the blessings of all 'mahabhagavatas'. "Then I can properly sing the glories of the Lord. I am the lowest of the low, so how can I explain the greatness of Sri Caitanya's transcendental character? Since I don't know anything, what's the use of my incoherent words? If, without knowing the highest truths about Lord Caitanya, I try to say something, I will suffer embarrassment before the great souls.</p>
<p>&nbsp;</p>
<p>Although I am not qualified and full of faults, I cherish an intense desire to sing about the sweet qualities of Sri Gauranga Mahaprabhu."</p>
<p>&nbsp;</p>
<p>"While living in Navadwip, Sri Murari Gupta had the opportunity to remian always in the company of Gaurachandra. Who can describe his greatness? He is known throughout the world as Hanuman. After crossing the ocean to Lanka, Hanuman burned Ravan's palace to the ground. Then Hanuman brought Rama the good news about His beloved Sita. He revived Laksmana by bringing 'visalya-karani' (a medical herb). That same Hanuman now resides in Nadia as Murari Gupta.</p>
<p>&nbsp;</p>
<p>Being highly realised, Murari Gupta knows all truths. As an eternal associate of the Lord, he is fixed at the lotus feet of Sri Gauranga Mahaprabhu. He expertly told all of Lord Caitanya's childhood and boyhood pastimes in his great Sanskrit epic, 'Sri-Krishna-Caitanya-carita-mahakavya',(also called 'Karcha', dairy.).(Caitanya Mangala. 1994. Mahanidhi Swami, page 4.)</p>
<p><br>
Murari Gupta begged Lord Caitanya (Gauranga) to "please be merciful, please give me ecstatic love of God!"</p>
<p><br>
"Lord Gauranga said, 'Listen Murari, You have love for Me. So just serve the 'Parabraman' appearing in the human form, having the colour of an 'indranila' gem(blue sapphire). He stands most attractively in His three-fold bening form, carrying a flute in His hand. Worship Srimati Radharani, the golden comlexioned daughter of King Vrshabhanu. She is the original energy of the Lord, and Her effulgence defeats the colour of 'gorachana' (bright yellow).</p>
<p><br>
"Engage yourself in the service of the 'gopis', and you'll attain the son of Nanda. In 'cintamani-bhumi', Vrindavana-dham, He sits on a jewelled throne resting upon a gem-studded platform surrounded by 'kalpa-vrksha' trees. By His inconceivible potency, 'Kama-dhenu' (surabhi-cows) wander everywhere fulfilling all desires. His glittering bodily effulgence is known as the formless 'Brahman'. You should know that this is the sweet truth about Krishna."</p>
<p>&nbsp;</p>
<p>'The devotees felt transcendental joy to hear these talk from Lord Gauranga. I a mood of submission, Murari Gupta requested, "My lord, I want to see the Lord's form as Raghunatha (Lord Rama)."</p>
<p><br>
Within a second, Murari saw Lord Ramachandra in His beautiful body the colour of fresh green grass. Sitadevi, Laksmana, bharata and Satrughna stood around the Lord. Murari rolled on the ground in astonishment. Gaurasundara pacified him with the touch of His lotus hand while blessing him. "May you become saturated in love of God. Actually Murari, you are no one other than Hanuman, and I am the same Lord Rama." After saying this, the Lord entered the temple."(Caitanya Mangala. Locan dasa Thakura.; Mahanidhi Swami. 1994. page 134.)</p>
<p>&nbsp;</p>
<p><br>
Once Lord Caitanya asked Murari Gupta to recite some verses he had composed, "For My pleasure, please recite that verse you composed."</p>
<p><br>
'Murari Gupta cited his book Sri Krishna Caitanya-carita: "I worship the blessed Lord and master of the three worlds, Sri Ramachandra. He wears a brilliant crown covered with a strand of jewels whose shining lights up all directions. His dazzling earings defeat the brilliance of Sukra and Brhaspati. his beautiful face looks like a pure spotless moon.</p>
<p><br>
"I worship the lotus feet of the master of the three worlds, Sri Ramachandra, whose charming eyes appear like blossoming lotus flowers. his red lips look like 'bimba' fruits, and His well-shaped nose attracts eveyone. His wonderfull smile embarrasses the moonlight."</p>
<p><br>
After hearing these verses, Gaura put His feet on Murari's head. Then He wrote the name "Ramadasa" on his forehead. Prabhu said, "Murari, from today on, by My mercy, you'll be known as Ramadasa. Without Raghunatha, you can't live a moment. Know for certain that I am that same Lord Raghunath (Rama)."</p>
<p><br>
To the surprise of everyone present, Lord Gauranga showed His transcendental form as Lord Ramachandra, long with Janaki (Sita), Laksmana, Hanuman, and other associates. Instantly, Murari fell at the Lord's feet and praised Gauranga. "All glories to Raghavira. He's the same as the darling of Sacimata." Then Murari rolled in the dust. Crying in happiness, he offered more prayers.</p>
<p><br>
Showing mery, Mahaprabhu said, "Murari, you should worship Me, and forget anything other than 'bhakti'. Although I am your worshippable Lord Raghunatha, still you should worship the lord of Radha. Perform 'sankirtan', attentively hear the glories of Radha and Krishna, and be devoted to Me."(Caitanya Mangala. Locana dasa Thakura.; Mahanidhi Swami, page 153-154.)</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.152</strong><br>
<strong>prathame murari-gupta prabhure na miliya</strong><br>
<strong>bahirete padi’ ache dandavat hana</strong></p>
<p style="text-align: left;"><br>
meeting; bahirete—outside; padi’—falling down; ache—was there; dandavat—falling flat like a stick; hana—becoming so.</p>
<p style="text-align: left;">&nbsp;</p>
<p><strong>TRANSLATION</strong><br>
Murari Gupta at first did not meet the Lord but rather remained outside the door, falling down like a stick to offer obeisances.</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.153</strong><br>
<strong>murari na dekhiya prabhu kare anveshana</strong><br>
<strong>murari la-ite dhana aila bahu-jana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
When Lord Sri Caitanya Mahaprabhu could not see Murari amongst the devotees, He inquired about him. Thereupon many people immediately went to Murari, running to take him to the Lord.</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.154</strong><br>
<strong>trina dui-guccha murari dasane dhariya</strong><br>
<strong>mahaprabhu age gela dainyadhina hana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
Thus Murari Gupta, catching two bunches of straw in his teeth, went before Sri Caitanya Mahaprabhu with humility and meekness.</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.155</strong><br>
<strong>murari dekhiya prabhu aila milite</strong><br>
<strong>pache bhage murari, lagila kahite</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
Upon seeing Murari come to meet Him, Lord Sri Caitanya Mahaprabhu went up to him, but Murari began to run away and speak as follows.</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.156</strong><br>
<strong>more na chuniha, prabhu, muni ta’ pamara</strong><br>
<strong>tomara sparsa-yogya nahe papa kalevara</strong></p>
<p style="text-align: left;"><br>
<strong>TRANSLATION</strong><br>
“My Lord, please do not touch me. I am most abominable and am not fit for You to touch because my body is sinful.”</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.157</strong><br>
<strong>prabhu kahe,—murari, kara dainya samvarana</strong><br>
<strong>tomara dainya dekhi’ mora vidirna haya mana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
The Lord said, “My dear Murari, please restrain your unnecessary humility. My mind is disturbed to see your meekness.”</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.158</strong><br>
<strong>eta bali’ prabhu tanre kaila alingana</strong><br>
<strong>nikate vasana kare anga sammarjana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
Saying this, the Lord embraced Murari and had him sit down by His side. The Lord then began to cleanse his body with His own hands.</p>
<p style="text-align: center;"><br>
<strong>Madhya 11.159-160</strong><br>
<strong>acaryaratna, vidyanidhi, pandita gadadhara</strong><br>
<strong>gangadasa, hari-bhatta, acarya purandara</strong><br>
<strong>pratyeke sabara prabhu kari’ guna gana</strong><br>
<strong>punah punah alingiya karila sammana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
Lord Sri Caitanya Mahaprabhu then again and again embraced all the devotees, including Acaryaratna, Vidyanidhi, Pandita Gadadhara, Gangadasa, Hari Bhatta and Acarya Purandara. The Lord described their good qualities and glorified them again and again.</p>
<p style="text-align: center;"><br>
<strong>Madhya 15.137</strong><br>
<strong>murari-guptere prabhu kari’ alingana</strong><br>
<strong>tanra bhakti-nishtha kahena, sune bhakta-gana</strong></p>
<p><br>
<strong>TRANSLATION</strong><br>
Sri Caitanya Mahaprabhu then embraced Murari Gupta and began to speak about his firm faith in devotional service. This was heard by all the devotees.</p>
<p><br>
"There was a devotee of Caitanya Mahaprabhu. His name was Murari Gupta. He was physician to the then Nawab, Muslim Nawab. Now, they were sitting. The Nawab was going some hunting excursion or something like that, but he was Nawab’s physician. He was to accompany him. So they were sitting on the back of the elephant.</p>
<p>&nbsp;</p>
<p>In the meantime that Murari Gupta saw one peacock, and as soon as he saw the peacock, the feather, he at once remembered Krishna and at once fainted and fall down. This is called alambana. This is called alambana. Alambana means anything to the context, immediately he remembers his Lord and becomes ecstatic. This is the first-class stage of Krishna consciousness." (A.C. Bhaktivedanta Swami Prabhupada. 17th March 1967. Srimad Bhagavatam lecture 7:7:32-35. San Francisco.)</p>
<p><br>
<a href="http://www.iskcondesiretree.net/photo/albums/sri-murari-gupta">Click here for images</a><br></p>
</div></div></div>
                            </div>
</div>

                </div>