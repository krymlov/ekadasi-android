﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Sri Jayananda Prabhu - Disappearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated"><p><span><img src="https://api.ning.com/files/z*xaX9jnXtY66jvgE-hUWoKhfSTT7soCAOIckm5QTjLa4Wk2u7XIxd8iU0AD4FMk**zXR96OAWZljQ8ecI63Eqzcechg-bYp/DisappearanceDayofSriJayanandaPrabhu.jpg" width="200" class="align-center"></span></p>
<p><span>The test of a theory is in its&nbsp;</span><span>implementation</span><span>. While one can spend a lot of time analyzingthe process of bhakti, faith readily comes when one sees the theoryin action. Jayananda Prabhu is such a devotee who exemplified in analmost classical manner the process of bhakti from inception tocompletion. When he left his body on May 1, 1977, Srila Prabhupadainstructed that his disappearance day should be celebrated as that of a great Vaisnava. Observing the pastimes of such devotees greatly reaffirms one's faith in the process of bhakti.</span><br>
<br>
<span>The background</span><br>
<span>**************</span><br>
<span>Jim Kohr was the all-American boy. Handsome, strong, intelligent born in an upper middle-class family. A good student, he took a degree in mechanical engineering from&nbsp;</span><span>Ohio State University</span><span>. However despite his outward trappings of success, he was often unhappy, empty and dissatisfied with the material conception of life. Jim did not fit into the upper classcrowd, so it was not very surprising that he ended up driving cabsin San Francisco. Though an introspective person, he was not reallyreligious. His&nbsp;</span><span>depression</span><span>&nbsp;was almost suicidal when in 1967 he happened toread a small article in the San Francisco paper about an IndianSwami who had come to the Bay area to propagate the chanting of thenames of God. For some reason the article ignited a ray of hope within him. He resolved to attend the lecture of the Indian Swami.</span><br>
<br>
<span>In the Bhagavad Gita (7.16), Krishna says, 'four kinds of pious men begin to render devotional service unto Me -- the distressed, the desirer of&nbsp;</span><span>wealth</span><span>, the inquisitive, and he who is searching for knowledge of the Absolute.' Jim was searching for answers from the material miseries, and in such a mood took his first step towards his destiny.</span><br>
<br>
<span>The beginnings</span><br>
<span>**************</span><br>
<span>Jim was instantly attracted to the Indian Swami,&nbsp;</span><span>who was</span><span>&nbsp;none other than Srila Prabhupada, the founder acarya of ISKCON. He had been in the U.S.A for only a year and was in the process of establishing his mission of bringing Krishna consciousness to the Western world. Jim began to regularly attendthe morning Bhagvatam lectures of Srila Prabhupada. On some occasions Jim would be the only guest listening to the morning lecture. Soon he became very attached to Srila Prabhupada and his teachings. Prabhupada lovingly reciprocated and would sometimes personally cook prasadam for Jim and serve him. Soon thereafter Srila Prabhupada accepted Jim as his disciple and initiated himwith the spiritual name Jayananda.</span><br>
<br>
<span>In Cc Madhya 19.151 Caitnaya Mahaprabhu says that '. out of many millions of wandering living entities, one who is very fortunate gets an opportunity to associate with a bona fide spiritual masterby the grace of Krishna. By the mercy of both Krishna and the spiritual master, such a person receives the seed of the creeper of devotional service.' In this way Jayananda received the seed of devotion, out of the causeless mercy of Srila Prabhupada. From the instructions and teaching presented by Srila Prabhupada, he understood his special relationship with Krishna, with Guru, and that there is an authorized process to establish this relationship.</span><br>
<br>
<span>Attraction to Krishna consciousness</span><br>
<span>***********************************</span><br>
<span>Jayananda was completely enamored by Krishna consciousness. He would rise every day morning before four, do a little arati, chant his rounds of japa, read and cook prasad. Then he would go for his' incense run' (selling incense sticks). He never deviated from this, he was completely happy as long as he was practicing Krishna Consciousness.</span><br>
<br>
<span>Jayananda worshipped prasadam. When a little prasadam spilled on the floor he would lick it up. He loved to cook, eat, offer and distribute prasadam in a big way. He even said 'prasadam' with so much love and devotion that it made one immediately want to take some.</span><br>
<br>
<span>Another example of his attachment to Krishna consciousness was his love for the holy name. He was always seen chanting and dancingenthusiastically during kirtans. One day after working hard for tenhours</span><span>straight</span><span>,when all the other devotees were looking forward to some rest,Jayananda enthusiastically bounced into the temple room for kirtan.His japa was very intense, very focussed, as he strove topersonally associate with each and every syllable of themahamantra.</span><br>
<br>
<span>In Bhakti-rasamrta sindhu, Srila Rupa Goswami mentions that theprocess of sadhna bhakti begins with a little faith (sraddha). Thisfaith then blossoms into a desire for devotee association(sadhu-sanga) and then into bhajan-kriya (devotional service).</span><br>
<br>
<span>Similarly, Jayananda, after receiving the association of Srila Prabhupada and other devotees at the temple began to execute devotional service as per the instructions he received from his spiritual master. As he executed his sadhna sincerely, he became purified of the material impediments to devotional service(anartha-nivrttih) and began to&nbsp;</span><span>manifest</span><span>&nbsp;the all-attractive qualities of a pure devotee.</span><br>
<br>
<span>Humility</span><br>
<span>********</span><br>
<span>Humility was certainly Jayananda's most prominent quality. He treated everyone as his superior, even new devotees. Although his service was glorious, he never wanted any glory. He avoided praise like the plague. Devotees got to know that if they wanted to keep Jayananda's association they would better not praise him. His humility was very natural and he always found something other than himself that was praise worthy. Even though he was a senior devotee,older than most of the people around him and eminently qualified, he was happy to simply serve.</span><br>
<br>
<span>Once a new boy at the temple wanted to do some service and was asked to help with the trash. The weekly trash run was done by Jayananda, who cheerfully took the little help the boy gave. Later when the boy became a devotee, he remembered thinking, 'If the garbage men at this temple can be so blissful, just imagine what the rest of the devotees are like!'</span><br>
<br>
<span>Caitanya Mahaprabhu, in the third verse of Siksastakam says down the qualification for offenseless chanting. 'One can chant the holyname of the Lord in a humble state of mind, thinking himself lower than the straw in the&nbsp;</span><span>street</span><span>. One should be more tolerant than the tree, devoid of all sense of false prestige and ready to offer all respects toothers. In such a state of mind one can chant the holy name of the Lord constantly.' Jayananda exemplified this verse. He was so humble that just being in his association would make one feel ashamed of one's pride. He was very special, yet no one paid any special attention to him. That was just the way he liked things.</span><br>
<br>
<span>Service attitude</span><br>
<span>****************</span><br>
<span>Jayananda was an expert at everything:&nbsp;</span><span>cooking</span><span>, preaching, Deity worship, public relations, sankirtan, selling incense, construction, and anything that it took to spread Krishna consciousness. He was a tireless worker, first torise in the morning and last to sleep at night. He was always running out to&nbsp;</span><span>get flowers</span><span>, washing dishes, cleaning the kitchen or taking out the trash. Whatever service he was given, he would make sure that it was done, no matter how busy he was or how much personal hardships he had to endure for it. No matter how hard he was working, he would never stop for a nap during the day. He seemed inexhaustible.</span><br>
<br>
<span>Many times when Jayananda went to Berkley to distribute left over prasadam, he would first organize a crew to clean the kitchen,working twice as hard as anybody else, then he would&nbsp;</span><span>transfer</span><span>&nbsp;the prasadam, load it into the van, drive it to Berkley, organize the distribution there and have kirtan while all this was going on. Many years later he readily accepted the position of the driver for the Radha Damodar travelling sankirtan party, working side by side with brahmacharis scarcely half his age. In spite of his advanced position and seniority he never asked for anything special and readily accepted menial position under new devotees.</span><br>
<br>
<span>In the Vishnu purana, Sri Krishna informs Arjuna that 'one who claims to be My devotee is actually not My devotee. One who claims to be the devotee of my devotees is in actuality My devotee'.Jayananda completely manifested this quality. He was always striving to be the 'dasanudasa', the servant-of-the-servant. But there was no artificial humility in him. Material humility is relative, it is predicated on the qualifications of the recipients.Jayananda had spiritual humility, it was absolute, without any consideration of the status or qualities of the recipient. He served every one and expected no one to serve him.</span><br>
<br>
<span>The process of remembering, discussing or enumerating the qualities and pastimes of the Lord and His devotees is very purifying. This week we continue the discussion of the qualities of Jayananda Prabhu, who in the relatively short time he was associated with Krishna consciousness, perfected his devotional service and left us many instructions by example.</span><br>
<br>
<span>Freedom from fault finding</span><br>
<span>**************************</span><br>
<span>Perhaps the most defining characteristic of Jayananda was that he never criticized anyone. Even if a devotee did something that warranted criticism, he would usually not say anything, or else make the mistake appear as something perfectly natural. He never spoke harsh words or chastised anybody. Sometimes devotees would come to him with expansive ideas of how to spread Krishna Consciousness. Jayananda would encourage these ideas, however extraordinary. At the same time he was not a fool. He could always pick up the right man for the job.</span><br>
<br>
<span>In the Nectar of Instruction (Verse 5), Srila Rupa Goswami says,'one should associate with and faithfully serve that pure devotee who is advanced in undeviated devotional service and whose heart is completely devoid of the propensity to criticize others.' Jayananda could not even bear to hear the criticism of another devotee. If such a thing were happening he would simply leave the room. These are the characteristics of an uttama-adhikari, one who has reached the highest level of perfection in his sadhana bhakti.</span><br>
<br>
<span>Dear to everyone</span><br>
<span>****************</span><br>
<span>Like the six Goswamis, Jayananda was dear to both the gentle and the ruffians. He was as much at home with the Italians at the produce market as he was with the Brahmacharis at the temple. Once a devotee was approached by a staggering drunk in San Francisco,who looked at his robes and asked, 'Hey, where is my old friend Jayananda?'</span><br>
<br>
<span>Many devotees who took over Jayananda's old territory would meet people who would say things like, 'Where is Johnny Ananda?' or'That man - he's the nicest and most pure man I've met' or 'I don't know much about your philosophy, but if Jayananda is into it, it must be all right.'</span><br>
<br>
<span>One woman public official on the San Francisco board was famous for giving the devotees a hard time during Ratha-yatra. One year when the devotees approached her, she asked, 'where is Jayananda?' On hearing that he had passed away she broke down and began to cry.The purity in Jayananda's heart would touch the even most cynical.</span><br>
<br>
<span>In BG 5.7, Sri Krishna says that 'One who works in devotion, who is a pure soul, and who controls his mind and senses is dear to everyone, and everyone is dear to him. Though always working, such a man is never entangled.' Every one loved Jayananda, since he had completely transcended the bodily conception. He would approach a drunk, a hippie or a devotee with the same compassion and enthusiasm. He spoke to the Supersoul in everyone, and everyone responded accordingly. Like Maharaja Yudhisthira, Jayananda's enemy was never born.</span><br>
<br>
<span>Expert at engaging everyone</span><br>
<span>****************************</span><br>
<span>It is said that though Krishna has nothing to do with non-devotees,His devotees are even more compassionate than Him and will try and engage them in the Lord's service. Jayananda was eager to seeevery one engaged in Krishna's service. Whenever a new bhakta would come. Jayananda made him feel he was engaged in important work. Hewas older, bigger and stronger than just about anyone in the temple, and everyone was glad to be working under him.</span><br>
<br>
<span>His preaching style was very simple and direct. He would speak from the heart to the heart. Once he was preaching to a couple of hippies, while crawled under an automobile. All that was visible of him was a pair of legs, yet the two hippies stood there, transfixed by his message. During Ratha-yatra time he would organize a crew of cynics, hippies, bloopers, uncooperative personalities and non-devotees off the street to help build the carts. He would get them to work for ten to fourteen hours a day, always glorifying them.</span><br>
<br>
<span>The CC Antya 7.11 it is stated, 'The fundamental religious system in the Age of Kali is the chanting of the holy name of Krishna.Unless empowered by Krishna, one cannot propagate the sankirtana movement.' Because of the genuine compassion in Jayananda, Krishna gave Him the unique ability to make people want to render devotional service, directly or indirectly.</span><br>
<br>
<span>Materially renounced</span><br>
<span>********************</span><br>
<span>Jayananda had almost no possessions, even during his years as ahouseholder. What ever he had, he used for the service of the temple and Srila Prabhupada. When he was gifted five thousand dollars, he promptly donated that to Srila Prabhupada. In the introduction of the Nectar of Devotion, Srila Prabhupada acknowledges this contribution. For many years he was almost single handedly supporting the temple by driving his cab for 12-14 hours aday. When he was in his last days of his life, he used the money given to him for his treatment to support the Ratha-yatra in LosAngeles.</span><br>
<br>
<span>He was extremely careful with what he considered to be Srila Prabhupada's money. When selling incense, he would sleep on park benches in bitter cold rather than spend money on a motel. He used his considerable charm to get people to donate almost every thing that was needed. What he could no get for free, he made sure that he received a good value for the money spent.</span><br>
<br>
<span>His final lesson in material detachment came when it was discovered that he was suffering from cancer of the lymph and blood. Jayananda continued as if nothing had changed. When his body became frail and weak, he continued preaching, inspiring and organizing from his bed in the hospital. For him the body was simply a means to render devotional service to the Lord.</span><br>
<br>
<span>In CC. Madhya 6.254 Caitanya Mahaprabhu summarizes in the phrase'vairagya-vidya-nija-bhakti-yoga', which means 'renunciation through the wisdom that comes from practicing devotional service.'Jayananda was a true sannyasi, as one who did not just renounce material objects but actually renounced the desire for these material objects. He was always eager to use everything for the service of Krishna, however he had no personal desire for anything material.</span><br>
<br>
<span>Jayananda: king of the Ratha-yatra</span><br>
<span>***********************************</span><br>
<span>Jayananda was the backbone of the Bay area Ratha-yatra for several years. Behind the scene he would do everything for the preparation of the festival. He would beg food, flowers, funds - buy materials and build the carts. He would arrange for the permits, organize thecooking and serving of prasad. Although things always went right down the wire, he would consistently succeed in fulfilling all this plans every year. After the festival he would cook a cake or a pie for each and every person who had some how helped in the festival. Because of his efforts the Bay area devotees to this day enjoy anamazingly harmonious relationship with the city officials.</span><br>
<br>
<span>In his last days Jayananda was busy organizing the Ratha-yatra from the hospital bed. He would talk to people on the phone, send his associates to meet various persons and things began to miraculously materialize. Every moment of his life was preciously used in the service of Krishna.</span><br>
<br>
<span>Mystic opulence</span><br>
<span>*****************</span><br>
<span>It is said that pure devotional service brings about much opulence.Thought the devotees never seek them out, once they are there, they are used for the service of Krishna.</span><br>
<br>
<span>Jayananda apparently could function with very little or even nosleep. Towards the last few days of the Ratha-yatra he would sleepless than three hours a day, yet he was the most energetic and enlivened member of the crew. His propensity to consume prasad was astounding. He could consume buckets of halva, plates of samosas and potatoes without any side effect. When he was in his last days,he was put on an intravenous diet, yet he would often ask devotees to sneak in huge quantities of samosas and cheese-potatoes for him,which he would happily consume without any apparent distress. He would some times sleep in the Bhagavatam classes, since he was very tired, yet later on he could perfectly quote from the class or have a deep discussion about its contents.</span><br>
<br>
<span>In BG 4.26, Sri Krishna declares that, 'One who is engaged in full devotional service, unfailing in all circumstances, at once transcends the modes of material nature and thus comes to the level of Brahman.' In the Närada-pancaratra, devotional service to the Lord is likened unto a queen attended by her maid servants in the form of material opulences, liberation and mysticism. Jayananda never had any hankering for these, but when they came, he simply used them in the service of Krishna.</span><br>
<br>
<span>Special relationship with Srila Prabhupada</span><br>
<span>******************************************</span><br>
<span>Jayananda has complete faith in Srila Prabhupada. He perfected his devotion by making the instructions of Srila Prabhupada the very core of his life. He was advanced enough to realize that real association was through following the instructions of the spiritual master. Unlike most of the other devotees, who would go out of their way to get some personal association of Srila Prabhupada, Jayananda was contend to work in the background, carrying out his instructions. He exemplified the superiority of association by vani (instructions) over vapu (personal association). Srila Prabhupada would invariably call of Jayananda when he was in the temple. Jayananda would resist saying, 'No, I cannot go. I am too dirty. I am too fallen,' such were the transcendental exchanges between the spiritual master and his dear disciple.</span><br>
<br>
<span>In CC. Madhya 19.151, Caitanya Mahaprabhu says that, 'Among all the living entities wandering throughout the universe, one who is most fortunate comes in contact with a representative of the Supreme Personality of Godhead and thus gets the opportunity to execute devotional service.' Jayananda was one of these fortunate souls who came in contact with a pure devotee of the Lord and under his guidance was able to perfect his devotional life.</span><br>
<br>
<span>Conclusion</span><br>
<span>**********</span><br>
<span>Jayananda passed away on May 1, 1977. He joined the Krishna consciousness movement in 1967, just when it is in its beginning, and left the planet a few months before Srila Prabhupada. In the scriptures it is said that when the pure devotees of the Lord appear to execute His will, their associates invariably accompany them. One cannot help but speculate that Jayananda had only come to serve his eternal spiritual master. The fact that Srila Prabhupada was on this planet at the time of his passing away is also significant. It let Srila Prabhupada affirm that 'every one should follow the example of Jayananda.'</span></p></div></div>
</div>
</div>
</div>