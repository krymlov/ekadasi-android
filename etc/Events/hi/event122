﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Sri Pundarika Vidyanidhi - Appearance</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
        <div id="page-content" class="description"><div class="xg_user_generated"><div style="text-align: center;">
<h1><img src="https://api.ning.com/files/*hJfqXLzk8*XJAm3kY7-uH5lORTXAaI6bIYx0T6L7E9i7vw8vLt1zAKmgTAZQ4-2n66dc-x4I0kTRIzdng7cstX2WLn42-KN/1pundarika.jpg?width=336&amp;height=355" border="0px"></h1>
<h1 style="text-align: center;">Sri Pundarika Vidyanidhi</h1>
</div>
<div style="text-align: justify;">Sri Pundarika Vidyanidhi was a
disciple of Madhavendra Puri and was the guru of Sri Gadadhara
Pandita. Pundarika Vidyanidhi was sometimes misunderstood to be too
much attached to material pleasures, but just by hearing the
recitation of one verse of the Bhagavatam he would enter into a
trance. In Krsna's pastimes he was Vrsabhanu, the father of Srimati
Radharani.<span style="font-weight: bold;">(See Sri
Caitanya-caritamrta, Adi-lila 10.14 and Madhya-lila
16.76-81.)</span></div>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">pundarika
vidyanidhi——bada-sakha jani<br>
yanra nama lana prabhu kandila apani</span></blockquote>
</div>
<div>
<h4>TRANSLATION</h4>
</div>
Pundarika Vidyanidhi, the third big branch, was so dear to Lord
Caitanya Mahaprabhu that in his absence Lord Caitanya Himself would
sometimes cry.
<p>&nbsp;</p>
<div>
<h4>PURPORT</h4>
</div>
<p style="text-align: justify;">In the Gaura-ganoddesa-dipika Srila
Pundarika Vidyanidhi is described as the father of Srimati
Radharani in krishna-lila. Caitanya Mahaprabhu therefore treated
him as His father. Pundarika Vidyanidhi’s father was known as
Banesvara or, according to another opinion, Suklambara Brahmacari,
and his mother’s name was Gangadevi. According to one opinion,
Banesvara was a descendent of Sri Sivarama Gangopadhyaya. The
original home of Pundarika Vidyanidhi was in East Bengal, in a
village near Dacca named Baghiya, which belonged to the Varendra
group of <span style="font-style: italic;">brahmana</span>
families. Sometimes these Varendra <span style="font-style: italic;">brahmanas</span> were at odds with another
group known as Radhiya <span style="font-style: italic;">brahmanas</span>, and therefore Pundarika
Vidyanidhi’s family was ostracized and at that time was not living
as a respectable family. Bhaktisiddhanta Sarasvati informs us that
one of the members of this family is living in Vrindavana and is
named Sarojananda Gosvami. One special characteristic of this
family is that each of its members had only one son or no son at
all, and therefore the family was not very expansive. There is a
place in the district of Cattagrama in East Bengal that is known as
Hata-hajari, and a short distance from this place is a village
known as Mekhala-grama in which Pundarika Vidyanidhi’s forefathers
lived. One can approach Mekhala-grama from Cattagrama either on
horseback, by bullock cart or by steamer. The steamer station is
known as Annapurnara-ghata. The birthplace of Pundarika Vidyanidhi
is about two miles southwest of Annapurnara-ghata. The temple
constructed there by Pundarika Vidyanidhi is now very old and much
in need of repair. Without repair, the temple may soon crumble.
There are two inscriptions on the bricks of that temple, but they
are so old that one cannot read them. There is another temple,
however, about two hundred yards south of this one, and some people
say that this is the old temple constructed by Pundarika
Vidyanidhi.</p>
<p style="text-align: justify;">Sri Caitanya Mahaprabhu called
Pundarika Vidyanidhi “father,” and He gave him the title
Premanidhi. Pundarika Vidyanidhi later became the spiritual master
of Gadadhara Pandita and an intimate friend of Svarupa Damodara’s.
Gadadhara Pandita at first misunderstood Pundarika Vidyanidhi to be
an ordinary pounds-and-shillings man, but later, upon being
corrected by Sri Caitanya Mahaprabhu, he became his disciple.
Another incident in the life of Pundarika Vidyanidhi involves his
criticizing the priest of the Jagannatha temple, for which
Jagannatha Prabhu chastised him personally by slapping his cheeks.
This is described in Sri Caitanya-bhagavata, Antya-lila, Chapter
Seven. Sri Bhaktisiddhanta Sarasvati Thakura informs us that there
are still two living descendants of the family of Pundarika
Vidyanidhi, who are named Sri Harakumara Smrititirtha and Sri
Krishnakinkara Vidyalankara. For further information one should
refer to the dictionary known as Vaishnava-manjusha. (A.C.
Bhaktivedanta Swami Prabhupada. Sri Chaitanya Charitamrta Adi lila
10:14. purport.)</p>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">bada
sakha,——gadadhara pandita-gosani<br>
tenho lakshmi-rupa, tanra sama keha nai</span></blockquote>
</div>
<div>
<h4>TRANSLATION</h4>
</div>
<div style="text-align: justify;">Gadadhara Pandita, the fourth
branch, is described as an incarnation of the pleasure potency of
Sri Krishna. No one, therefore, can equal him. (Sri Chaitanya
Charitamrta Adi lila 10:15.txt.)</div>
<div>
<h4>PURPORT</h4>
</div>
<div style="text-align: justify;">There is an interesting story how
Srila Gadahar Pandit first met Pundarik Vidyanidhi. He had heard
that he was a very great devotee and one worthy of seeking out. So
he want to his home for a <span style="font-style: italic;">darshan</span>. His initial seeing him sent
confusion through his being. As before him sat what appeared to be
the personification of sense gratification - dressed in oppulent
silk clothing, hair oiled and nicely combed back, seated upon a
huge silken cushion with a water-pipe for smoking in front of him
and beautiful young women seated around the room.</div>
<p style="text-align: justify;">Gadadhara Pandit came there
expecting to see a <span style="font-style: italic;">sadhu</span>,
a guru, someone he could approach for spitual knowldge, to learn
from, and take shelter of. But to his surprise he saw this sense
gratification...</p>
<p style="text-align: justify;">"I came here to learn from you
about Krishna" Gadadhar said, ....but no sooner the word Krishna
were uttered Pundarik Vidyanidhi fell to the ground his body his
body became strangely transformed and he mumbled the name of
Krishna , Hare Krishna Hare Krishna Krishna Krishna Hare Hare, Hare
Rama Hare Rama Rama Rama Hare Hare.....</p>
<p style="text-align: justify;">Gadhadhara then realised that this
was no ordinary man, but a very advanced devotee absorded in
blissful Krishna consciousness.</p>
<p>&nbsp;</p>
<div>
<h4>Sri Chaitanya Charitamrita Madhya-lila 16th Chapter
Summary</h4>
</div>
<div>
<h4>The Lord’s Attempt to Go to Vrindavana</h4>
</div>
<div style="text-align: justify;">Srila Bhaktivinoda Thakura gives
the following summary of this chapter in his
Amrita-pravaha-bhashya. When Sri Caitanya Mahaprabhu wanted to go
to Vrindavana, Ramananda Raya and Sarvabhauma Bhattacarya
indirectly presented many obstructions. In due course of time, all
the devotees of Bengal visited Jagannatha Puri for the third year.
This time, all the wives of the Vaishnavas brought many types of
food, intending to extend invitations to Sri Caitanya Mahaprabhu at
Jagannatha Puri. When the devotees arrived, Caitanya Mahaprabhu
sent his blessings in the form of garlands. In that year also, the
Gundica temple was cleansed, and when the Caturmasya period was
over, all the devotees returned to their homes in Bengal. Caitanya
Mahaprabhu forbade Nityananda to visit Nilacala every year.
Questioned by the inhabitants of Kulina-grama, Caitanya Mahaprabhu
again repeated the symptoms of a Vaishnava. Vidyanidhi also came to
Jagannatha Puri and saw the festival of Odana-shashthi. When the
devotees bade farewell to the Lord, the Lord was determined to go
to Vrindavana, and on the day of Vijaya-dasami, He departed.</div>
<p style="text-align: justify;">Maharaja Prataparudra made various
arrangements for Sri Caitanya Mahaprabhu’s trip to Vrindavana. When
He crossed the River Citrotpala, Ramananda Raya, Mardaraja and
Haricandana went with Him. Sri Caitanya Mahaprabhu requested
Gadadhara Pandita to go to Nilacala, Jagannatha Puri, but he did
not abide by this order. From Kataka, Sri Caitanya Mahaprabhu again
requested Gadadhara Pandita to return to Nilacala, and He bade
farewell to Ramananda Raya from Bhadraka. After this, Sri Caitanya
Mahaprabhu crossed the border of Orissa state, and He arrived at
Panihati by boat. Thereafter He visited the house of Raghava
Pandita, and from there He went to Kumarahatta and eventually to
Kuliya, where He excused many offenders. From there He went to
Ramakeli, where He saw Sri Rupa and Sanatana and accepted them as
His chief disciples. Returning from Ramakeli, He met Raghunatha
dasa and after giving him instructions sent him back home.
Thereafter the Lord returned to Nilacala and began to make plans to
go to Vrindavana without a companion. (A.C. Bhaktivedanta Swami
Prabhupad. Sri Chaitanya Charitamrta. 16th Chapter Summary.)</p>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">ei-mata saba
vaishnava gaude calila<br>
vidyanidhi se vatsara niladri rahila</span></blockquote>
</div>
<div>
<h4>TRANSLATION</h4>
</div>
Finally all the Vaishnavas returned to Bengal, but that year
Pundarika Vidyanidhi remained at Jagannatha Puri.
<div>
<h4>Madhya 16.77</h4>
</div>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">svarupa-sahita tanra
haya sakhya-priti<br>
dui-janaya krishna-kathaya ekatra-i sthiti</span></blockquote>
</div>
<div>
<h4>TRANSLATION</h4>
</div>
Svarupa Damodara Gosvami and Pundarika Vidyanidhi had a friendly,
intimate relationship, and as far as discussing topics about
Krishna, they were situated on the same platform.
<div>
<h4>Madhya 16.78</h4>
</div>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">gadadhara-pandite
tenho punah mantra dila<br>
odana-shashthira dine yatra ye dekhila</span></blockquote>
</div>
<div>
<h3>TRANSLATION</h3>
</div>
Pundarika Vidyanidhi initiated Gadadhara Pandita for the second
time, and on the day of Odana-shashthi he saw the festival.
<div>
<h4>PURPORT</h4>
</div>
<div style="text-align: justify;">At the beginning of winter, there
is a ceremony known as the Odana-shashthi. This ceremony indicates
that from that day forward, a winter covering should be given to
Lord Jagannatha. That covering is directly purchased from a weaver.
According to the <span style="font-style: italic;">arcana-marga</span>, a cloth should first be
washed to remove all the starch, and then it can be used to cover
the Lord. Pundarika Vidyanidhi saw that the priest neglected to
wash the cloth before covering Lord Jagannatha. Since he wanted to
find some fault in the devotees, he became indignant.</div>
<div>
<h4>Madhya 16.79</h4>
</div>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">jagannatha parena
tatha ‘maduya’ vasana<br>
dekhiya saghrina haila vidyanidhira mana</span></blockquote>
</div>
<div>
<h3>TRANSLATION</h3>
</div>
When Pundarika Vidyanidhi saw that Lord Jagannatha was given a
starched garment, he became a little hateful. In this way his mind
was polluted.
<div>
<h4>Madhya 16.80</h4>
</div>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">sei ratrye
jagannatha-balai asiya<br>
dui-bhai cada’na tanre hasiya hasiya</span></blockquote>
</div>
<div>
<h4>TRANSLATION</h4>
</div>
<div style="text-align: justify;">That night the brothers Lord
Jagannatha and Balarama came to Pundarika Vidyanidhi and, smiling,
began to slap him.</div>
<div>
<h4>Madhya 16.81</h4>
</div>
<div style="font-weight: bold; text-align: center;">
<blockquote><span style="font-style: italic;">gala phulila, acarya
antare ullasa<br>
vistari’ varniyachena vrindavana-dasa</span></blockquote>
</div>
<div>
<h4>TRANSLATION</h4>
</div>
Although his cheeks were swollen from the slapping, Pundarika
Vidyanidhi was very happy within. This incident has been
elaborately described by Thakura Vrindavana dasa (in his Chaitanya
Bhagavat).<br>
<br>
<img class="align-center" src="https://api.ning.com/files/OmULmJ2J69es362YJ4nHV1ZRZnpom4SN6jrKQh6qfJLJeB6oe9FnxqTd0nU6riZK2K0FcBiypU40PaGATRx5q1HsBBkN8MRb/35.png" width="125"><br></div></div>
                            </div>
</div>

                </div>