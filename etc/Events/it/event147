﻿<div class="xg_column xg_span-16 xj_classic_canvas">
                    <div class="xg_headline">
<div class="tb"><h1>Lord Vamana Dvadasi Appearance of lord Vamandev</h1>
    </div>
</div>
<div class="xg_module">
    <div class="xg_module_body wpage">
<br>
<b>Lord Vamanadeva, the Dwarf Incarnation</b><br style="font-weight: bold;">
<span style="font-weight: bold;">Srimad Bhagavatam 8the Canto 18th
Chapter Summary</span><br>
<br>
This chapter describes how Lord Vamanadeva appeared and how He went
to the sacrificial arena of Maharaja Bali, who received Him well
and fulfilled His desire by offering Him benedictions.<br>
Lord Vamanadeva appeared in this world from the womb of Aditi
completely equipped with conchshell, disc, club and lotus. His
bodily hue was blackish, and He was dressed in yellow garments.
Lord Vishnu appeared at an auspicious moment on Sravana-dvadasi
when the Abhijit star had arisen. At that time, in all the three
worlds (including the higher planetary system, outer space and this
earth), all the demigods, the cows, the brahmanas and even the
seasons were happy because of God’s appearance. Therefore this
auspicious day is called Vijaya. When the Supreme Personality of
Godhead, who has a sac-cid-ananda body, appeared as the son of
Kasyapa and Aditi, both of His parents were very astonished. After
His appearance, the Lord assumed the form of a dwarf (Vamana). All
the great sages expressed their jubilation, and with Kasyapa Muni
before them they performed the birthday ceremony of Lord Vamana. At
the time of Lord Vamanadeva’s sacred thread ceremony, He was
honored by the sun-god, Brihaspati, the goddess presiding over the
planet earth, the deity of the heavenly planets, His mother, Lord
Brahma, Kuvera, the seven rishis and others. Lord Vamanadeva then
visited the sacrificial arena on the northern side of the Narmada
River, at the field known as Bhrigukaccha, where brahmanas of the
Bhrigu dynasty were performing yajnas. Wearing a belt made of munja
straw, an upper garment of deerskin and a sacred thread and
carrying in His hands a danda, an umbrella and a waterpot
(kamandalu), Lord Vamanadeva appeared in the sacrificial arena of
Maharaja Bali. Because of His transcendentally effulgent presence,
all the priests were diminished in their prowess, and thus they all
stood from their seats and offered prayers to Lord Vamanadeva. Even
Lord Siva accepts on his head the Ganges water generated from the
toe of Lord Vamanadeva. Therefore, after washing the Lord’s feet,
Bali Maharaja immediately accepted the water from the Lord’s feet
on his head and felt that he and his predecessors had certainly
been glorified. Then Bali Maharaja inquired of Lord Vamanadeva’s
welfare and requested the Lord to ask him for money, jewels or
anything He might desire.<br>
<br style="font-weight: bold;">
<span style="font-weight: bold;">Lord Vamanadeva Begs Charity from
Bali Maharaja</span><br style="font-weight: bold;">
<span style="font-weight: bold;">Srimad Bhagavatam 8th Canto 19th
Chapter Summary</span><br style="font-weight: bold;">
<br>
This Nineteenth Chapter describes how Lord Vamanadeva asked for
three paces of land in charity, how Bali Maharaja agreed to His
proposal, and how Sukracarya forbade Bali Maharaja to fulfill Lord
Vamanadeva’s request.<br>
When Bali Maharaja, thinking Vamanadeva to be the son of a
brahmana, told Him to ask for anything He liked, Lord Vamanadeva
praised Hiranyakasipu and Hiranyaksha for their heroic activities,
and after thus praising the family in which Bali Maharaja had been
born, He begged the King for three paces of land. Bali Maharaja
agreed to give this land in charity, since this was very
insignificant, but Sukracarya, who could understand that Vamanadeva
was Vishnu, the friend of the demigods, forbade Bali Maharaja to
give this land. Sukracarya advised Bali Maharaja to withdraw his
promise. He explained that in subduing others, in joking, in
responding to danger, in acting for the welfare of others, and so
on, one could refuse to fulfill one’s promise, and there would be
no fault. By this philosophy, Sukracarya tried to dissuade Bali
Maharaja from giving land to Lord Vamanadeva.<br>
<br style="font-weight: bold;">
<span style="font-weight: bold;">Bali Maharaja Surrenders the
Universe</span><br style="font-weight: bold;">
<span style="font-weight: bold;">Srimad Bhagavatam 8th Canto 20th
Chapter Summary</span><br>
<br>
The summary of this Twentieth Chapter is as follows. Despite his
knowledge that Lord Vamanadeva was cheating him, Bali Maharaja gave
everything to the Lord in charity, and thus the Lord extended His
body and assumed a gigantic form as Lord Vishnu.<br>
After hearing the instructive advice of Sukracarya, Bali Maharaja
became contemplative. Because it is the duty of a householder to
maintain the principles of religion, economic development and sense
gratification, Bali Maharaja thought it improper to withdraw his
promise to the brahmacari. To lie or fail to honor a promise given
to a brahmacari is never proper, for lying is the most sinful
activity. Everyone should be afraid of the sinful reactions to
lying, for mother earth cannot even bear the weight of a sinful
liar. The spreading of a kingdom or empire is temporary; if there
is no benefit for the general public, such expansion has no value.
Previously, all the great kings and emperors expanded their
kingdoms with a regard for the welfare of the people in general.
Indeed, while engaged in such activities for the benefit of the
general public, eminent men sometimes even sacrificed their lives.
It is said that one who is glorious in his activities is always
living and never dies. Therefore, fame should be the aim of life,
and even if one becomes poverty-stricken for the sake of a good
reputation, that is not a loss. Bali Maharaja thought that even if
this brahmacari, Vamanadeva, were Lord Vishnu, if the Lord accepted
his charity and then again arrested him, Bali Maharaja would not
envy Him. Considering all these points, Bali Maharaja finally gave
in charity everything he possessed.<br>
Lord Vamanadeva then immediately extended Himself into a universal
body. By the mercy of Lord Vamanadeva, Bali Maharaja could see that
the Lord is all-pervading and that everything rests in His body.
Bali Maharaja could see Lord Vamanadeva as the supreme Vishnu,
wearing a helmet, yellow garments, the mark of Srivatsa, the
Kaustubha jewel, a flower garland, and ornaments decorating His
entire body. The Lord gradually covered the entire surface of the
world, and by extending His body He covered the entire sky. With
His hands He covered all directions, and with His second footstep
He covered the entire upper planetary system. Therefore there was
no vacant place where He could take His third footstep.<br>
<br>
<span style="font-weight: bold;">Bali Maharaja Arrested by the
Lord</span><br style="font-weight: bold;">
<span style="font-weight: bold;">Srimad Bhagavatam 8th Canto 21st
Chapter Summary</span><br>
<br>
This chapter describes how Lord Vishnu, desiring to advertise the
glories of Bali Maharaja, arrested him for not fulfilling his
promise in regard to the Lord’s third step.<br>
With the second step the Supreme Personality of Godhead reached the
topmost planet of the universe, Brahmaloka, which He diminished in
beauty by the effulgence of His toenails. Thus Lord Brahma,
accompanied by great sages like Marici and the predominating
deities of all the higher planets, offered humble prayers and
worship to the Lord. They washed the Lord’s feet and worshiped Him
with all paraphernalia. Riksharaja, Jambavan, played his bugle to
vibrate the glories of the Lord. When Bali Maharaja was deprived of
all his possessions, the demons were very angry. Although Bali
Maharaja warned them not to do so, they took up weapons against
Lord Vishnu. All of them were defeated, however, by Lord Vishnu’s
eternal associates, and, in accordance with Bali Maharaja’s order,
they all entered the lower planets of the universe. Understanding
Lord Vishnu’s purpose, Garuda, the carrier of Lord Vishnu,
immediately arrested Bali Maharaja with the ropes of Varuna. When
Bali Maharaja was thus reduced to a helpless position, Lord Vishnu
asked him for the third step of land. Because Lord Vishnu
appreciated Bali Maharaja’s determination and integrity, when Bali
Maharaja was unable to fulfill his promise, Lord Vishnu ascertained
that the place for him would be the planet Sutala, which is better
than the planets of heaven.<br>
<br>
<span style="font-weight: bold;">Bali Maharaja Surrenders His
Life</span><br style="font-weight: bold;">
<span style="font-weight: bold;">Srimad Bhagavatam 8th Canto 22nd
Chapter Summary</span><br style="font-weight: bold;">
<br>
The summary of this Twenty-second Chapter is as follows. The
Supreme Personality of Godhead was pleased by the behavior of Bali
Maharaja. Thus the Lord placed him on the planet Sutala, and there,
after bestowing benedictions upon him, the Lord agreed to become
his doorman.<br>
Bali Maharaja was extremely truthful. Being unable to keep his
promise, he was very much afraid, for he knew that one who has
deviated from truthfulness is insignificant in the eyes of society.
An exalted person can suffer the consequences of hellish life, but
he is very much afraid of being defamed for deviation from the
truth. Bali Maharaja agreed with great pleasure to accept the
punishment awarded him by the Supreme Personality of Godhead. In
Bali Maharaja’s dynasty there were many asuras who because of their
enmity toward Vishnu had achieved a destination more exalted than
that of many mystic yogis. Bali Maharaja specifically remembered
the determination of Prahlada Maharaja in devotional service.
Considering all these points, he decided to give his head in
charity as the place for Vishnu’s third step. Bali Maharaja also
considered how great personalities give up their family
relationships and material possessions to satisfy the Supreme
Personality of Godhead. Indeed, they sometimes even sacrifice their
lives for the satisfaction of the Lord, just to become His personal
servants. Accordingly, by following in the footsteps of previous
acaryas and devotees, Bali Maharaja perceived himself
successful.<br>
While Bali Maharaja, having been arrested by the ropes of Varuna,
was offering prayers to the Lord, his grandfather Prahlada Maharaja
appeared there and described how the Supreme Personality of Godhead
had delivered Bali Maharaja by taking his possessions in a tricky
way. While Prahlada Maharaja was present, Lord Brahma and Bali’s
wife, Vindhyavali, described the supremacy of the Supreme Lord.
Since Bali Maharaja had given everything to the Lord, they prayed
for his release. The Lord then described how a nondevotee’s
possession of wealth is a danger whereas a devotee’s opulence is a
benediction from the Lord. Then, being pleased with Bali Maharaja,
the Supreme Lord offered His disc to protect Bali Maharaja and
promised to remain with him.<br>
<br>
<br style="font-weight: bold;">
<span style="font-weight: bold;">The Demigods Regain the Heavenly
Planets</span><br style="font-weight: bold;">
<span style="font-weight: bold;">Srimad Bhagavatam 8th Canto 23rd
Chapter Summary</span><br>
<br>
This chapter describes how Bali Maharaja, along with his
grandfather Prahlada Maharaja, entered the planet Sutala and how
the Supreme Personality of Godhead allowed Indra to reenter the
heavenly planet.<br>
The great soul Bali Maharaja experienced that the highest gain in
life is to attain devotional service under the shelter of the
Lord’s lotus feet in full surrender. Being fixed in this
conclusion, his heart full of ecstatic devotion and his eyes full
of tears, he offered obeisances to the Personality of Godhead and
then, with his associates, entered the planet known as Sutala. Thus
the Supreme Personality of Godhead satisfied the desire of Aditi
and reinstalled Lord Indra. Prahlada Maharaja, being aware of
Bali’s release from arrest, then described the transcendental
pastimes of the Supreme Personality of Godhead in this material
world. Prahlada Maharaja praised the Supreme Lord for creating the
material world, for being equal to everyone and for being extremely
liberal to the devotees, just like a desire tree. Indeed, Prahlada
Maharaja said that the Lord is kind not only to His devotees but
also to the demons. In this way he described the unlimited
causeless mercy of the Supreme Personality of Godhead. Then, with
folded hands, he offered his respectful obeisances unto the Lord,
and after circumambulating the Lord he also entered the planet
Sutala in accordance with the Lord’s order. The Lord then ordered
Sukracarya to describe Bali Maharaja’s faults and discrepancies in
executing the sacrificial ceremony. Sukracarya became free from
fruitive actions by chanting the holy name of the Lord, and he
explained how chanting can diminish all the faults of the
conditioned soul. He then completed Bali Maharaja’s sacrificial
ceremony. All the great saintly persons accepted Lord Vamanadeva as
the benefactor of Lord Indra because He had returned Indra to his
heavenly planet. They accepted the Supreme personality of Godhead
as the maintainer of all the affairs of the universe. Being very
happy, Indra, along with his associates, placed Vamanadeva before
him and reentered the heavenly planet in their airplane. Having
seen the wonderful activities of Lord Vishnu in the sacrificial
arena of Bali Maharaja, all the demigods, saintly persons, Pitas,
Bhutas and Siddhas glorified the Lord again and again. The chapter
concludes by saying that the most auspicious function of the
conditioned soul is to chant and hear about the glorious activities
of Lord Vishnu.<br>
<br>
SB 8.23.1<br>
sri-suka uvaca<br>
ity uktavantam purusham puratanam<br>
mahanubhavo ’khila-sadhu-sammatah<br>
baddhanjalir bashpa-kalakulekshano<br>
bhakty-utkalo gadgadaya girabravit<br>
<br>
TRANSLATION<br>
Sukadeva Gosvami said: When the supreme, ancient, eternal
Personality of Godhead had thus spoken to Bali Maharaja, who is
universally accepted as a pure devotee of the Lord and therefore a
great soul, Bali Maharaja, his eyes filled with tears, his hands
folded and his voice faltering in devotional ecstasy, responded as
follows.<br>
<br>
SB 8.23.2<br>
sri-balir uvaca<br>
aho pranamaya kritah samudyamah<br>
prapanna-bhaktartha-vidhau samahitah<br>
yal loka-palais tvad-anugraho ’marair<br>
alabdha-purvo ’pasade ’sure ’rpitah<br>
<br>
TRANSLATION<br>
Bali Maharaja said: What a wonderful effect there is in even
attempting to offer respectful obeisances to You! I merely
endeavored to offer You obeisances, but nonetheless the attempt was
as successful as those of pure devotees. The causeless mercy You
have shown to me, a fallen demon, was never achieved even by the
demigods or the leaders of the various planets.<br>
PURPORT<br>
When Vamanadeva appeared before Bali Maharaja, Bali Maharaja
immediately wanted to offer Him respectful obeisances, but he was
unable to do so because of the presence of Sukracarya and other
demoniac associates. The Lord is so merciful, however, that
although Bali Maharaja did not actually offer obeisances but only
endeavored to do so within his mind, the Supreme Personality of
Godhead blessed him with more mercy than even the demigods could
ever expect. As confirmed in Bhagavad-gita (2.40), svalpam apy asya
dharmasya trayate mahato bhayat: “Even a little advancement on this
path can protect one from the most dangerous type of fear.” The
Supreme Personality of Godhead is known as bhava-grahi janardana
because He takes only the essence of a devotee’s attitude. If a
devotee sincerely surrenders, the Lord, as the Supersoul in
everyone’s heart, immediately understands this. Thus even though,
externally, a devotee may not render full service, if he is
internally sincere and serious the Lord welcomes his service
nonetheless. Thus the Lord is known as bhava-grahi janardana
because He takes the essence of one’s devotional mentality.<br>
<br>
SB 8.23.15<br>
sri-sukra uvaca<br>
kutas tat-karma-vaishamyam<br>
yasya karmesvaro bhavan<br>
yajneso yajna-purushah<br>
sarva-bhavena pujitah<br>
<br>
TRANSLATION<br>
Sukracarya said: My Lord, You are the enjoyer and lawgiver in all
performances of sacrifice, and You are the yajna-purusha, the
person to whom all sacrifices are offered. If one has fully
satisfied You, where is the chance of discrepancies or faults in
his performances of sacrifice?<br>
PURPORT<br>
In Bhagavad-gita (5.29) the Lord says, bhoktaram yajna-tapasam
sarva-loka-mahesvaram: the Lord, the supreme proprietor, is the
actual person to he satisfied by the performance of yajnas. The
Vishnu purana (3.8.9) says:<br>
<br>
varnasramacaravata<br>
purushena parah puman<br>
vishnur aradhyate pantha<br>
nanyat tat-tosha-karanam<br>
<br>
All the Vedic ritualistic sacrifices are performed for the purpose
of satisfying Lord Vishnu, the yajna-purusha. The divisions of
society—brahmana, kshatriya, vaisya, sudra, brahmacarya, grihastha,
vanaprastha and sannyasa—are all meant to satisfy the Supreme Lord,
Vishnu. To act according to this principle of the varnasrama
institution is called varnasramacarana. In Srimad-Bhagavatam
(1.2.13), Suta Gosvami says:<br>
<br>
atah pumbhir dvija-sreshtha<br>
varnasrama-vibhagasah<br>
svanushthitasya dharmasya<br>
samsiddhir hari-toshanam<br>
<br>
“O best among the twice-born, it is therefore concluded that the
highest perfection one can achieve by discharging his prescribed
duties according to caste divisions and orders of life is to please
the Personality of Godhead.” Everything is meant to satisfy the
Supreme Personality of Godhead. Therefore, since Bali Maharaja had
satisfied the Lord, he had no faults, and Sukracarya admitted that
cursing him was not good.<br>
<br>
SB 8.23.16<br>
mantratas tantratas chidram<br>
desa-kalarha-vastutah<br>
sarvam karoti nischidram<br>
anusankirtanam tava<br>
<br>
TRANSLATION<br>
There may be discrepancies in pronouncing the mantras and observing
the regulative principles, and, moreover, there may be
discrepancies in regard to time, place, person and paraphernalia.
But when Your Lordship’s holy name is chanted, everything becomes
faultless.<br>
PURPORT<br>
Sri Caitanya Mahaprabhu has recommended:<br>
<br>
harer nama harer nama<br>
harer namaiva kevalam<br>
kalau nasty eva nasty eva<br>
nasty eva gatir anyatha<br>
<br>
“In this age of quarrel and hypocrisy the only means of deliverance
is chanting the holy name of the Lord. There is no other way. There
is no other way. There is no other way.” (Brihan-naradiya Purana
38.126) In this age of Kali, it is extremely difficult to perform
Vedic ritualistic ceremonies or sacrifices perfectly. Hardly anyone
can chant the Vedic mantras with perfect pronunciation or
accumulate the paraphernalia for Vedic performances. Therefore the
sacrifice recommended in this age is sankirtana, constant chanting
of the holy name of the Lord. Yajnaih sankirtana-prayair yajanti hi
sumedhasah (Bhag. 11.5.29). Instead of wasting time performing
Vedic sacrifices, those who are intelligent, those who possess good
brain substance, should take to the chanting of the Lord’s holy
name and thus perform sacrifice perfectly. I have seen that many
religious leaders are addicted to performing yajnas and spending
hundreds and thousands of rupees for imperfect sacrificial
performances. This is a lesson for those who unnecessarily execute
such imperfect sacrifices. We should take the advice of Sri
Caitanya Mahaprabhu (yajnaih sankirtana-prayair yajanti hi
sumedhasah). Although Sukracarya was a strict brahmana addicted to
ritualistic activities, he also admitted, nischidram anusankirtanam
tava: “My Lord, constant chanting of the holy name of Your Lordship
makes everything perfect.” In Kali-yuga the Vedic ritualistic
ceremonies cannot be performed as perfectly as before. Therefore
Srila Jiva Gosvami has recommended that although one should take
care to follow all the principles in every kind of spiritual
activity, especially in worship of the Deity, there is still a
chance of discrepancies, and one should compensate for this by
chanting the holy name of the Supreme Personality of Godhead. In
our Krishna consciousness movement we therefore give special stress
to the chanting of the Hare Krishna mantra in all activities.<br>
<br>
SB 8.23.27.<br>
brahma sarvah kumaras ca<br>
bhrigv-adya munayo nripa<br>
pitarah sarva-bhutani<br>
siddha vaimanikas ca ye<br>
sumahat karma tad vishnor<br>
gayantah param adbhutam<br>
dhishnyani svani te jagmur<br>
aditim ca sasamsire<br>
<br>
<br>
TRANSLATION<br>
Lord Brahma, Lord Siva, Lord Karttikeya, the great sage Bhrigu,
other saintly persons, the inhabitants of Pitriloka and all other
living entities present, including the inhabitants of Siddhaloka
and living entities who travel in outer space by airplane, all
glorified the uncommon activities of Lord Vamanadeva. O King, while
chanting about and glorifying the Lord, they returned to their
respective heavenly planets. They also praised the position of
Aditi.<br>
<br>
SB 8.23.28<br>
sarvam etan mayakhyatam<br>
bhavatah kula-nandana<br>
urukramasya caritam<br>
srotriinam agha-mocanam<br>
<br>
TRANSLATION<br>
O Maharaja Parikshit, pleasure of your dynasty, I have now
described to you everything about the wonderful activities of the
Supreme Personality of Godhead Vamanadeva. Those who hear about
this are certainly freed from all the results of sinful
activities.<br>
<br>
SB 8.23.29<br>
param mahimna uruvikramato grinano<br>
yah parthivani vimame sa rajamsi martyah<br>
kim jayamana uta jata upaiti martya<br>
ity aha mantra-drig rishih purushasya yasya<br>
<br>
TRANSLATION<br>
One who is subject to death cannot measure the glories of the
Supreme Personality of Godhead, Trivikrama, Lord Vishnu, any more
than he can count the number of atoms on the entire planet earth.
No one, whether born already or destined to take birth, is able to
do this. This has been sung by the great sage Vasishtha.<br>
PURPORT<br>
Vasishtha Muni has given a mantra about Lord Vishnu: na te vishnor
jayamano na jato mahimnah param anantam apa. No one can estimate
the extent of the uncommonly glorious activities of Lord Vishnu.
Unfortunately, there are so-called scientists who are subject to
death at every moment but are trying to understand by speculation
the wonderful creation of the cosmos. This is a foolish attempt.
Long, long ago, Vasishtha Muni said that no one in the past could
measure the glories of the Lord and that no one can do so in the
future. One must simply be satisfied with seeing the glorious
activities of the Supreme Lord’s creation. The Lord therefore says
in Bhagavad-gita (10.42), vishtabhyaham idam kritsnam ekamsena
sthito jagat: “With a single fragment of Myself, I pervade and
support this entire universe.” The material world consists of
innumerable universes, each one full of innumerable planets, which
are all considered to be products of the Supreme Personality of
Godhead’s material energy. Yet this is only one fourth of God’s
creation. The other three fourths of creation constitute the
spiritual world. Among the innumerable planets in only one
universe, the so-called scientists cannot understand even the moon
and Mars, but they try to defy the creation of the Supreme Lord and
His uncommon energy. Such men have been described as crazy. Nunam
pramattah kurute vikarma (Bhag. 5.5.4). Such crazy men
unnecessarily waste time, energy and money in attempting to defy
the glorious activities of Urukrama, the Supreme Personality of
Godhead.<br>
.<br>
SB 8.23.30<br>
TRANSLATION<br>
If one hears about the uncommon activities of the Supreme
Personality of Godhead in His various incarnations, he is certainly
elevated to the higher planetary system or even brought back home,
back to Godhead.<br>
.<br>
SB 8.23.31<br>
TRANSLATION<br>
Whenever the activities of Vamanadeva are described in the course
of a ritualistic ceremony, whether the ceremony be performed to
please the demigods, to please one’s forefathers in Pitriloka, or
to celebrate a social event like a marriage, that ceremony should
be understood to be extremely auspicious.<br>
PURPORT<br>
There are three kinds of ceremonies—specifically, ceremonies to
please the Supreme Personality of Godhead or the demigods, those
performed for social celebrations like marriages and birthdays, and
those meant to please the forefathers, like the sraddha ceremony.
In all these ceremonies, large amounts of money are spent for
various activities, but here it is suggested that if along with
this there is recitation of the wonderful activities of Vamanadeva,
certainly the ceremony will be carried out successfully and will be
free of all discrepancies.<br>
<br>
Thus end the Bhaktivedanta purports of the Eighth Canto,
Twenty-third Chapter, of the Srimad-Bhagavatam, entitled “The
Demigods Regain the Heavenly planets.”</div></div>
                            </div>
</div>

                </div>