# Ekādaśī #

The App calculates Shuddha Ekadashi, Tithis and Nakshatras, Sunrise and Sunset timings as well Moon timings potentially for all places in the world.

All calculations are done offline by Gaurabda Calendar program - GCal. GCal is developed by Gopalapriya Das of ISKCON Bratislava. GCal is a free and full-featured calendar calculation program for Gaudiya Vaisnavas.

Ekadashi, also spelled as Ekadasi, is the eleventh lunar day (Tithi) of the shukla (bright) or krishna (dark) paksha (fortnight) of every lunar month in the Hindu calendar (Panchang). It is considered a spiritually beneficial day. Scriptures recommend observing an (ideally waterless) fast from sunrise on the day of Shuddha Ekadashi to sunrise on the day following Ekadashi.

It is believed that committing spiritual austerities during the Shuddha Ekadashi help the soul to attain liberation from the cycle of birth and death. In addition, the Ekadashi helps cleanse the physical, mental and spiritual level. Thus, Ayurvedic medicine recommends fasting to maintain and improve health.

### The App main functionality: ###
- it includes stories about all Ekadashis you can read offline
- it allows to find your current location using GPS, WiFi, 3G, etc
- it includes a database with ~100 000 cities, so, you can find your location offline
- it allows to find almost any place by its name using Google service if a device has access to the Internet
- calculates Shuddha Ekadashi, Parana period, Tithi, Nakshatra, Rashi, Vaar, Lagna for the given place and time, taking into account the time zone and the daylight saving time (if determined)
- calculates Brahma muhurta, Abhijit muhurta, Rahu kalam, Gulika kalam, Yama gandam and Sandhya Times
- the ability to export Gaurabda calendar to Google Calendar or CSV file (Outlook, Yahoo)
- calculation of the beginning and completion of Chaturmasya - the time of vows, the annual sacred fast. Supported systems - Ekadashi, Purnima and Pratipat
- calculation of the Sompad Tithis - are considered auspicious for getting success in activities started during this day
- the ability to share the description of Ekadashi, Festival, Tithi and other Texts
- calculation of dates of own events based on the lunar calendar
- calculation of Lagna, Janma and Surya Rashi for your own events
- calculation of Shunya, Dagdha, Parva and Galagraha tithi
- calculation of Shani Sade Sati for your own events
- exporting and importing of your own events

### The App shows: ###
- days of Lord Krishna
- the Ekadashi day and its corresponding Parana day/time
- days of appearance and disappearance of the Divine Teachers of Vaishnavas (special days of fasting)
- days of appearance and disappearance of the great teachers of the past Vaishnavas 
- days of appearance and disappearance greatest Vaishnavas
- well known festivals and events
- tithis, nakshatras and etc
- lunar calendar
- zodiac signs
- own events

# The App includes the widget called: Ekadasi Calendar. #

### Main widget functionality:###
- you can add several widgets on a device screen
- each widget can have its own settings for a selected location
- widget is fully resizable, also its color and transparency can be changed
- widget header/cell is clickable and opens the App, Ekadashi description and etc
- notification and reminders about the events of Gaurabda Calendar
- notification and reminders about the daily events like:
-- Brahma muhurta, Abhijit muhurta, Rahu kalam, Gulika kalam, Yama gandam and Sandhya Times

### The widget shows or notifies about:###
- days of Lord Krishna
- upcoming events of Gaurabda Calendar
- upcoming Shuddha Ekadashi for a selected location
- Ekadashi Parana date/time for a selected location
- Sankranti day - transmigration of the Sun from one Rāshi to the next

# Swiss Ephemeris License

Please make sure before you use the project you are familiar with the Swiss Ephemeris License
- https://www.astro.com/swisseph/swephinfo_e.htm
- https://www.astro.com/swisseph/secont_e.pdf

If you want the Swiss Ephemeris Free Edition for your software project, please proceed as follows:
- make sure you understand the License conditions
- download the software
- start programming